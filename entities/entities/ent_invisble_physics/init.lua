AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
include('shared.lua')

function ENT:Initialize()
	
	self:PhysicsInit( SOLID_VPHYSICS)
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	self:SetUseType( SIMPLE_USE )
	
	self:GetPhysicsObject():SetMass(20)	
	self:GetPhysicsObject():Wake()
end

function ENT:OnTakeDamage(DmgTbl)
	local Safe = self.Safe
	if Safe.SafeHealth == 0 or Safe.Cracked then return end
	local Attacker = DmgTbl:GetAttacker()
	if !Attacker:IsPlayer() or !Attacker:IsCriminal() then return end
	
	local DmgAmount = math.Round(DmgTbl:GetDamage())
	local CurHealth = Safe:GetNWInt("SafeHealth")
	local NewHealth = math.Clamp(CurHealth - DmgAmount, 0, Safe.SafeHealth)
	
	Safe:SetNWInt("SafeHealth",NewHealth)
	
	if NewHealth == 0 then
		Safe:OpenSafe()
	end
end