ENT.Type = "anim"
ENT.Base = "base_gmodentity"

ENT.PrintName = "Printer"

ENT.Spawnable = true

function ENT:SetupDataTables()

	self:NetworkVar( "Int" , 0 , "FuelAmount" )
	self:NetworkVar( "Int" , 1 , "PaperAmount" )
	self:NetworkVar( "Int" , 2 , "InkAmount" )

	self:NetworkVar( "Bool" , 0 , "PrinterOn" )
	self:NetworkVar( "Bool" , 1 , "PressAttached" )
	self:NetworkVar( "Bool" , 2 , "PressReady" )

end