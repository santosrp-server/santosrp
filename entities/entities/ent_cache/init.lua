AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
include('shared.lua')

function ENT:Initialize()
	--self.Entity:SetPos(self.Entity:GetPos()+Vector(0,0,5))
	
	self.Entity:SetModel("models/props/CS_militia/footlocker01_closed.mdl")	
	self.Entity:PhysicsInit( SOLID_VPHYSICS )
	self.Entity:SetMoveType( MOVETYPE_VPHYSICS )
	self.Entity:SetSolid( SOLID_VPHYSICS )
	self.Entity:SetUseType( SIMPLE_USE )
	
	self.Entity.Lock = ents.Create("ent_lock")	
	self.Entity.Lock:SetPos(self.Entity:GetPos()+self.Entity:GetRight()*-1.15+self.Entity:GetUp()*3.5+self.Entity:GetForward()*13)	
	--self.Entity.Lock:SetPos(self.Entity:GetPos()+self.Entity:GetRight()*0+self.Entity:GetUp()*1.9+self.Entity:GetForward()*13)
	self.Entity.Lock:SetAngles(self.Entity:GetAngles())
	self.Entity.Lock:Spawn()
	self.Entity.Lock:Activate()	
	self.Entity.Lock.Chest = self.Entity
	
	constraint.Weld(self.Lock, self.Entity, 0, 0, 0, true, true)	
	
	self.Entity.Grabbing = 0
	self.CacheTaken = false
	self:RandomizeContents()
	self:GetPhysicsObject():EnableMotion(false)
end

function ENT:RandomizeContents()
    self.CacheWeapon = table.Random(PDRobberyConfig.CacheWeapons)
end

function ENT:ResetCache()
    self.CacheWeapon = table.Random(PDRobberyConfig.CacheWeapons)
	self.Unlocked = false
	self.CacheTaken = false
	self:SetModel("models/props/CS_militia/footlocker01_closed.mdl")	
	
	self.Lock:SetModel("models/props_wasteland/prison_padlock001a.mdl")
	self.Lock:SetPos(self.Entity:GetPos()+self.Entity:GetRight()*-1.15+self.Entity:GetUp()*3.5+self.Entity:GetForward()*13)
	self.Lock:SetAngles(self.Entity:GetAngles())
	self.Lock:GetPhysicsObject():EnableMotion(false)
	self.Lock:SetNWBool("Lockpicked",false)
	self.Lock:SetCollisionGroup(COLLISION_GROUP_NONE)
	constraint.Weld(self.Lock, self, 0, 0, 0, true, true)

	net.Start("PBankRB_CacheTaken")
		net.WriteEntity(self)
		net.WriteBool(false)
	net.Broadcast()	
end

function ENT:CancelGrabbing(Player)
    self.Grabbing = 0 
	Player.Grabbing = false

	net.Start("PBankRB_Grabbing")
	    net.WriteBool(false)
		net.WriteFloat(0)
	net.Send(Player)
end

function ENT:Use( activator, caller )
	if self.Entity:GetTable().Tapped and self.Entity:GetTable().Tapped > CurTime() then return false; end	
	self.Entity:GetTable().Tapped = CurTime() + 1;
	if self.CacheTaken or !self.Unlocked then return false end
	if !activator:IsPlayer() then return false; end
	if activator.GotBag then return end
	if PDRobberyConfig.RestrictBagGrab and !activator:CanTouchBag() then return end 	

	activator.Grabbing = true
	self.Grabbing = activator
	
	net.Start("PBankRB_Grabbing")
	    net.WriteBool(true)
		net.WriteFloat(CurTime())
	net.Send(activator)	

    self.TimeToGrab = CurTime() + PDRobberyConfig.BagGrabTime
	
	hook.Add("KeyRelease", "MonitorUse" .. activator:UniqueID(), function(Player, Key) if Key == IN_USE and activator.Grabbing and Player == activator then self:CancelGrabbing(activator) hook.Remove("KeyRelease", "MonitorUse" .. activator:UniqueID()) end end)
end

function ENT:Think()
    local Player = self.Grabbing
	if Player == 0 then return end
	if Player:IsPlayer() then
	    if self:GetPos():Distance(Player:GetPos()) > PDRobberyConfig.BagGrabRange then
		    self:CancelGrabbing(Player)
			return
		end
		
		Player:SetEyeAngles((self:GetPos()- Player:GetShootPos()):Angle())
		
		if self.TimeToGrab and self.TimeToGrab < CurTime() then
			self:CancelGrabbing(Player)	
			self.CacheTaken = true

			net.Start("PBankRB_CacheTaken")
				net.WriteEntity(self)
				net.WriteBool(true)
			net.Broadcast()	
						
			net.Start("PBankRB_GotBag")
	    	    net.WriteBool(true)
				net.WriteString("Weapon cache")
	    	net.Send(Player)
			
	    	self.BaggedItem = ents.Create("ent_baggedmoney")
	    	self.BaggedItem.MainEnt = self
	    	self.BaggedItem.Weapon = self.CacheWeapon
	    	self.BaggedItem:Spawn()
	    	self.BaggedItem:Activate()
			self.BaggedItem:SetModel(PDRobberyConfig.WeaponCacheModel)		
            self.BaggedItem:PhysicsInit( SOLID_VPHYSICS )			
			
	    	self.BaggedItem.LP = Player
	    	Player.GotBag = true
	
	    	Player.Bag = self.BaggedItem
	
	    	self.BaggedItem:SetPos(Player:GetPos())
	
	    	self.BaggedItem:SetMoveType( MOVETYPE_NONE )
	    	self.BaggedItem:SetSolid( SOLID_NONE )
	    	self.BaggedItem:SetCollisionGroup( COLLISION_GROUP_NONE )
	    	self.BaggedItem:DrawShadow( false )  
	
	    	//Entity returns null otherwise
	    	timer.Simple(0.2, function() 
	    	    net.Start("PBankRB_EntGrabbedBy")
	    	        net.WriteEntity(Player)
	    	        net.WriteEntity(Player.Bag)
					net.WriteString(self.BaggedItem.Weapon)
	    	    net.Broadcast()	
	    	end)					
		end
	end
end