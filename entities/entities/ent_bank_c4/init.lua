AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

include("shared.lua")

function ENT.Initialize(self)

	self:SetModel("models/weapons/w_c4.mdl")
	self:SetUseType(SIMPLE_USE)
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)

	self:SetModelScale( 2 )

	self:SetSeconds( 120 )

	local phys = self:GetPhysicsObject()

	if phys:IsValid() then

		phys:Wake()
 
	end

end

function ENT:OnRemove()
    
    timer.Remove( "C4_"..self:EntIndex() )    
    
end

function ENT:Use( activator, caller )

	if !timer.Exists( "C4_"..self:EntIndex() ) then

		timer.Create( "C4_"..self:EntIndex(), 1, self:GetSeconds(), function()

			if !self:IsValid() then return end

			local seconds = self:GetSeconds() - 1

			self:SetSeconds( seconds )

			self:EmitSound("HL1/fvox/beep.wav")

			if seconds == 0 then

				local explode = ents.Create( "env_explosion" )
				explode:SetPos( self:GetPos() )
				explode:SetOwner( caller )
				explode.NoFire = true
				explode:Spawn()
				
				explode:SetKeyValue( "iMagnitude", "200" )
				explode:Fire( "Explode", 0, 0 )
				--explode:EmitSound( "weapon_AWP.Single", 400, 400 )	
				
				
				local effectdata = EffectData()
				effectdata:SetOrigin( self:GetPos() )
				
				effectdata:SetScale(1.5)
				effectdata:SetMagnitude(1.5)
				effectdata:SetRadius(1.5)
				util.Effect( "Explosion", effectdata )
		
		
				
								
				-- self.Vault:OnExplode() ANDERS THIS LINKS TO YOUR VAULT

				self:Remove()

			end

		end )

    else
    
        timer.Remove( "C4_"..self:EntIndex() )
        self:SetSeconds( self:GetSeconds() )
    
    end

end