include("shared.lua")

function ENT:SecondsToClock(seconds)
  local seconds = tonumber(seconds)

  if seconds <= 0 then
    return "00:00";
  else
    hours = string.format("%02.f", math.floor(seconds/3600));
    mins = string.format("%02.f", math.floor(seconds/60 - (hours*60)));
    secs = string.format("%02.f", math.floor(seconds - hours*3600 - mins *60));
    return mins..":"..secs
  end
end

function ENT:Draw()

	local camPos = self:LocalToWorld( Vector( 4.2, 4.2, 8.2 ) )

	local camAng = self:LocalToWorldAngles( Angle( 0, -90, 0 ) )

	self:DrawModel()

	cam.Start3D2D( camPos , camAng , 0.1)

		// Main Box
		draw.RoundedBox(0,0,0,58,30,Color(21,27,31,255))

		draw.SimpleText( self:SecondsToClock( self:GetSeconds() ) ,"DermaDefault",29,15,Color(255,0,0),TEXT_ALIGN_CENTER,TEXT_ALIGN_CENTER)

	cam.End3D2D()	
	

end