AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
include('shared.lua')

function ENT:Initialize()
	self.Entity:SetModel("models/props_wasteland/prison_padlock001a.mdl")
    --self.Entity:SetModelScale(0.55,0)	
	self.Entity:PhysicsInit( SOLID_VPHYSICS )
	self.Entity:SetMoveType( MOVETYPE_VPHYSICS )
	self.Entity:SetSolid( SOLID_VPHYSICS )
	self.Entity:SetUseType( SIMPLE_USE )
	
	self.Entity:Activate()
	self.Entity.isFadingDoor = true
	
	self.Entity:GetPhysicsObject():EnableMotion(false)
end

function ENT:Unlock()
    self:SetModel("models/props_wasteland/prison_padlock001b.mdl")
	self:GetPhysicsObject():EnableMotion(true)
	constraint.RemoveConstraints(self, "Weld")

	self:SetNWBool("Lockpicked",true)
	self:SetCollisionGroup(COLLISION_GROUP_DEBRIS)
	
	if IsValid(self.Chest) then
	    self.Chest:SetModel("models/props/CS_militia/footlocker01_open.mdl")
		self.Chest.Unlocked = true
	end
end

function ENT:Use( activator, caller )
end

function ENT:Think()
end
