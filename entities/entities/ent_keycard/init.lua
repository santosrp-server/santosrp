AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
include('shared.lua')

function ENT:Initialize()
	self.Entity:SetModel("models/thcsecuritycard.mdl")	
	self.Entity:SetModelScale(4,0)
	self.Entity:PhysicsInit( SOLID_VPHYSICS )
	self.Entity:SetMoveType( MOVETYPE_VPHYSICS )
	self.Entity:SetSolid( SOLID_VPHYSICS )
	self.Entity:SetUseType( SIMPLE_USE )
	self:GetPhysicsObject():Wake()
	
	timer.Simple(PDRobberyConfig.KeycardDespawnTimer, function() if IsValid(self) then self:Remove() end end)
end

function ENT:Use( activator, caller )
	if self.Entity:GetTable().Tapped and self.Entity:GetTable().Tapped > CurTime() then return false; end	
	self.Entity:GetTable().Tapped = CurTime() + 5;
	if !activator:IsPlayer() then return false; end
	if activator.HasKeycard then DarkRP.notify(activator, 1, 4, "You already have a keycard!") return false end
	
	activator.HasKeycard = true
	net.Start("PBankRB_Keycard")
		net.WriteBool(true)
	net.Send(activator)	
	self:Remove()
end

function ENT:Think()
end

function ENT:Touch(TouchEnt)
end