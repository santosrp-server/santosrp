AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
include('shared.lua')

function ENT:Initialize()
	self.Entity:SetModel("models/props/cs_assault/MoneyPallet.mdl")	
	self.Entity:PhysicsInit( SOLID_VPHYSICS )
	self.Entity:SetMoveType( MOVETYPE_VPHYSICS )
	self.Entity:SetSolid( SOLID_VPHYSICS )
	self.Entity:SetUseType( SIMPLE_USE )
	self:GetPhysicsObject():EnableMotion(false)
	
	self.Entity.Touched = 0
end

function ENT:Use( activator, caller )		
		
	if !activator:IsPlayer() then return end
	if self.Entity.Touched and self.Entity.Touched > CurTime() then return ; end
		
	self.Entity.Touched = CurTime() + 1; 
		
	if !activator:IsGovernment() then return end 
	
    local EntBag = activator.Bag
	
	if !IsValid(EntBag) then return end	
	
	if EntBag.Weapon then
		activator:addMoney(PDRobberyConfig.PoliceCacheReward)
		DarkRP.notify(activator, 1, 4, "You received $" .. PDRobberyConfig.PoliceCacheReward .. " for returning the weapon cache.")		
		local Cache = EntBag.MainEnt
		timer.Simple(PDRobberyConfig.CacheResetCD, function()
			Cache:ResetCache()
		end)
	else
		local Safe = EntBag.MainEnt
		timer.Simple(PDRobberyConfig.SafeDrillCD, function()
			if IsValid(Safe) then
				Safe:SetBodygroup(1,0)
				Safe.Physics:SetSolid(SOLID_VPHYSICS)
				Safe.Cracked = false
				Safe:RandomizeMoney()
			end
		end)

		local AmountToHandOut = math.Round(EntBag.Amount*PDRobberyConfig.PolicePercentAmount)
		local BagOwner = EntBag.DBOwner
			
		if EntBag.DepositBoxBag and EntBag.DBOwnerPick and IsValid(BagOwner) then
			DarkRP.notify(activator, 1, 4, "The money inside the bag belonged to another player and its content was returned to its owner.")
			DarkRP.notify(BagOwner, 1, 4, "Your deposited money was returned to you by a government official through a government drop-off station.")
			BagOwner:addMoney(EntBag.Amount)
		else
			DarkRP.notify(activator, 1, 4, "You received $" .. AmountToHandOut .. " for returning the moneybag.")
			activator:addMoney(AmountToHandOut)
		end
	end		
	
	net.Start("PBankRB_GotBag")
		net.WriteBool(false)
		net.WriteString("")
	net.Send(activator)
	
	EntBag:Remove()
	activator.GotBag = false	
end

function ENT:Think()
end

function ENT:Touch(TouchEnt)
	if TouchEnt:GetClass() == "ent_baggedmoney" and TouchEnt.LP then	
        local Player = TouchEnt.LP
		
		if !Player:IsPlayer() then return end
		if !Player:IsGovernment() then return end 
		if self.Entity.Touched and self.Entity.Touched > CurTime() then return ; end
		
		self.Entity.Touched = CurTime() + 1; 

		if TouchEnt.Weapon then
			Player:addMoney(PDRobberyConfig.PoliceCacheReward)
			DarkRP.notify(Player, 1, 4, "You received $" .. PDRobberyConfig.PoliceCacheReward .. " for returning the weapon cache.")		
			local Cache = TouchEnt.MainEnt
			timer.Simple(PDRobberyConfig.CacheResetCD, function()
				Cache:ResetCache()
			end)
		else
		    local Safe = TouchEnt.MainEnt
			timer.Simple(PDRobberyConfig.SafeDrillCD, function()
				if IsValid(Safe) then
					Safe:SetBodygroup(1,0)
					Safe.Physics:SetSolid(SOLID_VPHYSICS)
					Safe.Cracked = false
					Safe:RandomizeMoney()
				end
			end)

			local AmountToHandOut = math.Round(TouchEnt.Amount*PDRobberyConfig.PolicePercentAmount)
			local BagOwner = TouchEnt.DBOwner
			
			if TouchEnt.DepositBoxBag and TouchEnt.DBOwnerPick and IsValid(BagOwner) then
				DarkRP.notify(Player, 1, 4, "The money inside the bag belonged to another player and its content was returned to its owner.")
				DarkRP.notify(BagOwner, 1, 4, "Your deposited money was returned to you by a government official through a government drop-off station.")
				BagOwner:addMoney(TouchEnt.Amount)
			else
				DarkRP.notify(Player, 1, 4, "You received $" .. AmountToHandOut .. " for returning the moneybag.")
				Player:addMoney(AmountToHandOut)
			end
		end		
		
		TouchEnt:Remove()		
	end
end