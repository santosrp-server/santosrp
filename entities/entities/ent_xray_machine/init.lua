AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

include("shared.lua")

function ENT.Initialize(self)

	self:SetModel("models/props_wasteland/kitchen_counter001b.mdl")
	self:SetUseType(SIMPLE_USE)
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)

	local phys = self:GetPhysicsObject()

	if phys:IsValid() then
		phys:Wake()
	end

	self.secondMachine = ents.Create("prop_physics")
	self.secondMachine:SetModel("models/props_wasteland/kitchen_counter001b.mdl")
	self.secondMachine:SetPos( self:LocalToWorld( Vector(58,0,0.1) ) )
	self.secondMachine:SetAngles( Angle(0,90,0) )
	self.secondMachine:SetParent( self )
	self.secondMachine:Spawn()
	self.secondMachine:Activate()

	self.computer = ents.Create("ent_xray_comp")
	self.computer:SetPos( self:LocalToWorld( Vector(0,10,20) ) )
	self.computer:SetAngles( Angle(0,0,0) )
	self.computer:SetParent( self )
	self.computer:Spawn()
	self.computer:Activate()

	self.podShelf = ents.Create("prop_physics")
	self.podShelf:SetModel("models/props_trainstation/BenchOutdoor01a.mdl")
	self.podShelf:SetPos( self:LocalToWorld( Vector(-58,0,-10) ) )
	self.podShelf:SetAngles( Angle(0,0,0) )
	self.podShelf:SetParent( self )
	self.podShelf:Spawn()
	self.podShelf:Activate()

	self.podHolder = ents.Create("prop_physics")
	self.podHolder:SetModel("models/props_c17/FurnitureShelf001b.mdl")
	self.podHolder:SetPos( self:LocalToWorld( Vector(-85,0,-5) ) )
	self.podHolder:SetAngles( Angle(0,0,0) )
	self.podHolder:SetParent( self )
	self.podHolder:Spawn()
	self.podHolder:Activate()	
	self.podHolder:SetHealth( 999999999 )

	self.stretcher = ents.Create( "prop_vehicle_prisoner_pod" )
	self.stretcher:SetPos( self:LocalToWorld(Vector(-120,0,8)) )
	self.stretcher:SetModel( "models/vehicles/prisoner_pod_inner.mdl" )
	self.stretcher:SetAngles( Angle(-90,0,90) )
	self.stretcher:SetKeyValue( "vehiclescript", "scripts/vehicles/prisoner_pod.txt" )
	self.stretcher:Spawn()
	self.stretcher:Activate()
	self.stretcher:SetParent( self )
	self.stretcher:SetNoDraw( true )
	self.stretcher:SetCollisionGroup( COLLISION_GROUP_WORLD )

end

function ENT:SpawnFunction( ply, tr, ClassName )

	if ( !tr.Hit ) then return end

	local SpawnPos = tr.HitPos + tr.HitNormal * 20

	local ent = ents.Create( ClassName )
	ent:SetPos( SpawnPos )
	ent:Spawn()
	ent:Activate()

	return ent

end

function ENT:StartXRay()

	if self:GetRayOn() then return end

	if !self.stretcher:GetDriver() then return end
	if !self.stretcher:GetDriver():IsPlayer() then return end

	local xPlayer = self.stretcher:GetDriver()

	self.computer:SetPlayerName( xPlayer:Nick() )

	local holderPos = -85
	local podPos = -120

	local timAmount = 1

	self:SetRayOn( true )

	timer.Create( "XRAYMACHINE", 0.1, 360, function()

		if timAmount == 120 then

			self.computer:SetIsScanning( true )

		elseif timAmount == 240 then

			if self.stretcher:GetDriver() then
				if self.stretcher:GetDriver():IsPlayer() then
					self.computer:FinishScan( self.stretcher:GetDriver() )
				else
					self.computer:CancelScan()
				end
			end

		end

		if timAmount <= 120 then

			self.podHolder:SetPos( Vector( holderPos , 0 , -5 ) )
			self.stretcher:SetPos( Vector( podPos , 0 , 8 ) )

			holderPos = holderPos + 1
			podPos = podPos + 1

		elseif timAmount <= 240 then

		else

			if timAmount == 360 then
				self:SetRayOn( false )
			end

			self.podHolder:SetPos( Vector( holderPos , 0 , -5 ) )
			self.stretcher:SetPos( Vector( podPos , 0 , 8 ) )

			holderPos = holderPos - 1
			podPos = podPos - 1

		end

		self.computer:SetProgress( timAmount )
		timAmount = timAmount + 1

	end)

end