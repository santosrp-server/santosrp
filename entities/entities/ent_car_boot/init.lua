
AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
include('shared.lua')


function ENT:Initialize()
	
	self:SetModel("models/carboots/boot_bigger_red_center.mdl")

	local colGold = Color( 255, 229, 0, 255 ) -- Creates a black color
	self:SetMaterial("models/debug/debugwhite")
	
	
	self:SetColor( colGold )
	self:PhysicsInit( SOLID_VPHYSICS)
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	self:SetUseType( SIMPLE_USE )
	if IsValid(self:GetPhysicsObject()) then
		self:GetPhysicsObject():Wake()
		self:GetPhysicsObject():SetMass(1) 
	end
	self:SetUseType(ONOFF_USE)
	

	self.Weld = nil
	self.NextAttach = CurTime()
end

function ENT:Attach(obj)
	if(self.Weld) then return end
	if(self.NextAttach > CurTime()) then return end
	
	self.AttachedEntity = obj
	self.type = self.AttachedEntity:GetModel();
	self:EmitSound("physics/metal/sawblade_stick1.wav")
	
	local eff = EffectData()
	eff:SetOrigin(self:GetPos())
	util.Effect("ManhackSparks", eff)
	self:GetPhysicsObject():SetMass(100)
	//fr_wheel
	//Bone_WFR
	//Wheel_FR
	//SGM = fine
	// tdm = fine
	local tofind = {"fr_wheel", "Bone_AFR","Wheel_FR"};
	
	if(obj:IsVehicle()) then
		local wheel_bone = "negative";
		local mx =  self.AttachedEntity:GetBoneCount();
		local c = 0;
		while(c <= mx) do
			for k,v in pairs(tofind) do
				if(v == self.AttachedEntity:GetBoneName(c)) then
					//Found
					wheel_bone = v;
					break;
				end
			end
			c = c + 1 ;
		end
		local BoneIndx = obj:LookupBone(wheel_bone)
		local BonePos,BoneAng = obj:GetBonePosition( BoneIndx )
		
		self:SetPos(obj:WorldToLocal(BonePos));
		self:SetMoveParent( obj )
		
		local newpos;
		if(wheel_bone == "fr_wheel") then
			newpos = Vector(self:GetPos().x + 3,self:GetPos().y  , self:GetPos().z + 5 )
			
		end
		if(wheel_bone == "Bone_AFR") then
			 newpos = Vector(self:GetPos().x + 25,self:GetPos().y  , self:GetPos().z + 5 )
			
		end
		if(newpos != nil) then
			self:SetPos(newpos)
		end
		
		self:SetAngles( BoneAng )

		
		self.Weld = constraint.Weld(self, obj, 0, 0, 0, true)
		local phys = self:GetPhysicsObject()
		if phys and phys:IsValid() then
			phys:EnableMotion(false) -- Freezes the object in place
			self:SetUnFreezable(false) 
		end
		obj.booted = true; 
	end
end

function ENT:Detach()
	constraint.RemoveConstraints(self, "Weld")
	self:GetPhysicsObject():SetMass(20)
	self.Weld = nil
	self.AttachedEntity.booted = false;
	self:Remove()
end


function ENT:Think()
	self:Extinguish()
	
	if(self.AttachedEntity and not IsValid(self.AttachedEntity)) then
		self:Detach()
		
		self.AttachedEntity = nil
	end
end

function ENT:Use(activator, caller)
	if(not activator:IsPlayer()) then return end
	if(activator:GetPos():Distance(self:GetPos()) > 100) then return end
	
	net.Start("CarBootMenu")
		net.WriteEntity(self);
		net.WriteBool(CarBoot.Config.Functions['CanRemoveCarBoot'](activator))
	net.Send(activator);
end


/* Stop the unfreezing of it */
function BootStop(ply, ent)
	if ent:GetClass():lower() == "ent_car_boot" then
		return false
	end
end
 
hook.Add("PhysgunPickup", "Stops the boot from moving", BootStop)

