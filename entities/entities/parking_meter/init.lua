AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
include( 'shared.lua' )

function ENT:Initialize()
	
	self.Entity:SetModel( "models/sickness/parkingmeter_01.mdl" )
	self.Entity:PhysicsInit( SOLID_VPHYSICS )
	self.Entity:SetMoveType( MOVETYPE_NONE )
	self.Entity:SetSolid( SOLID_VPHYSICS )
	local phys = self.Entity:GetPhysicsObject()
	if (phys:IsValid()) then
		phys:EnableMotion(false)
		phys:Wake()
	end
	self:SetUseType( SIMPLE_USE )

end


function ENT:OnTakeDamage( dmginfo )
	return;
end

function ENT:Use( activator, caller )
	net.Start( "MeterMenu")
		net.WriteFloat(self:EntIndex())
		net.WriteBool(self:GetInUse());
		net.WriteFloat(self:GetBank());
		net.WriteFloat(self:GetExpireTime());
	net.Send(caller)

	

end
 

function ENT:Think()
	if(self:GetExpireTime() <= CurTime()) then
		self:SetInUse(false);
	end

	self:NextThink( CurTime() + 1 )
	return true 
end

local function PlayerPickup( ply, ent )
	if ( ent:GetClass():lower() == "parking_meter" ) then
		if(CarBoot.Config.Options['EnableDebug']) then
			return true;
		else
			return false;
		end
	end
end
hook.Add( "PhysgunPickup", "AllowMovingMeter", PlayerPickup )