include("shared.lua")

local PaidColor = Color(0,255,0)
local ExpiredColor = Color(255,0,0)
local Out = Vector(2, 0, 57)
local Out2 = Vector(-2, 0, 57)
function ENT:Draw()
	self:DrawModel()
	local pos = self:LocalToWorld(Out)
	local pos2 = self:LocalToWorld(Out2)

	local alpha = util.PixelVisible(pos, 1, self.Pcon)*255
	local alpha2 = util.PixelVisible(pos2, 1, self.Pcon2)*255

	local color = Color(255, 0, 0, alpha)
	if(self:GetInUse() == true) then
		color = Color(0,255,0,alpha)
	end


	color2 = Color(color.r, color.g, color.b, alpha2)

	if alpha > 1 then
		render.SetMaterial( self.LightMat )
		render.DrawSprite(pos, 18, 18, color)
	end
	if alpha2 > 1 then
		render.SetMaterial( self.LightMat )
		render.DrawSprite(pos2, 18, 18, color2)
	end

	if CarBoot.Config.Options['EnableDebug'] then
		render.DrawWireframeBox( self:GetPos(), self:GetAngles(), CarBoot.Config.AutoBootConfig['SizeMin'], CarBoot.Config.AutoBootConfig['SizeMax'], Color(255,0,0) )
	end
end

function ENT:Initialize()
	self.Pcon = util.GetPixelVisibleHandle()
	self.Pcon2 = util.GetPixelVisibleHandle()
	self.LightMat = Material("sprites/light_ignorez")

end

function ENT:Think()
	self:SetNextClientThink(CurTime()+600)
	return true
end 