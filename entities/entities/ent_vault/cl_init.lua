--[[
	Name: cl_init.lua
	For: SantosRP
	By: TalosLife
]]--

include "shared.lua"
ENT.RenderGroup = RENDERGROUP_BOTH

surface.CreateFont( "TheVaultFonter", {
	font = "Arial", -- Use the font-name which is shown to you by your operating system Font Viewer, not the file name
	extended = false,
	size = 40,
	weight = 500,
	blursize = 0,
	scanlines = 0,
	antialias = true,
	underline = false,
	italic = false,
	strikeout = false,
	symbol = false,
	rotary = false,
	shadow = false,
	additive = false,
	outline = false,
} )


function ENT:Update()

self.InputTextVault = self:GetNWString("InputVault","Loading")

end

function ENT:Initialize()
self.InputTextVault = ""
local thetimer timer.Create( "VaultTimer", 2, 0, function() self:Update() end )

end


function ENT:Draw()
	self:RemoveAllDecals()
	self.Entity:DrawModel() 
		
	
	local Rotation2 = Vector(30, 90, 90)
		local Position2 = self:GetPos()+Vector(0,0,0) + (self:GetRight() * -52.5) + (self:GetUp() * 66 )+ (self:GetForward() * 80)
		local Angles = self:GetAngles()
		
		Angles:RotateAroundAxis(Angles:Right(), Rotation2.x)
		Angles:RotateAroundAxis(Angles:Up(), Rotation2.y)
		Angles:RotateAroundAxis(Angles:Forward(), Rotation2.z)
	cam.Start3D2D(Position2, Angles, 0.1) 
		draw.DrawText(self.InputTextVault, "TheVaultFonter", 50, 50, Color( 255, 204, 51, 255 ), TEXT_ALIGN_CENTER )
	cam.End3D2D()	
	
end