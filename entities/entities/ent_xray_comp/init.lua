AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

include("shared.lua")

util.AddNetworkString("XRAYRESULTS")

function ENT.Initialize(self)

	self:SetModel("models/props_lab/monitor02.mdl")
	self:SetUseType(SIMPLE_USE)
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)

	local phys = self:GetPhysicsObject()

	if phys:IsValid() then
		phys:Wake()
	end

end

function ENT:Use( activator, caller )

	self:SetIsScanning( false )
	self:SetHasResult( false )
	self:SetProgress( 0 )

	self:GetParent():StartXRay()

end

local damageTable = {
	["Head"] 		= "PHead",
	["Chest"] 		= "PChest",
	["Left Arm"] 	= "PLArm",
	["Right Arm"] 	= "PRArm",
	["Left Leg"] 	= "PLLeg",
	["Right Leg"] 	= "PLReg",
}

function ENT:FinishScan( pPlayer )

	for k, v in pairs( GAMEMODE.PlayerDamage:GetLimbs() ) do
		
		local brokeName = damageTable[v.Name]
		local isBroke = GAMEMODE.PlayerDamage:IsPlayerLimbBroken( pPlayer, k )

		self:SetNWBool( brokeName , isBroke )

	end

	self:SetHasResult( true )
	self:SetIsScanning( false )

end

function ENT:CancelScan()

	self:SetIsScanning( false )
	self:SetHasResult( false )
	self:SetProgress( 0 )

end