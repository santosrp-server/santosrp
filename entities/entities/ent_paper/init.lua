AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

include("shared.lua")

util.AddNetworkString("oakrp_openpaper")
util.AddNetworkString("oakrp_updatepaper")

function ENT.Initialize(self)

	self:SetModel("models/props_lab/bindergraylabel01a.mdl")
	self:SetUseType(SIMPLE_USE)
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)

	local phys = self:GetPhysicsObject()

	if phys:IsValid() then

		phys:Wake()

	end

	self:SetPaperText( "" )

end

function ENT:Use(ply)

	net.Start("oakrp_openpaper")
		net.WriteEntity(self)
	net.Send(ply)

end

net.Receive("oakrp_updatepaper",function(len, ply)

	local text = net.ReadString()
	local entity = net.ReadEntity()

	entity:SetPaperText( text )

end)