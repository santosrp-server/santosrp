AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

include("shared.lua")

function ENT.Initialize(self)

	self:SetModel("models/props_junk/wood_crate002a.mdl")
	self:SetUseType(SIMPLE_USE)
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)

	local phys = self:GetPhysicsObject()

	if phys:IsValid() then

		phys:Wake()
 
	end

end

function ENT:Use( activator, caller )

	if !IsValid( self:GetPlyOwner() ) then return end

	if !(caller == self:GetPlyOwner()) then
		caller:AddNote( "Those supplies do not belong to you!" )
		return
	end

	local supply = "Business Supplies"

	if self:GetSell() then
		supply = table.Random( GAMEMODE.Config.HaulageIncSupplies )
	end

	if GAMEMODE.Inv:GivePlayerItem( caller, supply, 1 ) then
		self:Remove()
	end

end

hook.Add("PhysgunPickup", "GM.Business.DisablePickupSupplies", function(pPlayer, eEntity)
	if eEntity:GetClass() == "ent_business_supplies" then
		return false
	end
end)