ENT.Type = "anim"
ENT.Base = "base_gmodentity"

ENT.PrintName = "Business Supplies"

ENT.Spawnable = true

function ENT:SetupDataTables()
	self:NetworkVar( "Entity", 0, "PlyOwner" )
	self:NetworkVar( "String", 0, "Business" )
	self:NetworkVar( "Bool", 0, "Sell" )
end