if SERVER then
	AddCSLuaFile()
end

local NET_SEARCH = 0
local NET_PATDOWN = 1
local NET_OPEN_REQ = 2
local NET_SHOW_SEARCH = 3
if SERVER then
	util.AddNetworkString "SearchMenu"
	net.Receive( "SearchMenu", function( intMsgLen, pPlayer )
		local action = net.ReadUInt( 8 )
		local Pos = pPlayer:GetShootPos()
		local Aim = pPlayer:GetAimVector()
		local ent = util.TraceLine{
				start = Pos,
				endpos = Pos +Aim *150,
				filter = { pPlayer },
		}.Entity

		if not IsValid( ent ) or not ent:IsPlayer() then return end

		if ent:HasWeapon( "weapon_handcuffed" ) or ent:HasWeapon( "weapon_ziptied" ) then
			if action == NET_SEARCH then
				local found = {}
				local item
				for k, v in pairs( ent:GetInventory() ) do
					item = GAMEMODE.Inv:GetItem( k )
					if not item or not item.Illegal then continue end
					found[k] = v
				end

				for k, v in pairs( ent:GetEquipment() ) do
					item = GAMEMODE.Inv:GetItem( v )
					if not item or not item.Illegal then continue end
					found[v] = found[v] or 0
					found[v] = found[v] +1
				end

				net.Start( "SearchMenu" )
					net.WriteUInt( NET_SHOW_SEARCH, 8 )
					net.WriteEntity( ent )
					net.WriteTable( found )
				net.Send( pPlayer )
			elseif action == NET_PATDOWN then
				local found = {}
				local item
				for k, v in pairs( ent:GetInventory() ) do
					item = GAMEMODE.Inv:GetItem( k )
					if not item or not item.Illegal then continue end
					found[k] = v
					GAMEMODE.Inv:TakePlayerItem( ent, k, v )
				end

				for k, v in pairs( ent:GetEquipment() ) do
					item = GAMEMODE.Inv:GetItem( v )
					if not item or not item.Illegal then continue end
					found[v] = found[v] or 0
					found[v] = found[v] +1
					GAMEMODE.Inv:DeletePlayerEquipItem( ent, k )
				end

				if table.Count( found ) > 0 then
					local spawnPos = util.TraceLine{
						start = pPlayer:GetShootPos(),
						endpos = pPlayer:GetShootPos() +pPlayer:GetAimVector() *150,
						filter = pPlayer,
					}.HitPos

					GAMEMODE.Inv:MakeItemBox( pPlayer, spawnPos, Angle(0, pPlayer:GetAimVector():Angle().y, 0), found )
				end
			end
		end
	end )
elseif CLIENT then
	net.Receive( "SearchMenu", function( intMsgLen, pPlayer )
		local action = net.ReadUInt( 8 )
		if action == NET_OPEN_REQ then
			GAMEMODE.Gui:Derma_Query(
				"What would you like to do?",
				"Search Player",
				"Search",
				function()
					net.Start( "SearchMenu" )
						net.WriteUInt( NET_SEARCH, 8 )
					net.SendToServer()
				end,
				"Pat Down",
				function()
					net.Start( "SearchMenu" )
						net.WriteUInt( NET_PATDOWN, 8 )
					net.SendToServer()
				end,
				"Cancel",
				function() end
			)
		elseif action == NET_SHOW_SEARCH then
			local ply = net.ReadEntity()
			local foundItems = net.ReadTable()
			local text = "Found the following items:\n"

			for k, v in pairs( foundItems ) do
				text = text.. k.. "x".. v.. "\n"
			end
			GAMEMODE.Gui:Derma_Message(
				text,
				"Search Results",
				"OK"
			)
		end
	end )
end

SWEP.PrintName			= "Hands"
SWEP.Author				= "The Maw"
SWEP.Purpose    		= "Nothing"

SWEP.ViewModel			= "models/weapons/c_medkit.mdl"
SWEP.WorldModel			= ""

SWEP.AnimPrefix	 		= "rpg"

SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

function SWEP:Initialize()
	self:SetHoldType( "normal" )
	
	self.Time = 0
	self.Range = 150
end

function SWEP:Think()
	if self.Drag and (not self.Owner:KeyDown(IN_ATTACK) or not IsValid(self.Drag.Entity)) then
		self.Drag = nil
	end
end

function SWEP:PrimaryAttack()
	local Pos = self.Owner:GetShootPos()
	local Aim = self.Owner:GetAimVector()
	
	local Tr = util.TraceLine{
		start = Pos,
		endpos = Pos +Aim *self.Range,
		filter = player.GetAll(),
	}
	
	local HitEnt = Tr.Entity
	if self.Drag then 
		HitEnt = self.Drag.Entity
	else
		if SERVER and IsValid( HitEnt ) and GAMEMODE.Property:GetPropertyByDoor( HitEnt ) then
			self.Owner:EmitSound( "physics/wood/wood_crate_impact_hard3.wav" )
			self:SetNextPrimaryFire( CurTime() +0.25 )
			return
		end

		if not IsValid( HitEnt ) or HitEnt:GetMoveType() ~= MOVETYPE_VPHYSICS or
			HitEnt:IsVehicle() or HitEnt:GetNWBool( "NoDrag", false ) or
			HitEnt.BlockDrag or
			IsValid( HitEnt:GetParent() ) then 
			return
		end
		
		if not self.Drag then
			self.Drag = {
				OffPos = HitEnt:WorldToLocal(Tr.HitPos),
				Entity = HitEnt,
				Fraction = Tr.Fraction,
			}
		end
	end
	
	if CLIENT or not IsValid( HitEnt ) then return end
	
	local Phys = HitEnt:GetPhysicsObject()
		
	if IsValid( Phys ) then
		local Pos2 = Pos +Aim *self.Range *self.Drag.Fraction
		local OffPos = HitEnt:LocalToWorld( self.Drag.OffPos )
		local Dif = Pos2 -OffPos
		local Nom = (Dif:GetNormal() *math.min(1, Dif:Length() /100) *500 -Phys:GetVelocity()) *Phys:GetMass()
		
		Phys:ApplyForceOffset( Nom, OffPos )
		Phys:AddAngleVelocity( -Phys:GetAngleVelocity() /4 )
	end
end

function SWEP:SecondaryAttack()
	if CLIENT then return end
	if IsValid( self.Owner:GetVehicle() ) then return end

	local Pos = self.Owner:GetShootPos()
	local Aim = self.Owner:GetAimVector()
	local ent = util.TraceLine{
			start = Pos,
			endpos = Pos +Aim *self.Range,
			filter = { self, self.Owner },
	}.Entity

	if not IsValid( ent ) or not ent:IsPlayer() then return end
	if ent:HasWeapon( "weapon_handcuffed" ) or ent:HasWeapon( "weapon_ziptied" ) then
		net.Start( "SearchMenu" )
			net.WriteUInt( NET_OPEN_REQ, 8 )
		net.Send( self.Owner )
	end
end

if CLIENT then
	local x, y = ScrW() /2, ScrH() /2
	local MainCol = Color( 255, 255, 255, 255 )
	local Col = Color( 255, 255, 255, 255 )

	function SWEP:DrawHUD()
		if IsValid( self.Owner:GetVehicle() ) then return end
		local Pos = self.Owner:GetShootPos()
		local Aim = self.Owner:GetAimVector()
		
		local Tr = util.TraceLine{
			start = Pos,
			endpos = Pos +Aim *self.Range,
			filter = player.GetAll(),
		}
		
		local HitEnt = Tr.Entity
		if IsValid( HitEnt ) and HitEnt:GetMoveType() == MOVETYPE_VPHYSICS and
			not self.rDag and
			not HitEnt:IsVehicle() and
			not IsValid( HitEnt:GetParent() ) and
			not HitEnt:GetNWBool( "NoDrag", false ) then

			self.Time = math.min( 1, self.Time +2 *FrameTime() )
		else
			self.Time = math.max( 0, self.Time -2 *FrameTime() )
		end
		
		if self.Time > 0 then
			Col.a = MainCol.a *self.Time

			draw.SimpleText(
				"Drag",
				"DermaLarge",
				x,
				y,
				Col,
				TEXT_ALIGN_CENTER
			)
		end
		
		if self.Drag and IsValid( self.Drag.Entity ) then
			local Pos2 = Pos +Aim *100 *self.Drag.Fraction
			local OffPos = self.Drag.Entity:LocalToWorld( self.Drag.OffPos )
			local Dif = Pos2 -OffPos
			
			local A = OffPos:ToScreen()
			local B = Pos2:ToScreen()
			
			surface.DrawRect( A.x -2, A.y -2, 4, 4, MainCol )
			surface.DrawRect( B.x -2, B.y -2, 4, 4, MainCol )
			surface.DrawLine( A.x, A.y, B.x, B.y, MainCol )
		end
	end
end

function SWEP:PreDrawViewModel( vm, pl, wep )
	return true
end