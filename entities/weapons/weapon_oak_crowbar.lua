AddCSLuaFile()

SWEP.PrintName		= "Crowbar"
SWEP.Author			= "Jake Butts"

SWEP.ViewModel 		= Model("models/weapons/c_crowbar.mdl")
SWEP.WorldModel 	= Model("models/weapons/c_crowbar.mdl")
SWEP.ViewModelFOV	= 54

SWEP.Spawnable		= true
SWEP.Slot 			= 5
SWEP.UseHands 		= true

SWEP.Primary.Damage			= 5
SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

function SWEP:Initialize()
	self:SetHoldType( "melee" )
end

function SWEP:PrimaryAttack()

	if not SERVER then return end

	self.Owner:SetAnimation( PLAYER_ATTACK1 )

	self.Weapon:SetNextPrimaryFire( CurTime() +1 )

	local ent = util.TraceLine{
		start = self.Owner:GetShootPos(),
		endpos = self.Owner:GetShootPos() +self.Owner:GetAimVector() *100,
		filter = { self, self.Owner }
	}.Entity

	if not ent:IsValid() then self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER ) return end

	if ent:IsPlayer() then

		ent:BecomeRagdoll()

        timer.Simple( 5, function()
			if IsValid( ent ) then
				ent:UnRagdoll()
				GAMEMODE.Needs:SetPlayerNeed( ent, "Stamina", 0 )
			end
		end)

        self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )

	else

		self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )

	end

end