AddCSLuaFile()

SWEP.PrintName		= "Medkit"
SWEP.Author			= "Jake Butts"
SWEP.Instructions	= "Left click to heal another player, right click to stop bleeding." 

SWEP.ViewModel 		= Model("models/weapons/c_medkit.mdl")
SWEP.WorldModel 	= Model("models/weapons/w_medkit.mdl")
SWEP.ViewModelFOV	= 54

SWEP.Spawnable		= false
SWEP.Slot 			= 5
SWEP.UseHands 		= true

SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"


function SWEP:Initialize()
	self:SetHoldType( "slam" )
end

function SWEP:PrimaryAttack()
	self.Weapon:SetNextPrimaryFire( CurTime() +1 )

	local ent = util.TraceLine{
		start = self.Owner:GetShootPos(),
		endpos = self.Owner:GetShootPos() +self.Owner:GetAimVector() *100,
		filter = { self, self.Owner }
	}.Entity

	if SERVER then
		if not self:HasInventoryAmmo() then return end
		if not IsValid( ent ) or not ent:IsPlayer() then 
			ent = self.Owner 
			if not IsValid( ent ) or not ent:IsPlayer() then return end
		end
		
		local vars = {}
		
		for k, v in pairs( GAMEMODE.PlayerDamage:GetLimbs() ) do
			GAMEMODE.PlayerDamage:SetPlayerLimbHealth( ent, k, v.MaxHealth, true )
			vars["limb_hp_".. v.Name] = true
			vars["limb_bld_".. v.Name] = true
			vars["limb_brkn_".. v.Name] = true
			vars["limb_bndg_".. v.Name] = true
		end
		
		GAMEMODE.Net:SendGameVarBatchedUpdate( ent, vars )

		ent:SetHealth(100)
		
		self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK )
		self:TakeInventoryAmmo()
		ent:EmitSound( "items/medshot4.wav" )
	end
end

function SWEP:SecondaryAttack()
	self.Weapon:SetNextSecondaryFire( CurTime() +1 )
	
	local ent = util.TraceLine{
		start = self.Owner:GetShootPos(),
		endpos = self.Owner:GetShootPos() +self.Owner:GetAimVector() *100,
		filter = { self, self.Owner }
	}.Entity

	if SERVER then
		if not self:HasInventoryAmmo() then return end
		if not IsValid( ent ) or not ent:IsPlayer() then 
			ent = self.Owner 
			if not IsValid( ent ) or not ent:IsPlayer() then return end
		end
		
		for k, v in pairs( GAMEMODE.PlayerDamage:GetLimbs() ) do
			if GAMEMODE.PlayerDamage:IsPlayerLimbBleeding( ent, k ) then
				GAMEMODE.PlayerDamage:SetPlayerLimbBleeding( ent, k, false )
				break
			end
		end

		self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK )
		self:TakeInventoryAmmo()
		ent:EmitSound( "items/medshot4.wav" )
	end
end

function SWEP:TakeInventoryAmmo()
	if SERVER then
		local govItem = "Government Issue Medical Supplies"
		local civItem = "Medical Supplies"

		if not GAMEMODE.Inv:TakePlayerItem( self.Owner, govItem, 1 ) then
			GAMEMODE.Inv:TakePlayerItem( self.Owner, civItem, 1 )
		end

		if GAMEMODE.Inv:GetPlayerItemAmount( self.Owner, govItem ) <= 0 then
			if GAMEMODE.Inv:GetPlayerItemAmount( self.Owner, civItem ) <= 0 then
				local has, slot = GAMEMODE.Inv:PlayerHasItemEquipped( self.Owner, "Medkit" )

				if has then
					if GAMEMODE.Inv:GivePlayerItem( self.Owner, self.Owner:GetEquipSlot(slot) ) then
						GAMEMODE.Inv:DeletePlayerEquipItem( self.Owner, slot )
					end
				end
			end
		end
	end
end

function SWEP:HasInventoryAmmo()
	local govItem = "Government Issue Medical Supplies"
	local civItem = "Medical Supplies"

	if SERVER then
		if GAMEMODE.Inv:GetPlayerItemAmount( self.Owner, govItem ) > 0 then return true end
		return GAMEMODE.Inv:GetPlayerItemAmount( self.Owner, civItem ) > 0
	else
		if self.Owner ~= LocalPlayer() then return true end
		if GAMEMODE.Inv:PlayerHasItem( govItem, 1 ) then return true end
		if GAMEMODE.Inv:PlayerHasItem( civItem, 1 ) then return true end
		return false
	end
end

function SWEP:FireAnimationEvent( _, _, intEvent )
	if intEvent == 5001 or intEvent == 6001 then return true end
end

function SWEP:Think()
end