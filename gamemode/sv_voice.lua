--[[
	Name: sv_voice.lua
	For: SantosRP.Life
	By: Jake Butts
]]--

GM.Voice = (GAMEMODE or GM).Voice or {}
GM.Voice.Modes = (GAMEMODE or GM).Voice.Modes or {}

GM.Voice.Options = {
	"Whisper",
	"Talk",
	"Yell"
}

GM.Voice.Distances = {
	["Whisper"] = 150,
	["Talk"] = 500,
	["Yell"] = 800,
}

function GM.Voice:GetTalkingMode( pPlayer )
	local mode = self.Modes[pPlayer]
	if not mode then
		return 2
	end
	return mode
end

function GM.Voice:PlayerCanHearPlayersVoice( pListener, pTalker )
	local mode = self:GetTalkingMode( pTalker )
	local str = self.Options[mode]
	local distance = self.Distances[ str ]
	return ( pTalker:GetPos():Distance( pListener:GetPos() ) <= distance ), true
end

function GM.Voice:PlayerChangeMode( pPlayer, intVoiceMode )
	if intVoiceMode < 1 or intVoiceMode > 3 then return end
	self.Modes[pPlayer] = intVoiceMode

	pPlayer:AddNote( "Voice Mode Changed: ".. self.Options[ self:GetTalkingMode( pPlayer ) ] )

	GAMEMODE.Net:VoiceChanged( pPlayer, intVoiceMode )
end