--[[
	Name: misc_damond.lua
	For: OakRoleplay
	By: Jake Butts
]]--

local Item = {}
Item.Name = "Pill of Revitalization"
Item.Desc = "Take to return to normal"
Item.Type = "type_drugs"
Item.Model = "models/props_c17/canister_propane01a.mdl"
Item.Weight = 2
Item.Volume = 1
Item.CanDrop = false
Item.CanUse = true
Item.LimitID = "pill_rev"
Item.OnUse = function( tblItem, pPlayer )
	GAMEMODE.Jobs:GetJobByID( JOB_CIVILIAN ):PlayerSetModel( pPlayer )
end

GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 8, { ["vip"] = 4 } )

local Item = {}
Item.Name = "Pill of Crowification"
Item.Desc = "Take to turn into a crow"
Item.Type = "type_drugs"
Item.Model = "models/props_c17/canister_propane01a.mdl"
Item.Weight = 2
Item.Volume = 1
Item.CanDrop = false
Item.CanUse = true
Item.LimitID = "pill_rev"
Item.OnUse = function( tblItem, pPlayer )

	pPlayer:SetModel( "models/crow.mdl" )

end

GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 8, { ["vip"] = 4 } )

local Item = {}
Item.Name = "Pill of Pigeonification"
Item.Desc = "Take to turn into a pigeon"
Item.Type = "type_drugs"
Item.Model = "models/props_c17/canister_propane01a.mdl"
Item.Weight = 2
Item.Volume = 1
Item.CanDrop = false
Item.CanUse = true
Item.LimitID = "pill_rev"
Item.OnUse = function( tblItem, pPlayer )

	pPlayer:SetModel( "models/pigeon.mdl" )

end

GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 8, { ["vip"] = 4 } )

local Item = {}
Item.Name = "Pill of Seagullification"
Item.Desc = "Take to turn into a pigeon"
Item.Type = "type_drugs"
Item.Model = "models/props_c17/canister_propane01a.mdl"
Item.Weight = 2
Item.Volume = 1
Item.CanDrop = false
Item.CanUse = true
Item.LimitID = "pill_rev"
Item.OnUse = function( tblItem, pPlayer )

	pPlayer:SetModel( "models/seagull.mdl" )

end

GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 8, { ["vip"] = 4 } )