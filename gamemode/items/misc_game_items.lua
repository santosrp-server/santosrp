--[[
	Name: misc_game_items.lua
	For: TalosLife
	By: TalosLife
]]--

local Item = {}
Item.Name = "Medical Supplies"
Item.Desc = "A vial of essential medical supplies, consumed by the first aid kit."
Item.Model = "models/healthvial.mdl"
Item.Type = "type_ammo"
Item.Weight = 2
Item.Volume = 2
Item.CanDrop = true
Item.LimitID = "medical supplies"
 
Item.CraftingEntClass = "ent_crafting_table"
Item.CraftSkill = "Crafting"
Item.CraftSkillLevel = 5
Item.CraftSkillXP = 5
Item.CraftRecipe = {
	["Metal Plate"] = 2,
	["Cloth"] = 4,
}
GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 12 )


local Item = {}
Item.Name = "Government Issue Medical Supplies"
Item.Desc = "A vial of essential medical supplies, consumed by the first aid kit."
Item.Model = "models/healthvial.mdl"
Item.Type = "type_ammo"
Item.JobItem = "JOB_EMS" --This item can only be possessed by by players with this job
Item.Weight = 2
Item.Volume = 2
Item.CanDrop = false
GM.Inv:RegisterItem( Item )

local Item = {}
Item.Name = "Money Bag"
Item.Desc = "A stolen bag of money, don't get caught carrying this!"
Item.Model = "models/freeman/duffel_bag.mdl"
Item.DropClass = "ent_moneybag"
Item.NoBankStorage = true
Item.Weight = 25
Item.Volume = 100
Item.CanDrop = true
Item.Illegal = true
GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 20 )

local Item = {}
Item.Name = "Fuel Can"
Item.Desc = "A can of fuel."
Item.Model = "models/props_junk/gascan001a.mdl"
Item.Weight = 10
Item.Volume = 8
Item.CanDrop = true
Item.LimitID = "fuel can"
Item.DropClass = "ent_fuelcan"
GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 2 )


local Item = {}
Item.Name = "Storage Chest"
Item.Desc = "A large chest for storing items temporally."
Item.Model = "models/Items/ammocrate_smg1.mdl"
Item.Weight = 45
Item.Volume = 70
Item.HealthOverride = 10000
Item.CanDrop = true
Item.CollidesWithCars = true
Item.LimitID = "storage chest"
Item.DropClass = "ent_storage_chest"
GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 2, { ["vip"] = 1 } )


local Item = {}
Item.Name = "Road Flare"
Item.Desc = "A road flare, drop and press E on to ignite."
Item.Model = "models/props_junk/flare.mdl"
Item.Weight = 2
Item.Volume = 1
Item.CanDrop = true
Item.LimitID = "road flare"
Item.DropClass = "ent_roadflare"
Item.DrugLab_BlenderVars = {
	BlendProgress = 10,
	BlendAmountPerTick = 0.15,
	GiveItem = "Red Phosphorous",
	GiveAmount = 4,
}
GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 3 )


local Item = {}
Item.Name = "Engine Overhaul"
Item.Desc = "A complete engine overhaul kit. Used on vehicles that have 0 health."
Item.Model = "models/props_c17/TrapPropeller_Engine.mdl"
Item.Weight = 100
Item.Volume = 80
Item.CanDrop = true
Item.LimitID = "car repair kit"
Item.DropClass = "ent_carfix_high"

Item.CraftingEntClass = "ent_crafting_table"
Item.CraftSkill = "Crafting"
Item.CraftSkillLevel = 15
Item.CraftSkillXP = 5
Item.CraftRecipe = {
	["Engine Block"] = 1,
	["Car Battery"] = 1,
	["Metal Plate"] = 2,
	["Metal Pipe"] = 1,
	["Crowbar"] = 1,
	["Wrench"] = 2,
	["Pliers"] = 1,
}
GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 2 )


local Item = {}
Item.Name = "Vehicle Repair Kit"
Item.Desc = "Tools and components for repairing general vehicle damage."
Item.Model = "models/props/cs_office/Cardboard_box01.mdl"
Item.Weight = 80
Item.Volume = 25
Item.CanDrop = true
Item.LimitID = "car repair kit"
Item.DropClass = "ent_carfix_medium"

Item.CraftingEntClass = "ent_crafting_table"
Item.CraftSkill = "Crafting"
Item.CraftSkillLevel = 1
Item.CraftSkillXP = 5
Item.CraftRecipe = {
	["Car Battery"] = 8,
	["Metal Plate"] = 10,
	["Metal Pipe"] = 5,
	["Crowbar"] = 5,
	["Wrench"] = 5,
	["Pliers"] = 1,
}
GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 2 )


local Item = {}
Item.Name = "Roadside Quick-Fix Kit"
Item.Desc = "A small set of parts and a few tools for fixing light damage to vehicles."
Item.Model = "models/props_c17/BriefCase001a.mdl"
Item.Weight = 30
Item.Volume = 10
Item.CanDrop = true
Item.LimitID = "car repair kit"
Item.DropClass = "ent_carfix_light"

Item.CraftingEntClass = "ent_crafting_table"
Item.CraftSkill = "Crafting"
Item.CraftSkillLevel = 1
Item.CraftSkillXP = 5
Item.CraftRecipe = {
	["Metal Plate"] = 2,
	["Metal Pipe"] = 4,
	["Wrench"] = 3,
	["Pliers"] = 3,
}
GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 2 )


local Item = {}
Item.Name = "Spare Tire Kit"
Item.Desc = "A replacement set of tires for any vehicle."
Item.Model = "models/props_vehicles/carparts_wheel01a.mdl"
Item.Weight = 60
Item.Volume = 80
Item.CanDrop = true
Item.LimitID = "car repair kit"
Item.DropClass = "ent_carfix_wheels"

Item.CraftingEntClass = "ent_crafting_table"
Item.CraftSkill = "Crafting"
Item.CraftSkillLevel = 1
Item.CraftSkillXP = 5
Item.CraftRecipe = {
	["Metal Plate"] = 2,
	["Metal Bracket"] = 4,
	["Wrench"] = 1,
	["Rubber Tire"] = 4,
}
GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 2 )


local Item = {}
Item.Name = "Police Issue Roadside Quick-Fix Kit"
Item.Desc = "A small set of parts and a few tools for fixing light damage to vehicles."
Item.Model = "models/props_c17/BriefCase001a.mdl"
Item.Weight = 30
Item.Volume = 10
Item.CanDrop = true
Item.JobItem = "JOB_POLICE" --This item can only be possessed by by players with this job
Item.LimitID = "car repair kit"
Item.DropClass = "ent_carfix_light"
GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 2 )


local Item = {}
Item.Name = "Police Issue Spare Tire Kit"
Item.Desc = "A replacement set of tires for any vehicle."
Item.Model = "models/props_vehicles/carparts_wheel01a.mdl"
Item.Weight = 60
Item.Volume = 80
Item.CanDrop = true
Item.JobItem = "JOB_POLICE" --This item can only be possessed by by players with this job
Item.LimitID = "car repair kit"
Item.DropClass = "ent_carfix_wheels"
GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 2 )


local Item = {}
Item.Name = "Paper"
Item.Desc = "Some paper."
Item.Model = "models/props_lab/bindergraylabel01a.mdl"
Item.Weight = 10
Item.Volume = 8
Item.CanDrop = true
Item.LimitID = "paper"
Item.DropClass = "ent_paper"
GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 2 )