--[[
	Name: counterfeit_items.lua
	For: TalosLife
	By: TalosLife
]]--

local Item = {}
Item.Name = "Ink Vial"
Item.Desc = "A vial of ink."
Item.Model = "models/Combine_Helicopter/helicopter_bomb01.mdl"
Item.Weight = 2
Item.Volume = 2
Item.CanDrop = true
Item.LimitID = "ink"
Item.DropClass = "prop_physics"

Item.SetupEntity = function( _, eEnt )
	eEnt:SetModelScale( 0.2 )
	eEnt:Activate()
end

GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 12 )

local Item = {}
Item.Name = "Linen"
Item.Desc = "A supply of linen"
Item.Model = "models/props_c17/lampShade001a.mdl"
Item.Weight = 2
Item.Volume = 2
Item.CanDrop = true
Item.LimitID = "linen"
Item.DropClass = "prop_physics"

Item.SetupEntity = function( _, eEnt )
	eEnt:SetModelScale( 0.5 )
	eEnt:Activate()
end

GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 12 )

local Item = {}
Item.Name = "Cotton"
Item.Desc = "A supply of cotton"
Item.Model = "models/props/cs_office/trash_can_p5.mdl"
Item.Weight = 2
Item.Volume = 2
Item.CanDrop = true
Item.LimitID = "cotton"
Item.DropClass = "prop_physics"

GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 12 )

local Item = {}
Item.Name = "Printing Press"
Item.Desc = "A template for bank notes"
Item.Model = "models/props/de_nuke/NuclearFuelContainer.mdl"
Item.Weight = 2
Item.Volume = 2
Item.CanDrop = true
Item.Illegal = true
Item.LimitID = "printing_press"
Item.DropClass = "ent_print_printpress"

GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 12 )

local Item = {}
Item.Name = "Printer"
Item.Desc = "An ink printer"
Item.Model = "models/freeman/money_printer.mdl"
Item.Weight = 2
Item.Volume = 2
Item.CanDrop = true
Item.Illegal = true
Item.LimitID = "printer"
Item.DropClass = "ent_print_printer"

Item.CraftingEntClass = "ent_crafting_table"
Item.CraftingTab = "Machines"
Item.CraftSkill = "Crafting"
Item.CraftSkillLevel = 7
Item.CraftSkillXP = 10
Item.CraftRecipe = {
	["Metal Bracket"] = 8,
	["Metal Plate"] = 10,
	["Metal Pipe"] = 3,
	["Chunk of Plastic"] = 5,
	["Car Battery"] = 2,
}

GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 1 )

local Item = {}
Item.Name = "Counterfeit Money"
Item.Desc = "Money made illegally, sell it quick!"
Item.Model = "models/props/cs_assault/Money.mdl"
Item.Weight = 2
Item.Volume = 2
Item.CanDrop = true
Item.Illegal = true
Item.LimitID = "counter_money"
Item.DropClass = "prop_physics"

Item.SetupEntity = function( _, eEnt )
	eEnt:SetColor( Color(135,206,250,150) )
end

GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 12 )