--[[
	Name: furniture_hl2.lua
	For: TalosLife
	By: TalosLife
]]--

local Item = {}
Item.Name = "Metal Ladder"
Item.Desc = "Climb Things."
Item.Type = "type_furniture"
Item.Model = "models/props_c17/metalladder001.mdl"
Item.Weight = 200
Item.Volume = 50
Item.HealthOverride = 3000
Item.CanDrop = true
Item.DropClass = "prop_physics"

GM.Inv:RegisterItem( Item )

local Item = {}
Item.Name = "Wooden Ladder"
Item.Desc = "Climb Things."
Item.Type = "type_furniture"
Item.Model = "models/props_forest/ladderwood.mdl"
Item.Weight = 350
Item.Volume = 50
Item.HealthOverride = 500
Item.CanDrop = true
Item.DropClass = "prop_physics"

GM.Inv:RegisterItem( Item )