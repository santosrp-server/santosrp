local Item = {}
Item.Name = "C4"
Item.Desc = "An Item that can be used to open bank vaults."
Item.Model = "models/weapons/w_c4.mdl"
Item.Weight = 10
Item.Volume = 5
Item.CanDrop = true
Item.Illegal = true
Item.DropClass = "ent_bank_c4"

Item.CraftingEntClass = "ent_assembly_table"
Item.CraftSkill = "Gun Smithing"
Item.CraftSkillLevel = 10
Item.CraftSkillXP = 20
Item.CraftRecipe = {
	["Smokeless Gunpowder"] = 8,
	["Nitrocellulose"] = 12,
	["Metal Pipe"] = 15,
	["Metal Bar"] = 15,
    ["Aluminum Powder"] = 8,
    ["Pliers"] = 5,
	["C4 Casing"] = 1,
}
GM.Inv:RegisterItem( Item )

local Item = {}
Item.Name = "C4 Casing"
Item.Desc = "An timer to be used for C4"
Item.Model = "models/weapons/w_c4.mdl"
Item.Weight = 2
Item.Volume = 5
Item.CanDrop = true
Item.Illegal = true
Item.DropClass = "prop_physics"
GM.Inv:RegisterItem( Item )