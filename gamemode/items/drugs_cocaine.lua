--[[
	Name: drugs_cocaine.lua
	For: OakRP
	By: Robbot
]]--


--Drug components
local Item = {}
Item.Name = "Coca Leaves"
Item.Desc = "The starting item for Cocaine."
Item.Model = "models/oldbill/ahleaves.mdl"
Item.Weight = 4
Item.Volume = 6
Item.CanDrop = true
Item.Illegal = true
Item.LimitID = "coca leaves"
Item.DropClass = "prop_physics"
Item.DrugLab_BlenderVars = {
	BlendProgress = 10,
	BlendAmountPerTick = 0.1,
	GiveItem = "Coca Mush",
	GiveAmount = 1,
}
GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 1, { ["vip"] = 1 } )

local Item = {}
Item.Name = "Coca Mush"
Item.Desc = "A jar of powdered coca leaves."
Item.Model = "models/props_lab/jar01b.mdl"
Item.Weight = 2
Item.Volume = 4
Item.CanDrop = true
Item.Illegal = true
Item.LimitID = "coca mush"
Item.DropClass = "ent_fluid_coca_mush"
GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 2, { ["vip"] = 1 } )

local Item = {}
Item.Name = "Baking Soda"
Item.Desc = "A bag of Sodium Hydrogen Carbonate."
Item.Model = "models/weapons/w_package.mdl"
Item.Weight = 2
Item.Volume = 4
Item.CanDrop = true
Item.DropClass = "prop_physics"
GM.Inv:RegisterItem( Item )

local Item = {}
Item.Name = "Crack Cocaine Tray"
Item.Desc = "A tray of Cocaine and Baking soda."
Item.Model = "models/srcocainelab/cocainebrick.mdl"
Item.Weight = 2
Item.Volume = 4
Item.CanDrop = true
Item.Illegal = true
Item.CanCook = true
Item.DropClass = "prop_physics"
Item.CanCook = true

Item.Cooking_OvenVars = {
	Skill = "Chemistry",
	SkillWeight = 0,

	OvenProgress = 7,
	OvenAmountPerTick = 0.1,
	OvenMaxOverTime = 60, --Max time after being done before starting a fire
	GiveItem = "Crack Cocaine",
	GiveItems = {
		{ MinQuality = 0, GiveItem = "Crack Cocaine", GiveAmount = 1 },
	},

	GiveXP = { --In order from 0 score up
		{ MinQuality = 0, GiveAmount = 10 },
	},
}

Item.CraftingEntClass = "ent_crafting_table"
Item.CraftSkill = "Chemistry"
Item.CraftSkillLevel = 11
Item.CraftSkillXP = 10
Item.CraftRecipe = {
	["Baking Soda"] = 1,
	["Cocaine (High Quality)"] = 1,
}
GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 2, { ["vip"] = 1 } )

local Item = {}
Item.Name = "Crack Cocaine"
Item.Desc = "A brick of Crack Cocaine. The drug dealer loves this!"
Item.Model = "models/oldbill/ahcocaine.mdl"
Item.Type = "type_drugs"
Item.Weight = 1
Item.Volume = 1
Item.CanDrop = true
Item.Illegal = true
Item.LimitID = "crack cocaine"
Item.DropClass = "prop_physics"
GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 2, { ["vip"] = 1 } )


local Item = {}
Item.Name = "Cocaine Paste"
Item.Desc = "A jar of thick cocaine paste that needs to be processed."
Item.Type = "type_drugs"
Item.Model = "models/props_lab/jar01b.mdl"
Item.Weight = 5
Item.Volume = 3
Item.CanDrop = true
Item.Illegal = true
Item.DropClass = "ent_fluid_cocaine_paste"
Item.LimitID = "cocaine paste"
Item.ReactionChamberVars = {
	Mats = { --Items needed to make a single MakeAmount of the output fluid
		["Ammonia"] = 1,
		["Coca Mush"] = 2,
	},
	Interval = 0.75, --Interval
	MakeAmount = 2, --Amount of fluid to make per interval
	MinGiveAmount = 500, --Amount of the output fluid needed to give a player an item
	GiveItem = "Cocaine Paste",
	GiveAmount = 1,
}
GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 2, { ["vip"] = 1 } )

--Drug items
local Item = {}
Item.Name = "Cocaine ( Low Quality)"
Item.Desc = "A bag of low quality cocaine."
Item.Type = "type_drugs"
Item.Model = "models/srcocainelab/cocainebrick.mdl"
Item.Weight = 1
Item.Volume = 1
Item.CanDrop = true
Item.CanUse = true
Item.Illegal = true
Item.LimitID = "cocaine"
Item.DropClass = "prop_physics"

Item.OnUse = function( _, pPlayer )
	GAMEMODE.Drugs:PlayerApplyEffect( pPlayer, "Cocaine", 1 *60, 1 ) --Check if this effect "cocaine" links with the cocaineEffect
	GAMEMODE.Needs:AddPlayerNeed( pPlayer, "Stamina", 25 )
end
Item.SetupEntity = function( _, eEnt )
	eEnt.CanPlayerPickup = Item.CanPlayerPickup
	eEnt.CanPlayerUse = Item.CanPlayerUse
end
Item.CanPlayerPickup = function( eEnt, pPlayer, bCanUse )
	return true --Anyone can take drugs!
end
Item.CanPlayerUse = function( eEnt, pPlayer, bCanUse )
	return true --Anyone can take drugs!
end
Item.CookingPotVars = { --Recipe for Cocaine
	Skill = "Chemistry",
	SkillWeight = 0.39, --max % to remove from the score in the worst case

	--Only displayed if over time explode/fire are both off and item is grabbed before it reaches the max time cap
	--anything catches fire past the max time cap (set in the cooking pot shared file)
	OverCookMsg = "You overcooked it! This batch is ruined!",
	OverTimeExplode = true, --Explode and start a fire if the item goes over max time
	OverTimeStartFire = false, --Start a fire if the item goes over max time

	MinTime = 10,
	MaxTime = 120,
	TimeWeight = -0.85, --the closer this number is to 0, the less impact time has on the end score (-4 = 0/100% impact do not go below)
	IdealTimePercent = 0.75, --% from min to max time to consider ideal

	Items = {},
	Fluids = {
		["Cocaine Paste"] = { IdealAmount = 700, MaxAmount = 750, MinAmount = 600 },
		["Water"] = { IdealAmount = 100, MaxAmount = 150, MinAmount = 50 },
		["Methanol"] = { IdealAmount = 400, MaxAmount = 450, MinAmount = 350 },
	},
	GiveItems = { --In order from low quality to high quality (enter only 1 for no quality)
		{ MinQuality = 0.0, GiveItem = "Cocaine (Medium Quality)", GiveAmount = 4 },
		{ MinQuality = 0.72, GiveItem = "Cocaine (High Quality)", GiveAmount = 8 },
	},
	GiveXP = { --In order from 0 score up
		{ MinQuality = 0.0, GiveAmount = 8 },
		{ MinQuality = 0.72, GiveAmount = 9 },
	}
}
GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 8, { ["vip"] = 4 } )

local Item = {}
Item.Name = "Cocaine (Medium Quality)"
Item.Desc = "A bag of medium quality cocaine."
Item.Type = "type_drugs"
Item.Model = "models/srcocainelab/cocainebrick.mdl"
Item.Weight = 1
Item.Volume = 1
Item.CanDrop = true
Item.CanUse = true
Item.Illegal = true
Item.LimitID = "cocaine"
Item.DropClass = "prop_physics"

Item.OnUse = function( _, pPlayer )
	GAMEMODE.Drugs:PlayerApplyEffect( pPlayer, "Cocaine", 2 *60, 2 )
	GAMEMODE.Needs:AddPlayerNeed( pPlayer, "Stamina", 50 )
end
Item.SetupEntity = function( _, eEnt )
	eEnt.CanPlayerPickup = Item.CanPlayerPickup
	eEnt.CanPlayerUse = Item.CanPlayerUse
end
Item.CanPlayerPickup = function( eEnt, pPlayer, bCanUse )
	return true --Anyone can take drugs!
end
Item.CanPlayerUse = function( eEnt, pPlayer, bCanUse )
	return true --Anyone can take drugs!
end
GM.Inv:RegisterItem( Item )

local Item = {}
Item.Name = "Cocaine (High Quality)"
Item.Desc = "A bag of high quality cocaine."
Item.Type = "type_drugs"
Item.Model = "models/srcocainelab/cocainebrick.mdl"
Item.Weight = 1
Item.Volume = 1
Item.CanDrop = true
Item.CanUse = true
Item.Illegal = true
Item.LimitID = "cocaine"
Item.DropClass = "prop_physics"

Item.OnUse = function( _, pPlayer )
	GAMEMODE.Drugs:PlayerApplyEffect( pPlayer, "Cocaine", 4 *60, 4 )
	GAMEMODE.Needs:AddPlayerNeed( pPlayer, "Stamina", 75 )
end
Item.SetupEntity = function( _, eEnt )
	eEnt.CanPlayerPickup = Item.CanPlayerPickup
	eEnt.CanPlayerUse = Item.CanPlayerUse
end
Item.CanPlayerPickup = function( eEnt, pPlayer, bCanUse )
	return true --Anyone can take drugs!
end
Item.CanPlayerUse = function( eEnt, pPlayer, bCanUse )
	return true --Anyone can take drugs!
end
GM.Inv:RegisterItem( Item )


--[[ Cocaine Effect ]]--
local cocaineEffect = {}
cocaineEffect.Name = "Cocaine"
cocaineEffect.NiceName = "Cocaine"
cocaineEffect.EffectName = "High (Coke)"
cocaineEffect.Image = "santosrp/ae_icons/mild amphetamine stimulation 48x48.png"
cocaineEffect.MaxPower = 25


function cocaineEffect:OnStart( pPlayer ) 
	if SERVER then
		if GAMEMODE.Drugs:GetPlayerEffectPower( pPlayer, self.Name ) > self.MaxPower *0.5 then
			if math.random( 1, 2 ) == 1 then
				self:MakePlayerVomit( pPlayer )
			end
		end

		GAMEMODE.Player:ModifyMoveSpeed( pPlayer, "CokeSpeed", 0, 120 )
		if pPlayer:Health() == 100 then
			pPlayer:SetHealth( 125 )
		end		

		if GAMEMODE.Drugs:GetPlayerEffectPower( pPlayer, self.Name ) >= self.MaxPower then
			if not pPlayer:IsUncon() then
				pPlayer:GoUncon()
			end
		end
	end
end

function cocaineEffect:Think( pPlayer )
	if SERVER then

		if GAMEMODE.Drugs:GetPlayerEffectPower( pPlayer, self.Name ) > self.MaxPower *0.25 then
			if not pPlayer.m_intLastRandomVomit then
				pPlayer.m_intLastRandomVomit = CurTime() +math.random( 5, 10 )
			end

			if CurTime() > pPlayer.m_intLastRandomVomit then
				local offset = self.MaxPower *0.25
				local scalar = Lerp( (GAMEMODE.Drugs:GetPlayerEffectPower(pPlayer, self.Name) -offset) /(self.MaxPower -offset), 1, 0.33 )
				pPlayer.m_intLastRandomVomit = CurTime() +math.random( 60 *scalar, 120 *scalar )
				self:MakePlayerVomit( pPlayer )
			end
		end
	end
end

function cocaineEffect:OnStop( pPlayer )
	if CLIENT then return end
	
	if GAMEMODE.Drugs:GetPlayerEffectPower( pPlayer, self.Name ) <= self.MaxPower *0.25 then
		pPlayer.m_intLastRandomVomit = nil
	end
end

local randomSounds = {
	Sound( "npc/zombie/zombie_pain1.wav" ),
	Sound( "npc/zombie/zombie_pain3.wav" ),
	Sound( "npc/barnacle/barnacle_die1.wav" ),
}
function cocaineEffect:MakePlayerVomit( pPlayer )
	if not pPlayer:Alive() or pPlayer:IsRagdolled() then return end
	
	local snd = table.Random( randomSounds )
	pPlayer:EmitSound( snd )

	timer.Create( ("effect_vomit_%p"):format(pPlayer), 0.1, 10, function()
		if not IsValid( pPlayer ) or not pPlayer:Alive() then return end
		
		local effectData = EffectData()
		effectData:SetOrigin( pPlayer:GetShootPos() +(pPlayer:EyeAngles():Forward() *8) )
		effectData:SetNormal( pPlayer:GetAimVector() )
		util.Effect( "vomitSpray", effectData )
		pPlayer:ViewPunch( Angle(10, 0, 0) )
	end )
end

if CLIENT then
	local drugColor = {
		["$pp_colour_addr"] = 0,
		["$pp_colour_addg"] = 0,
		["$pp_colour_addb"] = 0,
		["$pp_colour_brightness"] = 0,
		["$pp_colour_contrast"] = 1,
		["$pp_colour_colour"] = 1,
		["$pp_colour_mulr"] = 0,
		["$pp_colour_mulg"] = 0,
		["$pp_colour_mulb"] = 0
	}

	function cocaineEffect:RenderScreenspaceEffects()
		local pow = math.min( GAMEMODE.Drugs:GetEffectPower(self.Name), self.MaxPower )

		drugColor["$pp_colour_colour"] = Lerp( pow /self.MaxPower *2, 1, 1.2 )
		DrawColorModify( drugColor )
		DrawToyTown( Lerp(pow /self.MaxPower, 1, 5), ScrH() *Lerp(pow /self.MaxPower, 0.5, 1) )
	end

	function cocaineEffect:GetMotionBlurValues( intW, intH, intForward, intRot )
		local pow = math.min( GAMEMODE.Drugs:GetEffectPower(self.Name), self.MaxPower )
		local speed = Lerp( pow /self.MaxPower, 0.5, 1.5 )
		local strength = Lerp( pow /self.MaxPower, 0.01, 0.05 )
		intRot = intRot +math.sin( CurTime() *speed ) *strength

		return intW, intH, intForward, intRot
	end
end
GM.Drugs:RegisterEffect( cocaineEffect ) --Check if this or line 711 is needed too