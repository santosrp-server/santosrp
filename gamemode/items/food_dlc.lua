--[[
	Name: food_dlc.lua
	For:OakRP
	By:Robbot
]]--

local function PlayerEatItem( tblItem, pPlayer )
	if tblItem.GiveHunger >= 0 then
		GAMEMODE.Needs:AddPlayerNeed( pPlayer, "Hunger", tblItem.GiveHunger )
	else
		GAMEMODE.Needs:TakePlayerNeed( pPlayer, "Hunger", math.abs(tblItem.GiveHunger) )
	end

	if tblItem.GiveStamina then
		if tblItem.GiveStamina >= 0 then
			GAMEMODE.Needs:AddPlayerNeed( pPlayer, "Stamina", tblItem.GiveStamina )
		else
			GAMEMODE.Needs:TakePlayerNeed( pPlayer, "Stamina", math.abs(tblItem.GiveStamina) )
		end
	end
	
	pPlayer:EmitSound( "santosrp/eating.mp3" )
end

local function PlayerDrinkItem( tblItem, pPlayer )
	if tblItem.GiveThirst >= 0 then
		GAMEMODE.Needs:AddPlayerNeed( pPlayer, "Thirst", tblItem.GiveThirst )
	else
		GAMEMODE.Needs:TakePlayerNeed( pPlayer, "Thirst", math.abs(tblItem.GiveThirst) )
	end

	if tblItem.GiveStamina then
		if tblItem.GiveStamina >= 0 then
			GAMEMODE.Needs:AddPlayerNeed( pPlayer, "Stamina", tblItem.GiveStamina )
		else
			GAMEMODE.Needs:TakePlayerNeed( pPlayer, "Stamina", math.abs(tblItem.GiveStamina) )
		end
	end

	if tblItem.IsCola then
		GAMEMODE.Drugs:PlayerApplyEffect( pPlayer, "cola", 1 *15, 1 )
	end
	
	pPlayer:EmitSound( "npc/barnacle/barnacle_gulp".. math.random(1, 2).. ".wav", 60, math.random(70, 130) )
end

local function PlayerCanEatItem( tblItem, pPlayer )
	if GAMEMODE.Needs:GetPlayerNeed( pPlayer, "Hunger" ) >= GAMEMODE.Needs:GetNeedData( "Hunger" ).Max then
		return tblItem.GiveHunger <= 0
	end
end

local function PlayerCanDrinkItem( tblItem, pPlayer )
	if GAMEMODE.Needs:GetPlayerNeed( pPlayer, "Thirst" ) >= GAMEMODE.Needs:GetNeedData( "Thirst" ).Max then
		return tblItem.GiveThirst <= 0
	end
end

local Item = {}
Item.Name = "Coca Cola"
Item.Desc = "A small can of Coca Cola, Sure to give you a jolt of energy."
Item.Type = "type_food"
Item.Model = "models/foodnhouseholditems/sodacan01.mdl"
Item.Weight = 1
Item.Volume = 2
Item.CanDrop = true
Item.CanUse = true
Item.LimitID = "soda"
Item.DropClass = "prop_physics"
Item.GiveThirst = 50
Item.GiveStamina = 15
Item.IsCola = true
Item.OnUse = PlayerDrinkItem
Item.PlayerCanUse = PlayerCanDrinkItem
GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 8, { ["vip"] = 4 } )

local Item = {}
Item.Name = "Monster"
Item.Desc = "A small can of Monster, Sure to give you a jolt of energy."
Item.Type = "type_food"
Item.Model = "models/foodnhouseholditems/sodacanb01.mdl"
Item.Weight = 1
Item.Volume = 2
Item.CanDrop = true
Item.CanUse = true
Item.LimitID = "soda"
Item.DropClass = "prop_physics"
Item.GiveThirst = 75
Item.GiveStamina = 15
Item.IsCola = true
Item.OnUse = PlayerDrinkItem
Item.PlayerCanUse = PlayerCanDrinkItem
GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 8, { ["vip"] = 4 } )

local Item = {}
Item.Name = "Half Watermelon"
Item.Desc = "A sliced watermelon."
Item.Type = "type_food"
Item.Model = "models/foodnhouseholditems/watermelon_half.mdl"
Item.Weight = 2
Item.Volume = 3
Item.CanDrop = true
Item.CanUse = true
Item.CanCook = true
Item.DropClass = "prop_physics"
Item.GiveHunger = 125
Item.OnUse = PlayerEatItem
Item.PlayerCanUse = PlayerCanEatItem
GM.Inv:RegisterItem( Item )

local Item = {}
Item.Name = "Watermelon Slice"
Item.Desc = "A slice of Watermelon."
Item.Type = "type_food"
Item.Model = "models/foodnhouseholditems/watermelon_slice.mdl"
Item.Weight = 1
Item.Volume = 1
Item.CanDrop = true
Item.CanUse = true
Item.CanCook = true
Item.DropClass = "prop_physics"
Item.GiveHunger = 75
Item.OnUse = PlayerEatItem
Item.PlayerCanUse = PlayerCanEatItem
GM.Inv:RegisterItem( Item )

local Item = {}
Item.Name = "Sweet Roll"
Item.Desc = "A sweet pastry"
Item.Type = "type_food"
Item.Model = "models/foodnhouseholditems/sweetroll.mdl"
Item.Weight = 2
Item.Volume = 3
Item.CanDrop = true
Item.CanUse = true
Item.CanCook = true
Item.DropClass = "prop_physics"
Item.GiveHunger = 100
Item.OnUse = PlayerEatItem
Item.PlayerCanUse = PlayerCanEatItem
GM.Inv:RegisterItem( Item )

local Item = {}
Item.Name = "Uncooked Turkey"
Item.Desc = "You will need to cook this to eat it."
Item.Type = "type_food"
Item.Model = "models/foodnhouseholditems/turkey.mdl"
Item.Weight = 4
Item.Volume = 3
Item.CanDrop = true
Item.CanUse = true
Item.CanCook = true
Item.DropClass = "prop_physics"
Item.GiveHunger = 50
Item.GiveStamina = 3
Item.OnUse = PlayerEatItem
Item.PlayerCanUse = PlayerCanEatItem
Item.Cooking_OvenVars = {
	Skill = "Cooking",
	SkillWeight = 1,

	OvenProgress = 7,
	OvenAmountPerTick = 0.1,
	OvenMaxOverTime = 60, --Max time after being done before starting a fire
	GiveItem = "Cooked Turkey",
	GiveItems = {
		{ MinQuality = 1, GiveItem = "Cooked Turkey", GiveAmount = 1 },
	},

	GiveXP = { --In order from 0 score up
		{ MinQuality = 1, GiveAmount = 10 },
	},
}
GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 2, { ["vip"] = 1 } )

local Item = {}
Item.Name = "Cooked Turkey"
Item.Desc = "A delicious Cooked Turkey"
Item.Type = "type_food"
Item.Model = "models/foodnhouseholditems/turkey2.mdl"
Item.Weight = 4
Item.Volume = 3
Item.CanDrop = true
Item.CanUse = true
Item.DropClass = "prop_physics"
Item.GiveHunger = 500
Item.GiveStamina = 10
Item.OnUse = PlayerEatItem
Item.PlayerCanUse = PlayerCanEatItem
GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 2, { ["vip"] = 1 } )


--[[ Cola Effect ]]--
local colaEffect = {}
colaEffect.Name = "cola"
colaEffect.NiceName = "Energized"
colaEffect.EffectName = "Energized"
colaEffect.Image = "santosrp/ae_icons/brain damage 48x48.png"
colaEffect.MaxPower = 25

function colaEffect:OnStart( pPlayer )
	if CLIENT then return end
	pPlayer:EmitSound( "npc/barnacle/barnacle_gulp".. math.random(1, 2).. ".wav", 60, math.random(70, 130) )
	GAMEMODE.Player:ModifyMoveSpeed( pPlayer, "ColaSpeed", 0, 5 )
end

function colaEffect:OnStop( pPlayer )
	if CLIENT then return end
	
	if GAMEMODE.Drugs:GetPlayerEffectPower( pPlayer, self.Name ) <= 0 then
		if GAMEMODE.Player:IsMoveSpeedModifierActive( pPlayer, "ColaSpeed" ) then
			GAMEMODE.Player:RemoveMoveSpeedModifier( pPlayer, "ColaSpeed" )
		end
	end
end

if CLIENT then
	function colaEffect:RenderScreenspaceEffects()
	end

	function colaEffect:GetMotionBlurValues( intW, intH, intForward, intRot )
	end
end

GM.Drugs:RegisterEffect( colaEffect )