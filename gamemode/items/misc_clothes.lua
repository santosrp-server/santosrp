--[[
	Name: misc_clothes.lua
	For: OakRoleplay
	By: Jake Butts
]]--

local Item = {}
Item.Name = "Pile of Clothes"
Item.Desc = "Your pile of clothes"
Item.Type = "type_drugs"
Item.Model = "models/humans/modern/male_01_01.mdl"
Item.Weight = 2
Item.Volume = 1
Item.CanDrop = false
Item.CanUse = true
Item.LimitID = "pile_clothes"
Item.OnUse = function( tblItem, pPlayer )
	GAMEMODE.Jobs:GetJobByID( JOB_CIVILIAN ):PlayerSetModel( pPlayer )
	GAMEMODE.Inv:GivePlayerItem( pPlayer, "Hazmat Suit", 1 )
end

GM.Inv:RegisterItem( Item )

local Item = {}
Item.Name = "Hazmat Suit"
Item.Desc = "Protect yourself against harsh chemicals."
Item.Type = "type_drugs"
Item.Model = "models/player/hazmat/colorable_hazmat.mdl"
Item.Weight = 2
Item.Volume = 1
Item.CanDrop = false
Item.CanUse = true
Item.LimitID = hazmat_suit
Item.CraftingEntClass = "ent_crafting_table"
Item.CraftSkill = "Gun Smithing"
Item.CraftSkillLevel = 5
Item.CraftSkillXP = 5
Item.CraftRecipe = {
	["Chunk of Plastic"] = 7,
	["Metal Plate"] = 10,
	["Specialized Rubber"] = 3,
}
Item.OnUse = function( tblItem, pPlayer )
	pPlayer:SetModel( "models/player/hazmat/colorable_hazmat.mdl" )
	GAMEMODE.Inv:GivePlayerItem( pPlayer, "Pile of Clothes", 1 )
end

GM.Inv:RegisterItem( Item )
GM.Inv:RegisterItemLimit( Item.LimitID, 8, { ["vip"] = 4 } )

local Item = {}
Item.Name = "Specialized Rubber"
Item.Desc = "Specialized Rubber for making Hazmat Suits."
Item.Model = "models/props_junk/plasticbucket001a.mdl"
Item.Weight = 20
Item.Volume = 10
Item.CanDrop = true
Item.DropClass = "prop_physics"
GM.Inv:RegisterItem( Item )



--[[ Drunk Effect ]]--
local chemEffect = {}
chemEffect.Name = "chemical"
chemEffect.NiceName = "Chemical Poisoning"
chemEffect.EffectName = "Chemical Poisoning"
chemEffect.Image = "santosrp/ae_icons/dizziness 48x48.png"
chemEffect.MaxPower = 10

function chemEffect:OnStart( pPlayer )
	if SERVER then

		if GAMEMODE.Drugs:GetPlayerEffectPower( pPlayer, self.Name ) > self.MaxPower *0.25 then
			if math.random( 1, 2 ) == 1 then
				self:MakePlayerVomit( pPlayer )
			end
		end

		if GAMEMODE.Drugs:GetPlayerEffectPower( pPlayer, self.Name ) >= self.MaxPower then
			if not pPlayer:IsUncon() then
				pPlayer:GoUncon()
			end
		end
	end
end

function chemEffect:Think( pPlayer )
	if CLIENT then return end

	if GAMEMODE.Drugs:GetPlayerEffectPower( pPlayer, self.Name ) > self.MaxPower *0.25 then
		if not pPlayer.m_intLastRandomVomit then
			pPlayer.m_intLastRandomVomit = CurTime() +math.random( 5, 10 )
		end

		if CurTime() > pPlayer.m_intLastRandomVomit then
			local offset = self.MaxPower *0.25
			local scalar = Lerp( (GAMEMODE.Drugs:GetPlayerEffectPower(pPlayer, self.Name) -offset) /(self.MaxPower -offset), 1, 0.33 )
			pPlayer.m_intLastRandomVomit = CurTime() +math.random( 60 *scalar, 120 *scalar )
			self:MakePlayerVomit( pPlayer )
		end
	end
end

function chemEffect:OnStop( pPlayer )
	if CLIENT then return end
	
	if GAMEMODE.Drugs:GetPlayerEffectPower( pPlayer, self.Name ) <= self.MaxPower *0.25 then
		pPlayer.m_intLastRandomVomit = nil
	end
end

local randomSounds = {
	Sound( "npc/zombie/zombie_pain1.wav" ),
	Sound( "npc/zombie/zombie_pain3.wav" ),
	Sound( "npc/barnacle/barnacle_die1.wav" ),
}
function chemEffect:MakePlayerVomit( pPlayer )
	if not pPlayer:Alive() or pPlayer:IsRagdolled() then return end
	
	local snd = table.Random( randomSounds )
	pPlayer:EmitSound( snd )

	timer.Create( ("effect_vomit_%p"):format(pPlayer), 0.1, 10, function()
		if not IsValid( pPlayer ) or not pPlayer:Alive() then return end
		
		local effectData = EffectData()
		effectData:SetOrigin( pPlayer:GetShootPos() +(pPlayer:EyeAngles():Forward() *8) )
		effectData:SetNormal( pPlayer:GetAimVector() )
		util.Effect( "vomitSpray", effectData )
		pPlayer:ViewPunch( Angle(10, 0, 0) )
	end )
end

if CLIENT then
	local drunkColor = {
		["$pp_colour_addr"] = 0,
		["$pp_colour_addg"] = 0,
		["$pp_colour_addb"] = 0,
		["$pp_colour_brightness"] = 0,
		["$pp_colour_contrast"] = 1,
		["$pp_colour_colour"] = 1,
		["$pp_colour_mulr"] = 0,
		["$pp_colour_mulg"] = 0,
		["$pp_colour_mulb"] = 0
	}

	function chemEffect:RenderScreenspaceEffects()
		local pow = math.min( GAMEMODE.Drugs:GetEffectPower(self.Name), self.MaxPower )
		local darkenMin = 0.5
		local darkenMax = 0.33
		local darken = Lerp( pow /self.MaxPower, darkenMin, darkenMax )

		drunkColor["$pp_colour_colour"] = Lerp( pow /self.MaxPower *2 -1, 1, 0.25 )
		DrawColorModify( drunkColor )
		DrawToyTown( Lerp(pow /self.MaxPower, 1, 5), ScrH() *Lerp(pow /self.MaxPower, 0.5, 1) )
	end

	function chemEffect:GetMotionBlurValues( intW, intH, intForward, intRot )
		local pow = math.min( GAMEMODE.Drugs:GetEffectPower(self.Name), self.MaxPower )
		local speed = Lerp( pow /self.MaxPower, 1, 3 )
		local strength = Lerp( pow /self.MaxPower, 0.01, 0.1 )
		intRot = intRot +math.sin( CurTime() *speed ) *strength

		return intW, intH, intForward, intRot
	end
end
GM.Drugs:RegisterEffect( chemEffect )

timer.Create("DrugLabChemicalTimer",10,0,function()
	if CLIENT then return end
	for _, v in pairs( ents.GetAll() ) do
		if v:GetClass() == "ent_drug_lab" then
			if v:GetBurnerOn() then
				for _, ply in pairs( ents.FindInSphere( v:GetPos() , 100 ) ) do
					if ply:IsPlayer() then
						if ply:GetModel() == "models/player/hazmat/colorable_hazmat.mdl" then continue end
						GAMEMODE.Drugs:PlayerApplyEffect( ply, "chemical", 3 *60, 2 )
					end
				end
			end
		end
	end
end)