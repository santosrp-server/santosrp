--[[
	Name: sv_trunks.lua
	For: SantosRP
	By: SantosRP
]]--

GM.Trunks = (GAMEMODE or GM).Trunks or {}
GM.Trunks.Cache = (GAMEMODE or GM).Trunks.Cache or {}

GM.Net:AddProtocol( "trunk", 15 )

function GM.Trunks:OpenTrunk( pPlayer, eVehicle )

    if !IsValid( eVehicle ) then return end

    if !eVehicle.CarData then return end
    if eVehicle.CarData.NoTrunk then return end
    if !eVehicle.TrunkSize then return end

    local tblTrunkItems = eVehicle.m_tblTrunkItems or {}

    GAMEMODE.Net:NewEvent( "trunk", "trk_str_o" )
        net.WriteEntity( eVehicle )
        net.WriteTable( tblTrunkItems )
        net.WriteInt( eVehicle.TrunkSize , 16 )
    GAMEMODE.Net:FireEvent( pPlayer )    

end

function GM.Trunks:OnPlayerTake( pPlayer, eVehicle, strItemID, intAmount )
    if not eVehicle.m_tblTrunkItems then eVehicle.m_bRemoved = true eVehicle:Remove() return end
    if not eVehicle.m_tblTrunkItems[strItemID] then return end
    
    intAmount = math.min( intAmount, eVehicle.m_tblTrunkItems[strItemID] )
    if intAmount <= 0 then return end
    
    if GAMEMODE.Inv:GivePlayerItem( pPlayer, strItemID, intAmount ) then
        eVehicle.m_tblTrunkItems[strItemID] = eVehicle.m_tblTrunkItems[strItemID] -intAmount

        if eVehicle.m_tblTrunkItems[strItemID] <= 0 then
            eVehicle.m_tblTrunkItems[strItemID] = nil
        end
    end
end

GM.Net:RegisterEventHandle( "trunk", "trk_str_t", function( intMsgLen, pPlayer )
    local ent = net.ReadEntity()
    if not IsValid( ent ) or not ent:IsVehicle() then return end
    if ent:GetPos():Distance( pPlayer:GetPos() ) > 200 then return end

    if pPlayer:IsIncapacitated() then return end
    if pPlayer:InVehicle() then return end

    GAMEMODE.Trunks:OnPlayerTake( pPlayer, ent, net.ReadString(), net.ReadUInt(16) )
    GAMEMODE.Trunks:OpenTrunk( pPlayer , ent )
end )

GM.Net:RegisterEventHandle( "trunk", "trk_str_a", function( intMsgLen, pPlayer )
    local ent = net.ReadEntity()
    if not IsValid( ent ) or not ent:IsVehicle() then return end
    if ent:GetPos():Distance( pPlayer:GetPos() ) > 200 then return end

    if pPlayer:IsIncapacitated() then return end
    if pPlayer:InVehicle() then return end

    local itemID, amount = net.ReadString(), net.ReadUInt( 16 )
    local itemData = GAMEMODE.Inv:GetItem( itemID )
    if not itemData or amount == 0 then return end
    if itemData.JobItem then return end
    
    local hasNum = GAMEMODE.Inv:GetPlayerItemAmount( pPlayer, itemID )
    if not hasNum or hasNum <= 0 then return end
    amount = math.min( amount, hasNum )

    local curSize = GAMEMODE.Inv:ComputeVolume( ent.m_tblTrunkItems or {} )
    if curSize +(itemData.Volume *amount) > ent.TrunkSize then
        pPlayer:AddNote( "This container is full!" )
        return
    end
    
    if GAMEMODE.Inv:TakePlayerItem( pPlayer, itemID, amount ) then
        ent.m_tblTrunkItems[itemID] = ent.m_tblTrunkItems[itemID] or 0
        ent.m_tblTrunkItems[itemID] = ent.m_tblTrunkItems[itemID] +amount
        GAMEMODE.Trunks:OpenTrunk( pPlayer , ent )
    end
end )

local Entity = FindMetaTable("Entity")

function Entity:GetTrunkPosition()
    if not GAMEMODE.Trunks.Cache[self:GetModel()] then
        local OBBM = self:OBBMaxs()
        
        GAMEMODE.Trunks.Cache[self:GetModel()] = self:OBBMins() + Vector(OBBM.x, 0, OBBM.z/2)
    end

    return GAMEMODE.Trunks.Cache[self:GetModel()]
end

hook.Add("PlayerUse", "OakRoleplayTrunks", function(pPlayer, eVehicle)
    if not IsValid( eVehicle ) or not eVehicle:IsVehicle() then return end

    local dot = (pPlayer:GetEyeTrace().HitPos -eVehicle:GetPos()):GetNormal():Dot( eVehicle:GetForward() )
    if dot < -0.19 and CurTime() >(pPlayer.m_intLastUsedTrunk or 0) then
        if eVehicle.IsLocked then return end
        if IsValid( eVehicle:GetDriver() ) then return end
        if eVehicle:GetDriver():IsPlayer() then return end        
        GAMEMODE.Trunks:OpenTrunk( pPlayer, eVehicle )
        pPlayer.m_intLastUsedTrunk = CurTime() +1
        return false
    end

end)

        --[[if eVehicle:IsVehicle() and pPlayer:GetPos():Distance(eVehicle:LocalToWorld(eVehicle:GetTrunkPosition())) <= 96 then
            
            if CurTime() > (eVehicle.TrunkSpam or 0) then

                if eVehicle.IsLocked then return end
                if IsValid( eVehicle:GetDriver() ) then return end
                if eVehicle:GetDriver():IsPlayer() then return end

                GAMEMODE.Trunks:OpenTrunk( pPlayer, eVehicle )

                eVehicle.TrunkSpam = CurTime() + 1
               
                return false

            end

        end]]