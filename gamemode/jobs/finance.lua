--[[
	Name: finance.lua
	For: TalosLife
	By: TalosLife
]]--

local Job = {}
Job.ID = 15
Job.Enum = "JOB_FINANCE"
Job.TeamColor = Color( 255, 100, 160, 255 )
Job.Name = "Minister of Finance"
Job.WhitelistName = "Minister of Finance"
Job.Pay = {
	{ PlayTime = 0, Pay = 203 },
	{ PlayTime = 4 *(60 *60), Pay = 257 },
	{ PlayTime = 12 *(60 *60), Pay = 311 },
	{ PlayTime = 24 *(60 *60), Pay = 378 },
}
Job.PlayerCap = GM.Config.Job_Finance_PlayerCap or { Min = 2, MinStart = 8, Max = 6, MaxEnd = 60 }

function Job:OnPlayerJoinJob( pPlayer )
end

function Job:OnPlayerQuitJob( pPlayer )
end

if SERVER then
	function Job:PlayerLoadout( pPlayer )
	end
else
	--client
end

GM.Jobs:Register( Job )