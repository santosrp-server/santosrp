--[[
	Name: civilian.lua
	For: TalosLife
	By: TalosLife
]]--

local Job = {}
Job.ID = 1
Job.Enum = "JOB_CIVILIAN"
Job.TeamColor = Color( 255, 255, 255, 255 )
Job.Name = "Civilian"
Job.Pay = {
	{ PlayTime = 0, Pay = 30 },
}

hook.Add( "GamemodeBuildPlayerComputerApps", "AutoInstallCivApps", function( pPlayer, entComputer, tblApps )
	if GAMEMODE.Jobs:GetPlayerJobID( pPlayer ) ~= JOB_CIVILIAN then return end
	tblApps["bizniz.exe"] = GAMEMODE.Apps:GetComputerApp( "bizniz.exe" )
	tblApps["haulage.exe"] = GAMEMODE.Apps:GetComputerApp( "haulage.exe" )
end )

function Job:OnPlayerJoinJob( pPlayer )
end

function Job:OnPlayerQuitJob( pPlayer )
end

function Job:GetPlayerModel( pPlayer )
	local base, overload

	if SERVER then
		base = GAMEMODE.Player:GetGameVar( pPlayer, "char_model_base", nil )
		overload = GAMEMODE.Player:GetGameVar( pPlayer, "char_model_overload", nil )
	else
		base = GAMEMODE.Player:GetGameVar( "char_model_base", nil )
		overload = GAMEMODE.Player:GetGameVar( "char_model_overload", nil )
	end
	
	if util.IsValidModel( overload or "" ) then
		return overload
	else
		return base
	end
end

function Job:PlayerSetModel( pPlayer )
	local mdl = self:GetPlayerModel(pPlayer)

	if mdl then
		pPlayer:SetModel( mdl )
		pPlayer:SetSkin( GAMEMODE.Player:GetGameVar(pPlayer, "char_skin", 0) )
	end
end

function Job:PlayerLoadout( pPlayer )
end

function GM.Net:CityWorker_RequestMoveJobItemFromVan( entVan, strItemID, intAmount )

	self:NewEvent( "ent", "cwork_van_take" )

		net.WriteEntity( entVan )

		net.WriteString( strItemID )

		net.WriteUInt( intAmount, 16 )

	self:FireEvent()

end



function GM.Net:CityWorker_RequestMoveJobItemToVan( entVan, strItemID, intAmount )

	self:NewEvent( "ent", "cwork_van_add" )

		net.WriteEntity( entVan )

		net.WriteString( strItemID )

		net.WriteUInt( intAmount, 16 )

	self:FireEvent()

end



GM.Net:RegisterEventHandle( "ent", "cwork_van_upd", function( intMsgLen, pPlayer )

	hook.Call( "GamemodeRefreshCityWorkVanMenu", GAMEMODE )

end )

GM.Jobs:Register( Job )