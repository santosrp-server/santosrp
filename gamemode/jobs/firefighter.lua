--[[
	Name: firefighter.lua
	For: SantosRP
	By: Ultra
]]--

AddCSLuaFile()

GM.ChatRadio:RegisterChannel( 3, "Fire", false )
GM.ChatRadio:RegisterChannel( 4, "Fire Encrypted", true )

local Job = {}
Job.ID = 4
Job.HasMasterKeys = true
Job.Receives911Messages = true
Job.Enum = "JOB_FIREFIGHTER"
Job.TeamColor = Color( 255, 100, 160, 255 )
Job.Name = "Firefighter"
Job.WhitelistName = "firefighter"
Job.PlayerModel = {
	Male_Fallback = "models/player/portal/male_07_fireman.mdl",
	Female_Fallback = "models/player/portal/male_07_fireman.mdl",
	
	Male = {
		["male_01"] = "models/player/portal/male_02_fireman.mdl",
		["male_02"] = "models/player/portal/male_02_fireman.mdl",
		["male_03"] = "models/player/portal/male_02_fireman.mdl",
		["male_04"] = "models/player/portal/male_04_fireman.mdl",
		["male_05"] = "models/player/portal/male_05_fireman.mdl",
		["male_06"] = "models/player/portal/male_06_fireman.mdl",
		["male_07"] = "models/player/portal/male_07_fireman.mdl",
		["male_08"] = "models/player/portal/male_08_fireman.mdl",
		["male_09"] = "models/player/portal/male_09_fireman.mdl",
	},
	Female = {
		["female_01"] = "models/player/portal/male_02_fireman.mdl",
		["female_02"] = "models/player/portal/male_02_fireman.mdl",
		["female_03"] = "models/player/portal/male_02_fireman.mdl",
		["female_04"] = "models/player/portal/male_04_fireman.mdl",
		["female_05"] = "models/player/portal/male_05_fireman.mdl",
		["female_06"] = "models/player/portal/male_06_fireman.mdl",
		["female_07"] = "models/player/portal/male_07_fireman.mdl",
		["female_08"] = "models/player/portal/male_08_fireman.mdl",
		["female_09"] = "models/player/portal/male_09_fireman.mdl",
	},
}
Job.Pay = {
	{ PlayTime = 0, Pay = 176 },
	{ PlayTime = 4 *(60 *60), Pay = 216 },
	{ PlayTime = 12 *(60 *60), Pay = 263 },
	{ PlayTime = 24 *(60 *60), Pay = 371 },
}
Job.PlayerCap = GM.Config.Job_Fire_PlayerCap or { Min = 2, MinStart = 8, Max = 6, MaxEnd = 60 }
Job.HasChatRadio = true
Job.DefaultChatRadioChannel = 3
Job.ChannelKeys = {
	[2] = true, --Police Encrypted
	[4] = true, --Fire Encrypted
	[6] = true, --EMS Encrypted
}
Job.ParkingGaragePos = GM.Config.FireParkingZone
Job.CarSpawns = GM.Config.FireCarSpawns
Job.FiretruckID = "fire_truck_large"
Job.FirstRespondID = "fire_first"
Job.FirstTahoeID = "fire_tahoe"
Job.RescueFiretruckID = "fire_truck_rescue"

Job.CatPositions = {
	{ pos = Vector( -5187.792969, -7073.568359, 198.967773 ), ang = Angle( 25.145, 100.102, -11.378 ) },
	{ pos = Vector( -3312.442139, -6126.792480, 227.617493 ), ang = Angle( 28.034, -100.761, 7.990 ) },
	{ pos = Vector( -33.712292, -7478.288574, 312.258850 ), ang = Angle( -12.710, -147.836, -5.666 ) },
	{ pos = Vector( 933.945068, -3408.140381, 427.171082 ), ang = Angle( -0.000, -77.521, 0.000 ) },
	{ pos = Vector( -1457.266724, 5729.553223, 969.189880 ), ang = Angle( 0.000, 23.592, 0.000 ) },
	{ pos = Vector( -1564.275757, 13495.971680, 781.899597 ), ang = Angle( -3.018, -25.289, -19.506 ) },
	{ pos = Vector( -14784.274414, 6927.260254, 846.709656 ), ang = Angle( 0.000, 69.607, 0.000 ) },
}

function Job:OnPlayerJoinJob( pPlayer )
end

function Job:OnPlayerQuitJob( pPlayer )
	local curCar = GAMEMODE.Cars:GetCurrentPlayerCar( pPlayer )
	if curCar and curCar.Job and curCar.Job == JOB_FIREFIGHTER then
		curCar:Remove()
	end
end

if SERVER then
	function Job:PlayerSetModel( pPlayer )
		local charData = GAMEMODE.Char:GetPlayerCharacter( pPlayer )
		if not charData then return end

		local valid, mdl = GAMEMODE.Util:FaceMatchPlayerModel(
			GAMEMODE.Player:GetGameVar( pPlayer, "char_model_base", "" ),
			charData.Sex == GAMEMODE.Char.SEX_MALE,
			self.PlayerModel
		)

		if valid then
			pPlayer:SetModel( mdl )
		else
			if charData.Sex == GAMEMODE.Char.SEX_MALE then
				pPlayer:SetModel( self.PlayerModel.Male_Fallback )
			elseif charData.Sex == GAMEMODE.Char.SEX_FEMALE then
				pPlayer:SetModel( self.PlayerModel.Female_Fallback )
			end
		end

		pPlayer:SetSkin( 0 )
	end

	function Job:PlayerLoadout( pPlayer )
		pPlayer:Give( "weapon_oak_extinguisher" )
		pPlayer:Give( "weapon_gspeak_radio_cop" )
	end

	function Job:OnPlayerSpawnFiretruck( pPlayer, entCar )

		entCar.IsFiretruck = true
		entCar.Hose = ents.Create( "ent_firehose" )
		entCar.Hose:SetPos( entCar:LocalToWorld( Vector(-65, 30, 57) ) )
		entCar.Hose:SetAngles( (-entCar:GetUp()):Angle() )
		entCar.Hose.Slot = 1
		entCar.Hose:SetOwner( entCar )
		entCar.Hose:Spawn()
		entCar.Hose:Activate()
		entCar.Hose:SetParent( entCar )
		
		entCar.Hose2 = ents.Create( "ent_firehose" )
		entCar.Hose2:SetPos( entCar:LocalToWorld( Vector(-65, 60, 57) ) )
		entCar.Hose2:SetAngles( (-entCar:GetUp()):Angle() )
		entCar.Hose2.Slot = 2
		entCar.Hose2:SetOwner( entCar )
		entCar.Hose2:Spawn()
		entCar.Hose2:Activate()
		entCar.Hose2:SetParent( entCar )

		entCar.m_tblTrunkItems = {

			["Government Issued Metal Ladder"] = 1,
			["Government Issued Large Metal Ladder"] = 1,
			["Police Issue Traffic Cone"] = 4,
			["Government Issue Traffic Barrel"] = 4,
			["Government Issue Barrier"] = 4,

		}

		pPlayer:AddNote( "You spawned your firetruck!" )
	end		
	
	
	function Job:OnPlayerSpawnFirstTahoeCar( pPlayer, entCar )
		entCar:SetSkin( 9 )
		pPlayer:AddNote( "Your spawned your tahoe vehicle!" )
	end

	function Job:OnPlayerSpawnFirstRespondCar( pPlayer, entCar )
		entCar:SetSkin( 2 )
		pPlayer:AddNote( "Your spawned your first responder vehicle!" )
	end	

	function Job:OnPlayerSpawnRescueFiretruck( pPlayer, entCar )
		pPlayer:AddNote( "You spawned your rescue firetruck!" )
	end		
	
		--Player wants to spawn a first responder vehicle
	function Job:PlayerSpawnFirstRespondCar( pPlayer )
		local car = GAMEMODE.Cars:PlayerSpawnJobCar( pPlayer, self.FirstRespondID, self.CarSpawns, self.ParkingLotPos )
		if IsValid( car ) then
			self:OnPlayerSpawnFirstRespondCar( pPlayer, car )
		end
	end


		--Player wants to spawn a first responder vehicle
	function Job:PlayerSpawnFirstTahoeCar( pPlayer )
		local car = GAMEMODE.Cars:PlayerSpawnJobCar( pPlayer, self.FirstTahoeID, self.CarSpawns, self.ParkingLotPos )
		if IsValid( car ) then
			self:OnPlayerSpawnFirstTahoeCar( pPlayer, car )
		
		end
	end	

	--Player wants to spawn a rescue firetruck
	function Job:PlayerSpawnRescueFiretruck( pPlayer )
		local car = GAMEMODE.Cars:PlayerSpawnJobCar( pPlayer, self.RescueFiretruckID, self.CarSpawns, self.ParkingGaragePos )
		if IsValid( car ) then
			self:OnPlayerSpawnRescueFiretruck( pPlayer, car )
		else
			print("MEME")
		end
	end	
	
	--Player wants to spawn a firetruck
	function Job:PlayerSpawnFiretruck( pPlayer )
		local car = GAMEMODE.Cars:PlayerSpawnJobCar( pPlayer, self.FiretruckID, self.CarSpawns, self.ParkingGaragePos )
		if IsValid( car ) then
			self:OnPlayerSpawnFiretruck( pPlayer, car )
		end
	end
	
	--Player wants to stow their firetruck
	function Job:PlayerStowFiretruck( pPlayer )
		GAMEMODE.Cars:PlayerStowJobCar( pPlayer, self.ParkingGaragePos )
	end

	timer.Create("FireCatTimer",60 *10,0,function()

		if GAMEMODE.Jobs:GetNumPlayers( JOB_FIREFIGHTER ) <= 0 then return end

		local randNum = math.random( 1 , #Job.CatPositions )

		local cat = ents.Create("ent_cat")
		cat:SetPos( Job.CatPositions[randNum].pos )
		cat:SetAngles( Job.CatPositions[randNum].ang )
		cat:Spawn()
		cat:Activate()

		local phys = cat:GetPhysicsObject()
		if IsValid( phys ) then
			phys:EnableMotion( false )
		end		

end)

concommand.Add("srp_spawn_cat",function()

	local randNum = math.random( 1 , #Job.CatPositions )

	local cat = ents.Create("ent_cat")
	cat:SetPos( Job.CatPositions[randNum].pos )
	cat:SetAngles( Job.CatPositions[randNum].ang )
	cat:Spawn()
	cat:Activate()

	local phys = cat:GetPhysicsObject()
	if IsValid( phys ) then
		phys:EnableMotion( false )
	end	

end)

else
	--client
	hook.Add( "HUDPaint", "Fire_DrawCats", function()
		if GAMEMODE.Jobs:GetPlayerJobID( LocalPlayer() ) ~= JOB_FIREFIGHTER then return end
		for k, v in pairs( ents.FindByClass("ent_cat") ) do
			local pos = v:GetPos():ToScreen()
			draw.SimpleText(
				"Cat Stuck!",
				"Trebuchet24",
				pos.x,
				pos.y,
				color_white,
				TEXT_ALIGN_CENTER,
				TEXT_ALIGN_TOP
			)
			local num, unit = GAMEMODE.Util:ConvertUnitsToM( v:GetPos():Distance(LocalPlayer():GetPos()) )
			draw.SimpleText(
				num.. unit,
				"Trebuchet24",
				pos.x,
				pos.y +18,
				color_white,
				TEXT_ALIGN_CENTER,
				TEXT_ALIGN_TOP
			)
		end
	end )	
end

GM.Jobs:Register( Job )