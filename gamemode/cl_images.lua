--[[
	Name: cl_images.lua
	For: SantosRP
	By: Jake Butts
]]--

GM.Images = (GAMEMODE or GM).Images or {}
GM.Images.Reg = (GAMEMODE or GM).Images.Reg or {}
GM.Images.Img = (GAMEMODE or GM).Images.Img or {}
GM.Images.Mat = (GAMEMODE or GM).Images.Mat or {}

file.CreateDir("santosrp/images")

function GM.Images:SaveImage(data, id)
	file.Write( "santosrp/images/"..id..".png", data )
end

function GM.Images:CacheImage(name, id)
	self.Img[name] = "santosrp/images/"..id..".png"
	self.Mat[name] = Material( "data/santosrp/images/"..id..".png" )
end

function GM.Images:CheckImage(id)
	return file.Exists( "santosrp/images/"..id..".png", "DATA" )
end

function GM.Images:DownloadImage(name, id)
	if self:CheckImage(id) then
		self:CacheImage(name, id)
		return
	end

	local userURL = "https://i.imgur.com/"..id..".png"
	http.Fetch( userURL,
		function( body, len, headers, code )
			self:SaveImage(body, id)
			self:CacheImage(name, id)
		end
	)
end

function GM.Images:RegisterImage(name, id)
	self.Reg[name] = id
end

function GM.Images:ProcessRegister()
	for k, v in pairs( self.Reg ) do
		self:DownloadImage( k, v )
	end
end

// Run when user logs in
hook.Add("HUDPaint", "GM.Images.LoadImages", function()
	hook.Remove("HUDPaint", "GM.Images.LoadImages")

	hook.Call("RequestImages")
end)