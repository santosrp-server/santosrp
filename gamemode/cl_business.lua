--[[
	Name: cl_business.lua
	For: SantosRP.Life
	By: Jake Butts
]]--

GM.Business = (GAMEMODE or GM).Business or {}
GM.Business.Businesses = (GAMEMODE or GM).Business.Businesses or {}
GM.Business.Mine = (GAMEMODE or GM).Business.Mine or {}

GM.Business.Prices = (GAMEMODE or GM).Business.Prices or {}

--[[ Business Management ]]--
function GM.Business:Load()
	self:LoadBusinesses()
end

function GM.Business:LoadBusinesses()
	GM:PrintDebug( 0, "->LOADING BUSINESSES" )

	local foundFiles, foundFolders = file.Find( GM.Config.GAMEMODE_PATH.. "business/*.lua", "LUA" )
	GM:PrintDebug( 0, "\tFound ".. #foundFiles.. " files." )

	for k, v in pairs( foundFiles ) do
		GM:PrintDebug( 0, "\tLoading ".. v )
		include( GM.Config.GAMEMODE_PATH.. "business/".. v )
		AddCSLuaFile( GM.Config.GAMEMODE_PATH.. "business/".. v )
	end

	GM:PrintDebug( 0, "->BUSINESSES LOADED" )

end

function GM.Business:RegisterBusiness( tblItem )
	self.Businesses[tblItem.Name] = tblItem
	GM:PrintDebug( 0, "\t\tRegistered item ".. tblItem.Name )
end

function GM.Business:GetBusinesses()
	return self.Businesses
end

function GM.Business:GetBusiness( strBusiness )
	return self.Businesses[strBusiness]
end

function GM.Business:GetBusinessValue( pPlayer, strBusiness )
	local pData = self.Mine
	local bData = pData[strBusiness]
	return bData.Value
end

function GM.Business:UpdateBusinesses( tblBusinesses )
	self.Mine = tblBusinesses
	hook.Call("GamemodeBusinessUpdate")
end

function GM.Business:UpdatePrices( tblPrices )
	self.Prices = tblPrices
end

function GM.Business:GetBuyPrice( strBusiness )
	return self.Prices[strBusiness]
end

--[[ Player Management ]] --
function GM.Business:PlayerOwnsBusiness( strBusiness )
	local pData = self.Mine
	if !pData[strBusiness] then return false end
	return true
end

function GM.Business:GetPlayerBusiness( strBusiness )
	return self.Mine[strBusiness]
end

--[[ Job Management ]]--
function GM.Business:RequestJob( strBusiness, jobType )
	GAMEMODE.Net:RequestJob( strBusiness, jobType )
end