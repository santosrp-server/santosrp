--[[
	Name: sh_mayor_taxes.lua
	For: TalosLife
	By: TalosLife
]]--

local App = {}
App.Name = "Business Manager"
App.ID = "bizniz.exe"
App.Panel = "SRPComputer_AppWindow_Business"
App.Icon = "santosrp/computer/icon_ss.png"
App.DekstopIcon = true
App.StartMenuIcon = true

--App code
--End app code

GM.Apps:Register( App )

if SERVER then return end

--App UI
local Panel = {}
function Panel:Init()

	self.strBusiness = {}

	self.m_pnlNameLabel = vgui3D.Create( "DLabel", self )
	self.m_pnlNameLabel:SetMouseInputEnabled( false )

	self.m_pnlNextButton = vgui3D.Create( "DButton", self )
	self.m_pnlNextButton.DoClick = function()
		if !GAMEMODE.Business:PlayerOwnsBusiness( self.strBusiness.Name ) then
			GAMEMODE.Net:RequestPurchaseBusiness( self.strBusiness.Name )
		end
	end
	
end

function Panel:SetBusiness( strBusiness )
	self.strBusiness = strBusiness
	self.m_pnlNameLabel:SetText( strBusiness.Name )

	if GAMEMODE.Business:PlayerOwnsBusiness( strBusiness.Name ) then
		self.m_pnlNextButton:SetText("Owned")
	else
		self.m_pnlNextButton:SetText("Purchase")
	end
end

function Panel:SetHome( m_pnlHome )
	self.m_pnlHome = m_pnlHome
end

function Panel:PerformLayout( intW, intH )
	local padding = 37
	self.m_pnlNameLabel:SizeToContents()
	self.m_pnlNameLabel:SetPos( padding, padding )

	self.m_pnlNextButton:SetSize( 150, 50 )
	self.m_pnlNextButton:SetPos( intW - 175, padding/2 )

end
vgui.Register( "SRPComputer_BusinessCard", Panel, "EditablePanel" )



local Panel = {}
function Panel:Init()
	self:GetParent():SetTitle( App.Name )
	self:GetParent():SetSize( 410, 380 )
	self:GetParent():SetPos( 75, 75 )

	self.m_pnlBusinessList = vgui3D.Create( "SRPComputer_ScrollPanel", self )
	self.m_tblBusinessCards = {}

	self:BuildBusinessCards()

	local hookID = ("UpdateBusinesses_%p"):format( self )
	hook.Add( "GamemodeBusinessUpdate", hookID, function()
		if not ValidPanel( self ) then
			hook.Remove( "GamemodeBusinessUpdate", hookID )
			return
		end

		self:BuildBusinessCards()
	end )
end

function Panel:BuildBusinessCards()
	for k, v in pairs( self.m_tblBusinessCards ) do
		if ValidPanel( v ) then v:Remove() end
	end
	self.m_tblBusinessCards = {}

	for k, v in pairs( GAMEMODE.Business:GetBusinesses() ) do
		self:CreateBusinessCard( v )
	end

	self:GetParent():InvalidateLayout()
end

function Panel:CreateBusinessCard( tblBusinessData )
	local businessCard = vgui3D.Create( "SRPComputer_BusinessCard", self.m_pnlBusinessList )
	businessCard:SetBusiness( tblBusinessData )
	businessCard:SetHome( self )
	businessCard.m_pnlParent = self
	table.insert( self.m_tblBusinessCards, businessCard )
	self.m_pnlBusinessList:AddItem( businessCard )
end

function Panel:PerformLayout( intW, intH )
	local padding = 3

	self.m_pnlBusinessList:SetPos( padding, padding )
	self.m_pnlBusinessList:SetSize( intW -(padding *2), intH -(padding *3) )

	for k, v in pairs( self.m_tblBusinessCards ) do
		v:SetSize( intW, 75 )
		v:DockMargin( 0, 0, 0, 0 )
		v:Dock( TOP )
	end
end
vgui.Register( "SRPComputer_AppWindow_Business", Panel, "EditablePanel" )