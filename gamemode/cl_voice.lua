--[[
	Name: cl_voice.lua
	For: SantosRP.Life
	By: Jake Butts
]]--

GM.Voice = (GAMEMODE or GM).Voice or {}
GM.Voice.LastChange = 0
GM.Voice.CurrentOption = 2

GM.Voice.DisplayHUD = false

GM.Voice.Options = {
	"Whispering",
	"Talking",
	"Yelling"
}

GM.Voice.Materials = {
	"voice_whisper",
	"voice_talk",
	"voice_yell"
}

function GM.Voice:RequestChange( voiceType )
	GAMEMODE.Net:RequestChangeVoice( voiceType )
end

function GM.Voice:GetNextOption()
	local new = self.CurrentOption + 1
	if new > 3 then new = 1 end
	return new
end

function GM.Voice:Tick()
	if input.IsKeyDown( KEY_Z ) then
		if (CurTime() - self.LastChange) < 1 then return end
		self.LastChange = CurTime()

		local option = self:GetNextOption()
		self:RequestChange( option )
	end
end

function GM.Voice:VoiceChanged( intVoiceMode )
	self.CurrentOption = intVoiceMode
end

function GM.Voice:GetTalkingMaterial()
	local matName = self.Materials[ self.CurrentOption ]
	return GAMEMODE.Images.Mat[ matName ]
end

hook.Add("PlayerStartVoice", "ImageOnVoice", function( pTalker )
	if LocalPlayer() == pTalker then
		GAMEMODE.Voice.DisplayHUD = true
	end
end)

hook.Add("PlayerEndVoice", "ImageOnVoice", function( pTalker )
	if LocalPlayer() == pTalker then
		GAMEMODE.Voice.DisplayHUD = false
	end
end)

hook.Add("HUDPaint", "GAMEMODE.Voice.DrawHUD", function()

	if !GAMEMODE.Voice.DisplayHUD then return end

	local w = ScrW()

	draw.SimpleTextOutlined( GAMEMODE.Voice.Options[ GAMEMODE.Voice.CurrentOption ], "DermaLarge", w-120, 47, Color(112, 220, 112), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER, 1, color_black )

	surface.SetDrawColor(255, 255, 255, 255)
	surface.SetMaterial( GAMEMODE.Voice:GetTalkingMaterial() )
	surface.DrawTexturedRect(w-100, 20, 60, 60)
	

end)