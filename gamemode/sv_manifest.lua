--[[
	Name: sv_manifest.lua
	For: santosrp
	By: santosrp
]]--

AddCSLuaFile "santosrp/gamemode/cl_init.lua"
AddCSLuaFile "santosrp/gamemode/cl_manifest.lua"
AddCSLuaFile "santosrp/gamemode/cl_networking.lua"
AddCSLuaFile "santosrp/gamemode/cl_player.lua"
AddCSLuaFile "santosrp/gamemode/cl_player_damage.lua"
AddCSLuaFile "santosrp/gamemode/cl_characters.lua"
AddCSLuaFile "santosrp/gamemode/cl_inventory.lua"
AddCSLuaFile "santosrp/gamemode/cl_gui.lua"
AddCSLuaFile "santosrp/gamemode/cl_npcs.lua"
AddCSLuaFile "santosrp/gamemode/cl_properties.lua"
AddCSLuaFile "santosrp/gamemode/cl_calcview.lua"
AddCSLuaFile "santosrp/gamemode/cl_cinicam.lua"
AddCSLuaFile "santosrp/gamemode/cl_cars.lua"
AddCSLuaFile "santosrp/gamemode/cl_jobs.lua"
AddCSLuaFile "santosrp/gamemode/cl_jail.lua"
AddCSLuaFile "santosrp/gamemode/cl_map.lua"
AddCSLuaFile "santosrp/gamemode/cl_hud.lua"
AddCSLuaFile "santosrp/gamemode/cl_hud_car.lua"
AddCSLuaFile "santosrp/gamemode/cl_license.lua"
AddCSLuaFile "santosrp/gamemode/cl_3d2dvgui.lua"
AddCSLuaFile "santosrp/gamemode/cl_skills.lua"
AddCSLuaFile "santosrp/gamemode/cl_drugs.lua"
AddCSLuaFile "santosrp/gamemode/cl_needs.lua"
AddCSLuaFile "santosrp/gamemode/cl_buddies.lua"
AddCSLuaFile "santosrp/gamemode/cl_chatradios.lua"
AddCSLuaFile "santosrp/gamemode/cl_weather.lua"
AddCSLuaFile "santosrp/gamemode/cl_daynight.lua"
AddCSLuaFile "santosrp/gamemode/cl_apps.lua"
AddCSLuaFile "santosrp/gamemode/cl_images.lua"
AddCSLuaFile "santosrp/gamemode/cl_voice.lua"
AddCSLuaFile "santosrp/gamemode/cl_business.lua"

AddCSLuaFile "santosrp/gamemode/sh_config.lua"
AddCSLuaFile "santosrp/gamemode/sh_init.lua"
AddCSLuaFile "santosrp/gamemode/sh_propprotect.lua"
AddCSLuaFile "santosrp/gamemode/sh_economy.lua"
AddCSLuaFile "santosrp/gamemode/sh_gmodhands.lua"
AddCSLuaFile "santosrp/gamemode/sh_npcdialog.lua"
AddCSLuaFile "santosrp/gamemode/sh_chatbox.lua"
AddCSLuaFile "santosrp/gamemode/sh_util.lua"
AddCSLuaFile "santosrp/gamemode/sh_car_radios.lua"
AddCSLuaFile "santosrp/gamemode/sh_item_radios.lua"
AddCSLuaFile "santosrp/gamemode/sh_unconscious.lua"
AddCSLuaFile "santosrp/gamemode/sh_santos_customs.lua"
AddCSLuaFile "santosrp/gamemode/sh_cars_misc.lua"
AddCSLuaFile "santosrp/gamemode/sh_pacmodels.lua"
AddCSLuaFile "santosrp/gamemode/sh_chat.lua"
AddCSLuaFile "santosrp/gamemode/sh_player_anims.lua"

include "santosrp/gamemode/sh_config.lua"
include "santosrp/gamemode/sh_propprotect.lua"
include "santosrp/gamemode/sh_economy.lua"
include "santosrp/gamemode/sh_gmodhands.lua"
include "santosrp/gamemode/sh_npcdialog.lua"
include "santosrp/gamemode/sh_chatbox.lua"
include "santosrp/gamemode/sh_util.lua"
include "santosrp/gamemode/sh_car_radios.lua"
include "santosrp/gamemode/sh_item_radios.lua"
include "santosrp/gamemode/sh_unconscious.lua"
include "santosrp/gamemode/sh_santos_customs.lua"
include "santosrp/gamemode/sh_cars_misc.lua"
include "santosrp/gamemode/sh_pacmodels.lua"
include "santosrp/gamemode/sh_chat.lua"
include "santosrp/gamemode/sh_player_anims.lua"

include "santosrp/gamemode/sv_config.lua"
include "santosrp/gamemode/sv_networking.lua"
include "santosrp/gamemode/sv_player.lua"
include "santosrp/gamemode/sv_entity.lua"
include "santosrp/gamemode/sv_player_damage.lua"
include "santosrp/gamemode/sv_entity_damage.lua"
include "santosrp/gamemode/sv_characters.lua"
include "santosrp/gamemode/sv_chatradios.lua"
include "santosrp/gamemode/sv_commands.lua"
include "santosrp/gamemode/sv_jobs.lua"
include "santosrp/gamemode/sv_economy.lua"
include "santosrp/gamemode/sv_inventory.lua"
include "santosrp/gamemode/sv_npcs.lua"
include "santosrp/gamemode/sv_properties.lua"
include "santosrp/gamemode/sv_cars.lua"
include "santosrp/gamemode/sv_map.lua"
include "santosrp/gamemode/sv_jail.lua"
include "santosrp/gamemode/sv_license.lua"
include "santosrp/gamemode/sv_mysql.lua"
include "santosrp/gamemode/sv_mysql_player.lua"
include "santosrp/gamemode/sv_trunks.lua"
include "santosrp/gamemode/sv_firesystem.lua"
include "santosrp/gamemode/sv_bailout.lua"
include "santosrp/gamemode/sv_bank_storage.lua"
include "santosrp/gamemode/sv_phone.lua"
include "santosrp/gamemode/sv_santos_customs.lua"
include "santosrp/gamemode/sv_skills.lua"
include "santosrp/gamemode/sv_drugs.lua"
include "santosrp/gamemode/sv_needs.lua"
include "santosrp/gamemode/sv_buddies.lua"
include "santosrp/gamemode/sv_pvs_buffer.lua"
include "santosrp/gamemode/sv_chop_shop.lua"
include "santosrp/gamemode/sv_daynight.lua"
include "santosrp/gamemode/sv_weather.lua"
include "santosrp/gamemode/sv_apps.lua"

include "santosrp/gamemode/sv_bankrobbery.lua"
include "santosrp/gamemode/sv_ipbgroups.lua"
include "santosrp/gamemode/sv_voice.lua"
include "santosrp/gamemode/sv_business.lua"

--Load vgui
local foundFiles, foundFolders = file.Find( GM.Config.GAMEMODE_PATH.. "vgui/*.lua", "LUA" )
for k, v in pairs( foundFiles ) do
	AddCSLuaFile( GM.Config.GAMEMODE_PATH.. "vgui/".. v )
end

--Add resources
local function FilesInDir( strDir, b )
	local files, folders = file.Find( strDir.. "/*", "GAME" )
	local ret = {}

	for k, file in pairs( files ) do
		ret[k] = strDir.. "/".. file
	end

	for k, folder in pairs( folders ) do
		for k2, v2 in pairs( FilesInDir(strDir.. "/".. folder, true) ) do
			table.insert( ret, v2 )
		end
	end

	return ret
end

resource.AddWorkshop( '180507408' ) -- FA:S 2.0 Alpha SWEPs
resource.AddWorkshop( '181283903' ) -- FA:S 2.0 Alpha SWEPS - Pistols
resource.AddWorkshop( '181656972' ) -- FA:S 2.0 Alpha SWEPs - Rifles
resource.AddWorkshop( '183139624' ) -- FA:S 2.0 Alpha SWEPs - SMGs
resource.AddWorkshop( '201027186' ) -- FA:S 2.0 Alpha SWEPs - Misc
resource.AddWorkshop( '201027715' ) -- FA:S 2.0 Alpha SWEPs - U. Rifles
resource.AddWorkshop( '666626259' ) -- Talos Life Content - Materials 7
resource.AddWorkshop( '666627837' ) -- Talos Life Content - Materials 8
resource.AddWorkshop( '666628225' ) -- Talos Life Content - Materials 9
resource.AddWorkshop( '666647069' ) -- Talos Life Content - Models 1
resource.AddWorkshop( '666629480' ) -- Talos Life Content - Models 2
resource.AddWorkshop( '666630507' ) -- Talos Life Content - Models 3
resource.AddWorkshop( '666632034' ) -- Talos Life Content - Models 4
resource.AddWorkshop( '666633517' ) -- Talos Life Content - Models 5
resource.AddWorkshop( '666635080' ) -- Talos Life Content - Models 6
resource.AddWorkshop( '666636527' ) -- Talos Life Content - Models 7
resource.AddWorkshop( '666637883' ) -- Talos Life Content - Models 8
resource.AddWorkshop( '666639400' ) -- Talos Life Content - Models 9
resource.AddWorkshop( '666640635' ) -- Talos Life Content - Models 10
resource.AddWorkshop( '666641956' ) -- Talos Life Content - Models 11
resource.AddWorkshop( '649293142' ) -- Talos Life Content - Player Models - Materials
resource.AddWorkshop( '649293875' ) -- Talos Life Content - Player Models - Models
resource.AddWorkshop( '666607951' ) -- Talos Life Content - Miscellaneous 1
resource.AddWorkshop( '666610986' ) -- Talos Life Content - Sound 1
resource.AddWorkshop( '666615048' ) -- Talos Life Content - Sound 2
resource.AddWorkshop( '666618487' ) -- Talos Life Content - Materials 1
resource.AddWorkshop( '666620131' ) -- Talos Life Content - Materials 2
resource.AddWorkshop( '666622276' ) -- Talos Life Content - Materials 3
resource.AddWorkshop( '666623204' ) -- Talos Life Content - Materials 4
resource.AddWorkshop( '666623483' ) -- Talos Life Content - Materials 5
resource.AddWorkshop( '666624754' ) -- Talos Life Content - Materials 6
resource.AddWorkshop( '579409868' ) -- Paralake City V3
resource.AddFile( "materials/isnbetalogo.png" ) -- ISNLogo
resource.AddFile( "materials/isnlogo.png" ) -- ISNBeta