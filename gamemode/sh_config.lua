--[[
	Name: sh_config.lua
	For: SantosRP
	By: SantosRP
]]--

DEV_SERVER = true

GM.Config = {}
GM.Config.GAMEMODE_PATH = "santosrp/gamemode/"
GM.Config.DeathWaitTime = 3 *60 --Time to wait for insta-death countdown

GM.Config.MorphList = {
	"models/crow.mdl",
	"models/pigeon.mdl",
	"models/seagull.mdl",
}

--[[ Load Images ]] --
hook.Add("RequestImages", "MinimapGetImages", function()

	// Branding
	GAMEMODE.Images:RegisterImage("logo", "VE6VC0a")

	// Map
	GAMEMODE.Images:RegisterImage("map_overhead", "rZqzT1M")
	GAMEMODE.Images:RegisterImage("map_player", "WUDzzyM")
	GAMEMODE.Images:RegisterImage("map_pd", "G1CNvUL")
	GAMEMODE.Images:RegisterImage("map_cardealer", "pqfovM3")
	GAMEMODE.Images:RegisterImage("map_ems", "PmcJH8y")
	GAMEMODE.Images:RegisterImage("map_shell", "ErNiKYn")
	GAMEMODE.Images:RegisterImage("map_tow", "vqXqPeS")
	GAMEMODE.Images:RegisterImage("map_truck", "ILtPEeu")
	GAMEMODE.Images:RegisterImage("map_bank", "cp9oxVA")
	GAMEMODE.Images:RegisterImage("map_shop", "0PD8KD0")
	//GAMEMODE.Images:RegisterImage("map_sign", "2uwhvLe")
	//GAMEMODE.Images:RegisterImage("car_tracker", "SwozglN")

	// Voice
	GAMEMODE.Images:RegisterImage("voice_whisper", "ItyhOlM")
	GAMEMODE.Images:RegisterImage("voice_talk", "Kmlb6mo")
	GAMEMODE.Images:RegisterImage("voice_yell", "dHgpZg7")

	// Finally, Process
	GAMEMODE.Images:ProcessRegister()

end)

--[[ ConVars ]]--
if CLIENT then
	GM.Config.HasDataCap = CreateClientConVar( "srp_datacap_mode", "0", true, false )
	GM.Config.LightSensitiveMode = CreateClientConVar( "srp_lightsensitive_mode", "0", true, false )

	// Minimap
	GM.Config.MapPlayer = CreateClientConVar( "srp_map_player", "1", true, false )
	GM.Config.MapPoints = CreateClientConVar( "srp_map_points", "1", true, false )
	GM.Config.MapSigns = CreateClientConVar( "srp_map_signs", "1", true, false )
	GM.Config.MapCops = CreateClientConVar( "srp_map_cops", "1", true, false )
end

--[[ User Group Settings ]]--
GM.Config.UserGroupConfig = {
	["founder"] = { Name = "Owner", Color = Color(255, 50, 50, 255) },
	["dos"] = { Name = "Teamspeak Manager", Color = Color(222,135,248, 255) },
	["ld"] = { Name = "Development Manager", Color = Color(222,135,248, 255) },
	["user"] = { Name = "Citizen", Color = Color(255, 255, 255, 255) },
	["supporter"] = { Name = "Supporter", Color = Color(255, 215, 0, 255) },
	["am"] = { Name = "Staff Manager", Color = Color(222,135,248, 255) },
	["ca"] = { Name = "Community Relations", Color = Color(69, 77, 196, 255) },
	["mod"] = { Name = "Moderator", Color = Color(19, 126, 26, 255) },
	["admin"] = { Name = "Administrator", Color = Color(38, 111, 255) },	
	["sa"] = { Name = "Senior Administrator", Color = Color(132, 20, 200) },	
}
GM.Config.VIPGroups = {
	["superadmin"] = true,
	["headadmin"] = true,
	["community_manager"] = true,
	["admin"] = true,
	["moderator"] = true,
	["vip"] = true,
	["Owner"] = true,
}

--[[ Item Limits ]]--
GM.Config.MaxItemLimit = 64
GM.Config.GroupExtraMaxItems = {
	["vip"] = 24,
}

--[[ Business Settings ]]--
GM.Config.MinimumSupplies = 10
GM.Config.MaximumSupplies = 100

GM.Config.HaulageIncSupplies = {
	"Electric Supplies",
	"Hardware Supplies",
	"Supermarket Supplies",
	"Clothing Supplies"
}

GM.Config.MaskList = {
	
	"Doctor Mask (Skin 1)",
	"Doctor Mask (Skin 2)",
	"Doctor Mask (Skin 3)",

	"Ninja Mask (Skin 1)",
	"Ninja Mask (Skin 2)",
	"Ninja Mask (Skin 3)",
	"Ninja Mask (Skin 4)",
	"Ninja Mask (Skin 5)",
	"Ninja Mask (Skin 6)",
	"Ninja Mask (Skin 7)",
	"Ninja Mask (Skin 8)",
	"Ninja Mask (Skin 9)",
	"Ninja Mask (Skin 10)",
	"Ninja Mask (Skin 11)",

	"Head Wrap 1 (Skin 1)",
	"Head Wrap 1 (Skin 2)",
	"Head Wrap 1 (Skin 3)",
	"Head Wrap 1 (Skin 4)",

	"Head Wrap 2 (Skin 1)",
	"Head Wrap 2 (Skin 2)",
	"Head Wrap 2 (Skin 3)",
	"Head Wrap 2 (Skin 4)",

	"Mask 1 (Skin 1)",
	"Mask 1 (Skin 2)",
	"Mask 1 (Skin 3)",
	"Mask 1 (Skin 4)",
	"Mask 1 (Skin 5)",
	"Mask 1 (Skin 6)",
	"Mask 1 (Skin 7)",
	"Mask 1 (Skin 8)",
	"Mask 1 (Skin 9)",
	"Mask 1 (Skin 10)",
	"Mask 1 (Skin 11)",
	"Mask 1 (Skin 12)",
	"Mask 1 (Skin 13)",
	"Mask 1 (Skin 14)",
	"Mask 1 (Skin 15)",

	"Mask 2 (Skin 1)",
	"Mask 2 (Skin 2)",
	"Mask 2 (Skin 3)",
	"Mask 2 (Skin 4)",
	"Mask 2 (Skin 5)",
	"Mask 2 (Skin 6)",
	"Mask 2 (Skin 7)",
	"Mask 2 (Skin 8)",
	"Mask 2 (Skin 9)",

	"Mask 4",

	"Mask 5 (Skin 1)",
	"Mask 5 (Skin 2)",
	"Mask 5 (Skin 3)",
	"Mask 5 (Skin 4)",

	"Monkey Mask (Skin 1)",
	"Monkey Mask (Skin 2)",
	"Monkey Mask (Skin 3)",
	"Monkey Mask (Skin 4)",

	"Skull Mask (Skin 1)",
	"Skull Mask (Skin 2)",
	"Skull Mask (Skin 3)",
	"Skull Mask (Skin 4)",

	"Zombie Mask (Skin 1)",
	"Zombie Mask (Skin 2)",

	"Bag Mask (Skin 1)",
	"Bag Mask (Skin 2)",
	"Bag Mask (Skin 3)",
	"Bag Mask (Skin 4)",
	"Bag Mask (Skin 5)",
	"Bag Mask (Skin 6)",
	"Bag Mask (Skin 7)",
	"Bag Mask (Skin 8)",
	"Bag Mask (Skin 9)",
	"Bag Mask (Skin 10)",
	"Bag Mask (Skin 11)",
	"Bag Mask (Skin 12)",
	"Bag Mask (Skin 13)",
	"Bag Mask (Skin 14)",
	"Bag Mask (Skin 15)",
	"Bag Mask (Skin 16)",
	"Bag Mask (Skin 17)",
	"Bag Mask (Skin 18)",
	"Bag Mask (Skin 19)",
	"Bag Mask (Skin 20)",
	"Bag Mask (Skin 21)",
	"Bag Mask (Skin 22)",
	"Bag Mask (Skin 23)",
	"Bag Mask (Skin 24)",
	"Bag Mask (Skin 25)",
	"Bag Mask (Skin 26)",

	"Cookie Face",
	"Bear Mask",
	"Horse Mask",
	"Raccoon Mask",

	"Balaclava"

}

--[[ Character Settings ]]--
GM.Config.MaxCharacters = DEV_SERVER and 3 or 1
GM.Config.NameLength = { First = 15, Last = 15 }
GM.Config.StartingMoney = { Wallet = 10000, Bank = 0 }
GM.Config.MaxCarryWeight = 500
GM.Config.MaxCarryVolume = 900
--Player models allowed in character creation
GM.Config.PlayerModels = {

	Female = {

		["models/humans/modern/female_01.mdl"] = true,

		["models/humans/modern/female_02.mdl"] = true,

		["models/humans/modern/female_03.mdl"] = true,

		["models/humans/modern/female_04.mdl"] = true,

		["models/humans/modern/female_06.mdl"] = true,

		["models/humans/modern/female_07.mdl"] = true,

	},

	Male = {

		["models/humans/modern/male_01_01.mdl"] = true,

		["models/humans/modern/male_01_02.mdl"] = true,

		["models/humans/modern/male_01_03.mdl"] = true,

		["models/humans/modern/male_02_01.mdl"] = true,

		["models/humans/modern/male_02_02.mdl"] = true,

		["models/humans/modern/male_02_03.mdl"] = true,

		["models/humans/modern/male_03_01.mdl"] = true,

		["models/humans/modern/male_03_02.mdl"] = true,

		["models/humans/modern/male_03_03.mdl"] = true,

		["models/humans/modern/male_03_04.mdl"] = true,

		["models/humans/modern/male_03_05.mdl"] = true,

		["models/humans/modern/male_03_06.mdl"] = true,

		["models/humans/modern/male_03_07.mdl"] = true,

		["models/humans/modern/male_04_01.mdl"] = true,

		["models/humans/modern/male_04_02.mdl"] = true,

		["models/humans/modern/male_04_03.mdl"] = true,

		["models/humans/modern/male_04_04.mdl"] = true,

		["models/humans/modern/male_05_01.mdl"] = true,

		["models/humans/modern/male_05_02.mdl"] = true,

		["models/humans/modern/male_05_03.mdl"] = true,

		["models/humans/modern/male_05_04.mdl"] = true,

		["models/humans/modern/male_05_05.mdl"] = true,

		["models/humans/modern/male_06_01.mdl"] = true,

		["models/humans/modern/male_06_02.mdl"] = true,

		["models/humans/modern/male_06_03.mdl"] = true,

		["models/humans/modern/male_06_04.mdl"] = true,

		["models/humans/modern/male_06_05.mdl"] = true,

		["models/humans/modern/male_07_01.mdl"] = true,

		["models/humans/modern/male_07_02.mdl"] = true,

		["models/humans/modern/male_07_03.mdl"] = true,

		["models/humans/modern/male_07_04.mdl"] = true,

		["models/humans/modern/male_07_05.mdl"] = true,

		["models/humans/modern/male_07_06.mdl"] = true,

		["models/humans/modern/male_08_01.mdl"] = true,

		["models/humans/modern/male_08_02.mdl"] = true,

		["models/humans/modern/male_08_03.mdl"] = true,

		["models/humans/modern/male_08_04.mdl"] = true,

		["models/humans/modern/male_09_01.mdl"] = true,

		["models/humans/modern/male_09_02.mdl"] = true,

		["models/humans/modern/male_09_03.mdl"] = true,

		["models/humans/modern/male_09_04.mdl"] = true,

	},

}

--List of player models a player can switch to, if the face type matches

--Used in the clothing shop

GM.Config.PlayerModelOverloads = {

	Male = {
		["male_01"] = {
			"models/player/suits/male_01_closed_coat_tie.mdl",
			"models/player/suits/male_01_closed_tie.mdl",
			"models/player/suits/male_01_open.mdl",
			"models/player/suits/male_01_open_tie.mdl",
			"models/player/suits/male_01_open_waistcoat.mdl",
			"models/player/suits/male_01_shirt.mdl",
			"models/player/suits/male_01_shirt_tie.mdl",
		},
		["male_02"] = {
			"models/player/suits/male_02_closed_coat_tie.mdl",
			"models/player/suits/male_02_closed_tie.mdl",
			"models/player/suits/male_02_open.mdl",
			"models/player/suits/male_02_open_tie.mdl",
			"models/player/suits/male_02_open_waistcoat.mdl",
			"models/player/suits/male_02_shirt.mdl",
			"models/player/suits/male_02_shirt_tie.mdl",
		},
		["male_03"] = {
			"models/player/suits/male_03_closed_coat_tie.mdl",
			"models/player/suits/male_03_closed_tie.mdl",
			"models/player/suits/male_03_open.mdl",
			"models/player/suits/male_03_open_tie.mdl",
			"models/player/suits/male_03_open_waistcoat.mdl",
			"models/player/suits/male_03_shirt.mdl",
			"models/player/suits/male_03_shirt_tie.mdl",
		},
		["male_04"] = {
			"models/player/suits/male_04_closed_coat_tie.mdl",
			"models/player/suits/male_04_closed_tie.mdl",
			"models/player/suits/male_04_open.mdl",
			"models/player/suits/male_04_open_tie.mdl",
			"models/player/suits/male_04_open_waistcoat.mdl",
			"models/player/suits/male_04_shirt.mdl",
			"models/player/suits/male_04_shirt_tie.mdl",
		},
		["male_05"] = {
			"models/player/suits/male_05_closed_coat_tie.mdl",
			"models/player/suits/male_05_closed_tie.mdl",
			"models/player/suits/male_05_open.mdl",
			"models/player/suits/male_05_open_tie.mdl",
			"models/player/suits/male_05_open_waistcoat.mdl",
			"models/player/suits/male_05_shirt.mdl",
			"models/player/suits/male_05_shirt_tie.mdl",
		},
		["male_06"] = {
			"models/player/suits/male_06_closed_coat_tie.mdl",
			"models/player/suits/male_06_closed_tie.mdl",
			"models/player/suits/male_06_open.mdl",
			"models/player/suits/male_06_open_tie.mdl",
			"models/player/suits/male_06_open_waistcoat.mdl",
			"models/player/suits/male_06_shirt.mdl",
			"models/player/suits/male_06_shirt_tie.mdl",
		},
		["male_07"] = {
			"models/player/suits/male_07_closed_coat_tie.mdl",
			"models/player/suits/male_07_closed_tie.mdl",
			"models/player/suits/male_07_open.mdl",
			"models/player/suits/male_07_open_tie.mdl",
			"models/player/suits/male_07_open_waistcoat.mdl",
			"models/player/suits/male_07_shirt.mdl",
			"models/player/suits/male_07_shirt_tie.mdl",
		},
		["male_08"] = {
			"models/player/suits/male_08_closed_coat_tie.mdl",
			"models/player/suits/male_08_closed_tie.mdl",
			"models/player/suits/male_08_open.mdl",
			"models/player/suits/male_08_open_tie.mdl",
			"models/player/suits/male_08_open_waistcoat.mdl",
			"models/player/suits/male_08_shirt.mdl",
			"models/player/suits/male_08_shirt_tie.mdl",
		},
		["male_09"] = {
			"models/player/suits/male_09_closed_coat_tie.mdl",
			"models/player/suits/male_09_closed_tie.mdl",
			"models/player/suits/male_09_open.mdl",
			"models/player/suits/male_09_open_tie.mdl",
			"models/player/suits/male_09_open_waistcoat.mdl",
			"models/player/suits/male_09_shirt.mdl",
			"models/player/suits/male_09_shirt_tie.mdl",
		},
	},
	Female = {},
}

--Skin indexes to block in character creation / clothing store
GM.Config.BlockedModelSkins = {
	["models/humans/modern/female_01.mdl"] = { 6, 21 },
	["models/humans/modern/female_02.mdl"] = { 6, 21 },
	["models/humans/modern/female_03.mdl"] = { 6, 21 },
	["models/humans/modern/female_04.mdl"] = { 6, 21 },
	["models/humans/modern/female_06.mdl"] = { 6, 21 },
	["models/humans/modern/female_07.mdl"] = { 6, 21 },
	
	["models/humans/modern/male_01_01.mdl"] = { 1 },
	["models/humans/modern/male_01_02.mdl"] = { 1 },
	["models/humans/modern/male_01_03.mdl"] = { 1 },
	["models/humans/modern/male_02_01.mdl"] = { 1 },
	["models/humans/modern/male_02_02.mdl"] = { 1 },
	["models/humans/modern/male_02_03.mdl"] = { 1 },
	["models/humans/modern/male_03_01.mdl"] = { 1 },
	["models/humans/modern/male_03_02.mdl"] = { 1 },
	["models/humans/modern/male_03_03.mdl"] = { 1 },
	["models/humans/modern/male_03_04.mdl"] = { 1 },
	["models/humans/modern/male_03_05.mdl"] = { 1 },
	["models/humans/modern/male_03_06.mdl"] = { 1 },
	["models/humans/modern/male_03_07.mdl"] = { 1 },
	["models/humans/modern/male_04_01.mdl"] = { 1 },
	["models/humans/modern/male_04_02.mdl"] = { 1 },
	["models/humans/modern/male_04_03.mdl"] = { 1 },
	["models/humans/modern/male_04_04.mdl"] = { 1 },
	["models/humans/modern/male_05_01.mdl"] = { 1 },
	["models/humans/modern/male_05_02.mdl"] = { 1 },
	["models/humans/modern/male_06_01.mdl"] = { 1 },
	["models/humans/modern/male_06_02.mdl"] = { 1 },
	["models/humans/modern/male_06_03.mdl"] = { 1 },
	["models/humans/modern/male_06_04.mdl"] = { 1 },
	["models/humans/modern/male_06_05.mdl"] = { 1 },
	["models/humans/modern/male_07_01.mdl"] = { 1 },
	["models/humans/modern/male_07_02.mdl"] = { 1 },
	["models/humans/modern/male_07_03.mdl"] = { 1 },
	["models/humans/modern/male_07_04.mdl"] = { 1 },
	["models/humans/modern/male_07_05.mdl"] = { 1 },
	["models/humans/modern/male_07_06.mdl"] = { 1 },
	["models/humans/modern/male_08_01.mdl"] = { 1 },
	["models/humans/modern/male_08_02.mdl"] = { 1 },
	["models/humans/modern/male_08_03.mdl"] = { 1 },
	["models/humans/modern/male_08_04.mdl"] = { 1 },
	["models/humans/modern/male_09_01.mdl"] = { 1 },
	["models/humans/modern/male_09_02.mdl"] = { 1 },
	["models/humans/modern/male_09_03.mdl"] = { 1 },
	["models/humans/modern/male_09_04.mdl"] = { 1 },
}


--[[ Car Settings ]]--
--Stock colors a player may pick from when buying a new car
GM.Config.StockCarColors = {
	["White"] = Color( 255, 255, 255, 255 ),
	["Silver"] = Color( 182, 182, 182, 255 ),
	["Black"] = Color( 36, 36, 36, 255 ),
	["Red"] = Color( 255, 0, 0, 255 ),
	["Blue"] = Color( 0, 63, 255, 255 ),
	["Green"] = Color( 0, 127, 31, 255 ),
	["Yellow"] = Color( 255, 250, 0, 255 ),
}
GM.Config.LPlateCost = 2000 --Cost to buy custom license plates
GM.Config.BaseFuelCost = 3 --Cost per unit of fuel for the gas pumps
GM.Config.MaxCarHealth = 100
GM.Config.CarInsuranceTaxInterval = 60 *60 *12 --Time in seconds of continuous play time to wait before billing a player for the cars they own
GM.Config.CarInsuranceBillTime = 86400 *7 --Time in seconds a car insurance bill should last for before default
GM.Config.CarInsuranceBillScale = 0.100 --How much to scale the full repair cost +tax of a vehicle by for setting the price of a car insurance bill

--NOTES FOR JAIL/LICENSE/TICKET SETTINGS
--Keep string lengths and history counts small, or it could end up saving a large amount of data
--IF the data exceeds ~63kb the net messages for the police computer will start to FAIL!
--Saving a large amount of data may also cause a small amount of lag during the save

--[[ Jail Settings ]]--
GM.Config.MinJailTime = 1 *60 --Min time a player can be jailed for in seconds
GM.Config.MaxJailTime = 60 *60 *10 --Max time a player can be jailed for in seconds
GM.Config.MaxJailReasonLen = 64 --Max string length for jail reason
GM.Config.MaxJailFreeReasonLen = 64 --Max string length for early release reason
GM.Config.MaxArrestHistory = 10 --Max number of past arrests to keep track of
GM.Config.MaxWarrantReasonLength = 64 --Max string length for warrant reason

--[[ License/Ticket Settings ]]--
GM.Config.MaxTicketReasonLength = 64 --Max string length for a ticket reason
GM.Config.MinTicketPrice = 10 --Min price a player may set for a ticket
GM.Config.MaxTicketPrice = 2500 --Max price a player may set for a ticket
GM.Config.MaxOutstandingTickets = 10 --Max number of unpaid tickets a player can have, if at max a cop may not write any more tickets for that player
GM.Config.MaxTicketHistory = 10 --Max number of paid tickets to keep track of
GM.Config.MaxLicenseDotHistory = 10 --Max number of past license dots to keep track of
GM.Config.MaxLicenseDotReasonLength = 64 --Max string length for a license dot reason
GM.Config.MinLicenseRevokeTime = 1 *60 --Min time a player may revoke another player's license for in seconds
GM.Config.MaxLicenseRevokeTime = 48 *60 *60 --Max time a player may revoke another player's license for in seconds

--[[ Dispatch Settings ]]--
GM.Config.Text911CoolDown = 30

--[[ Job Lockers ]]--
GM.Config.JobLockerItems = {
	[2] = { --Police locker
		["Night Stick"] = 1,
		["Radar Gun"] = 1,
		["Spike Strip"] = 1,
		["Battering Ram"] = 1,
		["Taser"] = 1,
		["Police Badge"] = 1,
		["Police Issue Glock-20 Undercover"] = 1,
		
		["Police Issue M4A1"] = 1,
		//["Police Issue G3A3"] = 1,
		//["Police Issue KS-23"] = 1,
		["Police Issue MP5A5"] = 1,
		["Police Issue M24"] = 1,
		--["Police Issue M3 Super 90"] = 1,
		["Police Issue Glock-20"] = 1,
		["Police Issue P226"] = 1,
		["Police Issue Flash Grenade"] = 2,
		["Police Issue Pepper Spray"] = 5,

		["Police Issue 5.56x45MM 60 Rounds"] = 2,
		["Police Issue .357 SIG 30 Rounds"] = 2,
		["Police Issue 10x25MM 60 Rounds"] = 2,
		//["Police Issue 12 Gauge 16 Rounds"] = 2,
		["Police Issue 7.62x51MM 40 Rounds"] = 2,
		["Police Issue 9x19MM 60 Rounds"] = 2,
		//["Police Issue 23x75MMR 40 Rounds"] = 2,
		
		//["Police Issued Suppressor"] = 1,
		["Police Issued EoTech 553"] = 1,
		["Police Issued CompM4"] = 1,
		["Police Issued Foregrip"] = 1,
		["Police Issued ELCAN C79"] = 1,

		["Police Issue Traffic Cone"] = 10,
		["Police Issue Traffic Board"] = 1,
		["Police Issue Spare Tire Kit"] = 2,
		["Police Issue Traffic Barrel"] = 10,
		["Police Issue Checkpoint"] = 1,
        //["Vehicle Parking Boot"] = 1,
        ["Police Issued Clamp"] = 1,
		--["Police Issued Riot Shield"] = 1,
        ["Engine Overhaul"] = 1,
	},
	[3] = { --EMS locker
		["Government Issue First Aid Kit"] = 1,
		["Government Issue Medical Supplies"] = 32,
		["Morphine Applicator"] = 1,
		//["Blood Draw Syringe"] = 1,
		["Patient Clipboard"] = 1,
		["Police Issue Traffic Cone"] = 10,
		["Police Issue Spare Tire Kit"] = 2,
		["Government Issue Barrier"] = 2,
		["Government Issue Traffic Barrel"] = 10,
		["Engine Overhaul"] = 1,
	},
	[4] = { --FD locker
		["Police Issue Traffic Cone"] = 10,
		["Police Issue Spare Tire Kit"] = 2,
		["Government Issue Traffic Barrel"] = 10,
		["Goverment Issued Taser"] = 1,
		["Government Issue Barrier"] = 2,
        ["Engine Overhaul"] = 1,
        ["Fire Axe"] = 1,
        ["Government Issued Large Metal Ladder"] = 1,
        ["Government Issued Metal Ladder"] = 1,
	},	
	[13] = { --Secret service locker
		["Government Issue Glock-20"] = 1,
		["Government Issue M4A1"] = 1,
		["Government Issue 10x25MM 60 Rounds"] = 2,
		["Government Issue Handcuffs"] = 1,
		["Secret Service Badge"] = 1,
	    ["Government Issue Barrier"] = 6,
		["Government Issue Traffic Barrel"] = 10,
		["Government Issued Taser"] = 1,
		["Government Issue Pepper Spray"] = 2,
		["Goverment Issued Earpiece"] = 1,
		["Government Issue 5.56x45MM 60 Rounds"] = 1,
	},
}

--[[ Job Player Caps ]]--
GM.Config.Job_Taxi_PlayerCap = {
	Min = 2, --The smallest number of players that may become this job
	Max = 4, --The largest number of players that may become this job
	MinStart = 4, --At or below this number of total players, only Min amount of players may become this job 
	MaxEnd = 60 --At or above this number of total players, the job cap will be at the Max set value
}
GM.Config.Job_Tow_PlayerCap = {
	Min = 2, --The smallest number of players that may become this job
	Max = 3, --The largest number of players that may become this job
	MinStart = 4, --At or below this number of total players, only Min amount of players may become this job 
	MaxEnd = 60 --At or above this number of total players, the job cap will be at the Max set value
}
GM.Config.Job_BusDriver_PlayerCap = {
	Min = 2, --The smallest number of players that may become this job
	Max = 3, --The largest number of players that may become this job
	MinStart = 4, --At or below this number of total players, only Min amount of players may become this job 
	MaxEnd = 60 --At or above this number of total players, the job cap will be at the Max set value
}
GM.Config.Job_SalesTruck_PlayerCap = {
	Min = 1, --The smallest number of players that may become this job
	Max = 3, --The largest number of players that may become this job
	MinStart = 1, --At or below this number of total players, only Min amount of players may become this job 
	MaxEnd = 50 --At or above this number of total players, the job cap will be at the Max set value
}
GM.Config.Job_MailTruck_PlayerCap = {
	Min = 2, --The smallest number of players that may become this job
	Max = 4, --The largest number of players that may become this job
	MinStart = 8, --At or below this number of total players, only Min amount of players may become this job 
	MaxEnd = 60 --At or above this number of total players, the job cap will be at the Max set value
}
GM.Config.Job_Police_PlayerCap = {
	Min = 4, --The smallest number of players that may become this job
	Max = 16, --The largest number of players that may become this job
	MinStart = 8, --At or below this number of total players, only Min amount of players may become this job 
	MaxEnd = 60 --At or above this number of total players, the job cap will be at the Max set value
}
GM.Config.Job_EMS_PlayerCap = {
	Min = 4, --The smallest number of players that may become this job
	Max = 7, --The largest number of players that may become this job
	MinStart = 8, --At or below this number of total players, only Min amount of players may become this job 
	MaxEnd = 60 --At or above this number of total players, the job cap will be at the Max set value
}
GM.Config.Job_Fire_PlayerCap = {
	Min = 4, --The smallest number of players that may become this job
	Max = 5, --The largest number of players that may become this job
	MinStart = 8, --At or below this number of total players, only Min amount of players may become this job 
	MaxEnd = 60 --At or above this number of total players, the job cap will be at the Max set value
}
GM.Config.Job_Lawyer_PlayerCap = {
	Min = 4, --The smallest number of players that may become this job
	Max = 8, --The largest number of players that may become this job
	MinStart = 8, --At or below this number of total players, only Min amount of players may become this job 
	MaxEnd = 60 --At or above this number of total players, the job cap will be at the Max set value
}
GM.Config.Job_Prosecutor_PlayerCap = {
	Min = 4, --The smallest number of players that may become this job
	Max = 8, --The largest number of players that may become this job
	MinStart = 8, --At or below this number of total players, only Min amount of players may become this job 
	MaxEnd = 60 --At or above this number of total players, the job cap will be at the Max set value
}
GM.Config.Job_Judge_PlayerCap = {
	Min = 2, --The smallest number of players that may become this job
	Max = 4, --The largest number of players that may become this job
	MinStart = 8, --At or below this number of total players, only Min amount of players may become this job 
	MaxEnd = 60 --At or above this number of total players, the job cap will be at the Max set value
}
GM.Config.Job_Finance_PlayerCap = {
	Min = 1, --The smallest number of players that may become this job
	Max = 1, --The largest number of players that may become this job
	MinStart = 8, --At or below this number of total players, only Min amount of players may become this job 
	MaxEnd = 60 --At or above this number of total players, the job cap will be at the Max set value
}
GM.Config.Job_SService_PlayerCap = {
	Min = 4, --The smallest number of players that may become this job
	Max = 12, --The largest number of players that may become this job
	MinStart = 8, --At or below this number of total players, only Min amount of players may become this job 
	MaxEnd = 60 --At or above this number of total players, the job cap will be at the Max set value
}
GM.Config.SSApplyInterval = 2 *60

--[[ Property Settings ]]--
GM.Config.PropertyEvictTime = 60 *60 --Time in seconds after being billed for a property to evict the owner
GM.Config.PropertyTaxInterval = 60 *60 *3 --How often (in seconds) should we bill players for a property they own?
GM.Config.PropertyTaxScale = 0.015 --How much to scale the cost of a property + tax for setting the price of a property tax bill
GM.Config.PropertyCats = {
	"Stores",
	"Apartments",
	"Warehouse",
	"House",
}

--[[ Performance Settings ]]--
GM.Config.RenderDist_Level0 = 300 --Most expensive render operations
GM.Config.RenderDist_Level1 = 768
GM.Config.RenderDist_Level2 = 1500

--[[ Driving Test Questions ]]--
GM.Config.DrivingTestQuestions = {
	{ Question = "What do you do when its a green light?", Options = { ["You begin to move"] = true, ["You stop"] = true, ["You turn off your engine"] = true } },
	{ Question = "What do you do if you see someone thats just crashed.", Options = { ["Continue driving"] = true, ["Call your friends"] = true, ["Investigate the scene"] = true } },
	{ Question = "Someone has just crashed into you and damaged your car.", Options = { ["Pull a weapon on him"] = true, ["Exchange insurance information"] = true, ["Talk shit to him while ramming his car"] = true } },
	{ Question = "Your car seems to be not functioning properly, what do you do?", Options = { ["Call the cops"] = true, ["Stand on the road to get someones attention"] = true, ["Phone up mechanical services"] = true } },
	{ Question = "You encounter a police road block and the officer tells you to turn around, do you", Options = { ["Ignore the officer and continue driving"] = true, ["Sit in your car and do nothing"] = true, ["Carefully turn around and drive"] = true } },
	{ Question = "You see a another driver driving recklessly, what do you do?", Options = { ["Inform the police"] = true, ["Drive recklessly yourself"] = true, ["Message your friend"] = true } },
	{ Question = "You have just accidentally crashed into a pole and you have injured yourself, what do you do?", Options = { ["Lie on the road and wait for someone to help"] = true, ["Follow someone until they help you"] = true, ["Call EMS"] = true } },
}

--[[ NPC Healer Settings ]]--
GM.Config.NPCHealerCost = 250 --Cost a player must pay to be healed
GM.Config.MinEMSToDisable = 2 --Minimum number of players with ems jobs to disable the healer npc

--[[ NPC Clothing Shop Settings ]]--
GM.Config.ClothingPrice = 200

--[[ Phone Settings ]]--
GM.Config.MaxTextMsgLen = 256

--[[ Skills ]]--
GM.Config.Skills = {
	["Crafting"] = { MaxLevel = 25, Const = 0.25, ReductionRatio = 0.75 /25 }, --Used for crafting table
	["Assembly"] = { MaxLevel = 25, Const = 0.25, ReductionRatio = 0.75 /25 }, --Used for assembly table
	["Botany"] = { MaxLevel = 25, Const = 0.25 }, --Used for growing weed/coco
	["Chemistry"] = { MaxLevel = 25, Const = 0.25 }, --Used for making meth
	["Cooking"] = { MaxLevel = 25, Const = 0.25 }, --Used for cooking food
	["Stamina"] = { MaxLevel = 25, Const = 0.25 }, --Used for sprint duration/regen speed
	["Gun Smithing"] = { MaxLevel = 25, Const = 0.25, ReductionRatio = 0.75 /25 }, --Used for crafting gun parts
}

--[[ Crafting ]]--
GM.Config.CraftingSounds = {
	Sound( "SantosRP/ui_repairweapon_01.mp3" ),
	Sound( "SantosRP/ui_repairweapon_02.mp3" ),
	Sound( "SantosRP/ui_repairweapon_03.mp3" ),
	Sound( "SantosRP/ui_repairweapon_04.mp3" ),
	Sound( "SantosRP/ui_repairweapon_05.mp3" ),
	Sound( "SantosRP/ui_repairweapon_06.mp3" ),
	Sound( "SantosRP/ui_repairweapon_07.mp3" ),
}

--[[ Chop Shop ]]--
GM.Config.ChopShop_CarStealDuration = 60 *1 --Time before a player can spawn a stolen car again
GM.Config.ChopShop_CarStealCooldown = 60 *5 --Time before a player can chop another car
GM.Config.ChopShop_CarChopDuration = 60 *5 --Time to chop a car