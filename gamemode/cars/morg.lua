--[[
	Name: Alfa
	For: TalosLife
	By: TalosLife
]]--

local Car = {}
Car.Make = "Morgan"
Car.Name = "Morgan Aero SS"
Car.UID = "morgan_aero"
Car.Desc = "A drivable Morgan Aero SSS by TheDanishMaster"
Car.Model = "models/tdmcars/morg_aeross.mdl"
Car.Script = "scripts/vehicles/TDMCars/morg_aeross.txt"
Car.Price = 92000
Car.TrunkSize = 500
Car.FuellTank = 80
Car.FuelConsumption = 8
Car.LPlates = {

	{

		pos = Vector( 0, 91.7, 20 ),

		ang = Angle( 0, 180, 80 ),

		scale = 0.025

	},

	{

		pos = Vector( 0, -100, 23 ),

		ang = Angle( 0, 0, 90 ),

		scale = 0.028

	}

}
GM.Cars:Register( Car )