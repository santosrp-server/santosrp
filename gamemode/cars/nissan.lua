--[[
	Name: nissan.lua
	For: TalosLife
	By: Bradley
]]--

local Car = {}
Car.Make = "Nissan"
Car.Name = "Skyline"
Car.UID = "r34tdm"
Car.Desc = "A drivable Nissan GT-R Black Edition by TheDanishMaster"
Car.Model = "models/tdmcars/skyline_r34.mdl"
Car.Script = "scripts/vehicles/TDMCars/for_crownvic.txt"
Car.Price = 34500
Car.TrunkSize = 400
Car.FuellTank = 87.75
Car.FuelConsumption = 12.5
Car.LPlates = {

	{

		pos = Vector( 0,108.5, 20.5 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.030

	},

	{

		pos = Vector( 0, -106.5, 23.4 ),
		ang = Angle( 0, 0, 83.5 ),
		scale = 0.029

	}

}
GM.Cars:Register( Car )


local Car = {}
Car.Make = "Nissan"
Car.Name = "Silvia"
Car.UID = "nissilvs15tdm"
Car.Desc = "A drivable Nissan GT-R Black Edition by TheDanishMaster"
Car.Model = "models/tdmcars/nissan_silvias15.mdl"
Car.Script = "scripts/vehicles/TDMCars/300c.txt"
Car.Price = 24000
Car.FuellTank = 87.75
Car.FuelConsumption = 12.5
Car.TrunkSize = 400
Car.LPlates = {

	{

		pos = Vector( 0,106, 18 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.034

	},

	{

		pos = Vector( 0, -104.5, 20.75 ),
		ang = Angle( 0, 0, 95 ),
		scale = 0.029

	}

}
GM.Cars:Register( Car )

local Car = {}
Car.Make = "Nissan"
Car.Name = "Leaf"
Car.UID = "nis_leaftdm"
Car.Desc = "A drivable Nissan GT-R Black Edition by TheDanishMaster"
Car.Model = "models/tdmcars/nis_leaf.mdl"
Car.Script = "scripts/vehicles/TDMCars/beetle68.txt"
Car.Price = 31000
Car.FuellTank = 87.75
Car.TrunkSize = 700
Car.FuelConsumption = 12.5
Car.LPlates = {

	{

		pos = Vector( 20,50, 53 ),
		ang = Angle( 0, 180, 25 ),
		scale = 0.034

	},

	{

		pos = Vector( 0, -102, 22.75 ),
		ang = Angle( 0, 0, 85 ),
		scale = 0.029

	}

}
GM.Cars:Register( Car )


local Car = {}
Car.Make = "Nissan"
Car.Name = "GTR Black"
Car.UID = "gtrtdm"
Car.Desc = "A drivable Nissan GT-R Black Edition by TheDanishMaster"
Car.Model = "models/tdmcars/nissan_gtr.mdl"
Car.Script = "scripts/vehicles/TDMCars/murcielago.txt"
Car.Price = 110000
Car.FuellTank = 87.75
Car.TrunkSize = 400
Car.FuelConsumption = 12.5
Car.LPlates = {

	{

		pos = Vector( 0,110, 18 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.034

	},

	{

		pos = Vector( 0, -110.5, 30.75 ),
		ang = Angle( 0, 0, 85 ),
		scale = 0.029

	}

}
GM.Cars:Register( Car )

local Car = {}
Car.Make = "Nissan"
Car.Name = "370z"
Car.UID = "370ztdm"
Car.Desc = "A drivable Nissan GT-R Black Edition by TheDanishMaster"
Car.Model = "models/tdmcars/nis_370z.mdl"
Car.Script = "scripts/vehicles/TDMCars/gallardo.txt"
Car.Price = 30000
Car.FuellTank = 87.75
Car.FuelConsumption = 12.5
Car.TrunkSize = 400
Car.LPlates = {

	{

		pos = Vector( 0,103, 17 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.034

	},

	{

		pos = Vector( 0, -100.5, 29.5 ),
		ang = Angle( 0, 0, 85 ),
		scale = 0.029

	}

}
GM.Cars:Register( Car )


local Car = {}
Car.Make = "Nissan"
Car.Name = "350z"
Car.UID = "350ztdm"
Car.Desc = "A drivable Nissan GT-R Black Edition by TheDanishMaster"
Car.Model = "models/tdmcars/350z.mdl"
Car.Script = "scripts/vehicles/TDMCars/bmwm3e92.txt"
Car.Price = 13500
Car.FuellTank = 87.75
Car.FuelConsumption = 12.5
Car.TrunkSize = 500
Car.LPlates = {

	{

		pos = Vector( 0,101, 16 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.034

	},

	{

		pos = Vector( 0, -103, 30.5 ),
		ang = Angle( 0, 0, 85 ),
		scale = 0.029

	}

}
GM.Cars:Register( Car )
