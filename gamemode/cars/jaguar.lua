--[[
	Name: jaguar.lua
	For: TalosLife
	By: TalosLife
]]--

local Car = {}
Car.Make = "Jaguar"
Car.Name = "Jaguar F-Type"
Car.UID = "jaguar_f_type"
Car.Desc = "The Jaguar F-Type V12, gmod-able by TDM"
Car.Model = "models/tdmcars/jag_ftype.mdl"
Car.Script = "scripts/vehicles/TDMCars/jag_ftype.txt"
Car.Price = 142000
Car.TrunkSize = 500
Car.FuellTank = 150
Car.FuelConsumption = 12.125
Car.LPlates = {

	{

		pos = Vector( 0, 57, 30 ), 

		ang = Angle( 0, 180, 90 ), 

		scale = 0.00			   

	},

	{

		pos = Vector( 0, -106.2, 37 ),

		ang = Angle( 0, 0, 80 ),

		scale = 0.025

	}

}
GM.Cars:Register( Car )

local Car = {}
Car.Make = "Jaguar"
Car.Name = "XFR"
Car.UID = "jaguar_xfr"
Car.Desc = "The Jaguar F-Type V12, gmod-able by TDM"
Car.Model = "models/lonewolfie/jaguar_xfr.mdl"
Car.Script = "scripts/vehicles/lwcars/jag_xfr.txt"
Car.Price = 142000
Car.TrunkSize = 500
Car.FuellTank = 150
Car.FuelConsumption = 12.125
Car.LPlates = {

	{
		pos = Vector( 0, 120, 21 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.028
	},
	{
		pos = Vector( 0, -114, 41 ),
		ang = Angle( 0, 0, 70 ),
		scale = 0.026
	}

}
GM.Cars:Register( Car )

local Car = {}
Car.Make = "Jaguar"
Car.Name = "F-Pace"
Car.UID = "jaguar_fpace"
Car.Desc = "The Jaguar F-Type V12, gmod-able by TDM"
Car.Model = "models/crsk_autos/jaguar/fpace_2016.mdl"
Car.Script = "scripts/vehicles/crsk_autos/crsk_jaguar_fpace_2016.txt"
Car.Price = 55000
Car.TrunkSize = 500
Car.FuellTank = 150
Car.FuelConsumption = 12.125
Car.LPlates = {

	{
		pos = Vector( 0, 109.8, 23 ),
		ang = Angle( 0, 180, 80 ),
		scale = 0.035
	},
	{
		pos = Vector( 0, -128, 45 ),
		ang = Angle( 0, 0, 80 ),
		scale = 0.03
	}

}
GM.Cars:Register( Car )