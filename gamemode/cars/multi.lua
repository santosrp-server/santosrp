--[[
	Name: multi.lua
	For: OakRP
	By: Robbot
]]--

local Car = {}
Car.Make = "DMC"
Car.Name = "Delorean DMC-12"
Car.UID = "delorean_dmc_12"
Car.Desc = "The Delorean, Marty, We've got to go back!"
Car.Model = "models/tdmcars/del_dmc.mdl"
Car.Script = "scripts/vehicles/TDMCars/delorean.txt"
Car.Price = 22000
Car.TrunkSize = 1000
Car.FuellTank = 200
Car.FuelConsumption = 12.125
Car.LPlates = {

	{

		pos = Vector( 0, -101.5, 41.5 ),

		ang = Angle( 0, 0, 75 ),

		scale = 0.025

	}

}
GM.Cars:Register( Car )
