--[[
	Name: astonmartin.lua
	For: TalosLife
	By: TalosLife
]]--

local Car = {}
Car.Make = "Volkswagen"
Car.Name = "Volkswagen Golf GTI 2014"
Car.UID = "volkswagen_golf_gti"
Car.Desc = "A drivable Volkswagen Golf GTI 2014 by TheDanishMaster"
Car.Model = "models/tdmcars/vw_golfgti_14.mdl"
Car.Script = "scripts/vehicles/TDMCars/bmwm3e92.txt"
Car.TrunkSize = 500
Car.Price = 19000
Car.FuellTank = 80
Car.FuelConsumption = 12.375
Car.LPlates = {

	{

		pos = Vector( 0, 103.5, 22.8 ),

		ang = Angle( 0, 180, 80 ),

		scale = 0.020

	},

	{

		pos = Vector( 0, -101, 21.8 ),

		ang = Angle( 0, 0, 80 ),

		scale = 0.025

	}

}
GM.Cars:Register( Car )

local Car = {}
Car.Make = "Volkswagen"
Car.Name = "Volkswagen Camper 1965"
Car.UID = "volkswagen_camper"
Car.Desc = "A drivable Volkswagen Camper 1965 by TheDanishMaster"
Car.Model = "models/tdmcars/vw_camper65.mdl"
Car.Script = "scripts/vehicles/TDMCars/beetle68.txt"
Car.TrunkSize = 500
Car.Price = 20000
Car.FuellTank = 80
Car.FuelConsumption = 12.375
Car.LPlates = {

	{

		pos = Vector( 0, 114.5, 21.5 ),

		ang = Angle( 0, 180, 90 ),

		scale = 0.030

	},

	{

		pos = Vector( 0, -100.8, 34.9 ),

		ang = Angle( 0, 0, 90 ),

		scale = 0.045

	}

}
GM.Cars:Register( Car )

local Car = {}
Car.Make = "Volkswagen"
Car.Name = "Volkswagen New Beetle Convertible"
Car.UID = "volkswagen_new_beetle"
Car.Desc = "A drivable VW Beetle by TheDanishMaster"
Car.Model = "models/tdmcars/vw_beetleconv.mdl"
Car.Script = "scripts/vehicles/TDMCars/300c.txt"
Car.TrunkSize = 500
Car.Price = 25000
Car.FuellTank = 80
Car.FuelConsumption = 12.375
Car.LPlates = {

	{

		pos = Vector( 0, 103.8, 21.5 ),

		ang = Angle( 0, 180, 85 ),

		scale = 0.030

	},

	{

		pos = Vector( 0, -96.4, 23 ),

		ang = Angle( 0, 0, 75 ),

		scale = 0.030

	}

}
GM.Cars:Register( Car )

local Car = {}
Car.Make = "Volkswagen"
Car.Name = "Volkswagen Golf R32"
Car.UID = "volkswagen_new_golf_r32"
Car.Desc = "A drivable VW Golf R32 by TheDanishMaster"
Car.Model = "models/tdmcars/vw_golfr32.mdl"
Car.Script = "scripts/vehicles/TDMCars/for_crownvic.txt"
Car.TrunkSize = 500
Car.Price = 13000
Car.FuellTank = 80
Car.FuelConsumption = 12.375
Car.LPlates = {

	{

		pos = Vector( 0, 97, 24 ),

		ang = Angle( 0, 180, 90 ),

		scale = 0.034

	},

	{

		pos = Vector( 0, -97, 24 ),

		ang = Angle( 0, 0, 80 ),

		scale = 0.025

	}

}
GM.Cars:Register( Car )

local Car = {}
Car.Make = "Volkswagen"
Car.Name = "Volkswagen Golf VR6 GTi"
Car.UID = "volkswagen_golf_vr6"
Car.Desc = "A drivable Golf VR6 GTi by TheDanishMaster"
Car.Model = "models/tdmcars/golfvr6_mk3.mdl"
Car.Script = "scripts/vehicles/TDMCars/300c.txt"
Car.TrunkSize = 500
Car.Price = 20000
Car.FuellTank = 80
Car.FuelConsumption = 12.375
Car.LPlates = {

	{

		pos = Vector( 0, 100.5, 19.5 ),

		ang = Angle( 0, 180, 90 ),

		scale = 0.022

	},

	{

		pos = Vector( 0, -90, 37.5 ),

		ang = Angle( 0, 0, 80 ),

		scale = 0.028

	}

}
GM.Cars:Register( Car )

local Car = {}
Car.Make = "Volkswagen"
Car.Name = "Volkswagen Golf R32"
Car.UID = "vw_golfr32tdm"
Car.Desc = "A drivable Golf VR6 GTi by TheDanishMaster"
Car.Model = "models/tdmcars/vw_golfr32.mdl"
Car.Script = "scripts/vehicles/TDMCars/300c.txt"
Car.TrunkSize = 500
Car.Price = 13000
Car.FuellTank = 80
Car.FuelConsumption = 12.375
Car.LPlates = {

	{

		pos = Vector( 0, 97, 24 ),

		ang = Angle( 0, 180, 90 ),

		scale = 0.034

	},

	{

		pos = Vector( 0, -97, 24 ),

		ang = Angle( 0, 0, 80 ),

		scale = 0.025

	}

}
GM.Cars:Register( Car )

local Car = {}
Car.Make = "Volkswagen"
Car.Name = "Volkswagen Golf MKII"
Car.UID = "volkswagen_golf_mkii"
Car.Desc = "A drivable Golf MKII by TheDanishMaster"
Car.Model = "models/tdmcars/golf_mk2.mdl"
Car.Script = "scripts/vehicles/TDMCars/beetle68.txt"
Car.TrunkSize = 500
Car.Price = 5500
Car.FuellTank = 80
Car.FuelConsumption = 12.375
Car.LPlates = {

	{

		pos = Vector( 0, 95.8, 22 ),

		ang = Angle( 0, 180, 90 ),

		scale = 0.034

	},

	{

		pos = Vector( 0, -91, 43 ),

		ang = Angle( 0, 0, 80 ),

		scale = 0.033

	}

}
GM.Cars:Register( Car )