--[[
	Name: ford.lua
	For: TalosLife
	By: Bradley
]]--

local Car = {}
Car.Make = "Ford"
Car.Name = "Transit"
Car.UID = "transittdm"
Car.Desc = "A drivable Ford Transit by TheDanishMaster"
Car.Model = "models/tdmcars/ford_transit.mdl"
Car.Script = "scripts/vehicles/TDMCars/transit.txt"
Car.Price = 22000
Car.TrunkSize = 1200
Car.FuellTank = 67
Car.FuelConsumption = 12.5
Car.LPlates = {

	{

		pos = Vector( 0, 122, 25.5 ),

		ang = Angle( 0, 180, 90 ),

		scale = 0.034

	},

	{

		pos = Vector( 0, -112, 40 ),

		ang = Angle( 0, 0, 90 ),

		scale = 0.029

	}

}
GM.Cars:Register( Car )

local Car = {}
Car.Make = "Ford"
Car.Name = "Raptor"
Car.UID = "ford_raptor"
Car.Desc = "Ford Raptor"
Car.Model = "models/tdmcars/for_raptor.mdl"
Car.Script = "scripts/vehicles/TDMCars/raptorsvt.txt"
Car.Price = 50000
Car.TrunkSize = 700
Car.FuellTank = 162
Car.FuelConsumption = 8.125
Car.LPlates = {

	{

		pos = Vector( 0, 137, 28 ),

		ang = Angle( 0, 180, 90 ),

		scale = 0.025

	},

	{

		pos = Vector( 0, -129.6, 32.5 ),

		ang = Angle( 0, 0, 90 ),

		scale = 0.025

	}

}
GM.Cars:Register( Car )

local Car = {}
Car.Make = "Ford"
Car.Name = "GT05"
Car.UID = "gt05tdm"
Car.Desc = "A drivable Ford GT 05 by TheDanishMaster"
Car.Model = "models/tdmcars/gt05.mdl"
Car.Script = "scripts/vehicles/TDMCars/gt05.txt"
Car.Price = 450000
Car.TrunkSize = 500
Car.FuellTank = 112
Car.FuelConsumption = 21
Car.LPlates = {

	{

		pos = Vector( 0, 114, 15 ),

		ang = Angle( 0, 180, 100 ),

		scale = 0.031

	},

	{

		pos = Vector( 0, -102.5, 30.5 ),

		ang = Angle( 0, 0, 110 ),

		scale = 0.037

	}

}
GM.Cars:Register( Car )

local Car = {}
Car.Make = "Ford"
Car.Name = "F350"
Car.UID = "f350"
Car.Desc = "A drivable Ford F350 SuperDuty by TheDanishMaster"
Car.Model = "models/tdmcars/for_f350.mdl"
Car.Script = "scripts/vehicles/TDMCars/f350.txt"
Car.Price = 32000
Car.TrunkSize = 500
Car.FuellTank = 200
Car.FuelConsumption = 3.5
Car.LPlates = {

	{

		pos = Vector( 0, 143.5, 27 ),

		ang = Angle( 0, 180, 90 ),

		scale = 0.026

	},

	{

		pos = Vector( 0, -155.5, 34.8 ),

		ang = Angle( 0, 0, 90 ),

		scale = 0.029

	}

}
GM.Cars:Register( Car )

local Car = {}
Car.Make = "Ford"
Car.Name = "Mustang GT"
Car.UID = "ford_mustang"
Car.Desc = "A drivable Ford Mustang GT by TheDanishMaster"
Car.Model = "models/tdmcars/for_mustanggt.mdl"
Car.Script = "scripts/vehicles/TDMCars/mustanggt.txt"
Car.Price = 45000
Car.TrunkSize = 500
Car.FuellTank = 72
Car.FuelConsumption = 13
Car.LPlates = {

	{
		pos = Vector( 0, -114, 27 ),
		ang = Angle( 0, 0, 90 ),
		scale = 0.032
	}

}
GM.Cars:Register( Car )

local Car = {}
Car.Make = "Ford"
Car.Name = "SVT Cobra R"
Car.UID = "ford_svt_cobra"
Car.Desc = "A drivable Ford Mustang GT by TheDanishMaster"
Car.Model = "models/lonewolfie/ford_foxbody_stock.mdl"
Car.Script = "scripts/vehicles/lwcars/ford_foxbody.txt"
Car.Price = 15000
Car.TrunkSize = 300
Car.FuellTank = 72
Car.FuelConsumption = 13
Car.LPlates = {

	{
		pos = Vector( 0, -107, 34 ),
		ang = Angle( 0, 0, 90 ),
		scale = 0.028
	},
	{
		pos = Vector( 0, 115, 20 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.028
	}

}
GM.Cars:Register( Car )

local Car = {}
Car.Make = "Ford"
Car.Name = "Focus RS"
Car.UID = "focusrstdm"
Car.Desc = "A drivable Ford Focus RS by TheDanishMaster"
Car.Model = "models/tdmcars/focusrs.mdl"
Car.Script = "scripts/vehicles/TDMCars/focusrs.txt"
Car.Price = 30000
Car.TrunkSize = 500
Car.FuellTank = 95
Car.FuelConsumption = 15.6
Car.LPlates = {

	{

		pos = Vector( 0, 101, 21.5 ),

		ang = Angle( 0, 180, 90 ),

		scale = 0.035

	},

	{

		pos = Vector( 0, -103, 38 ),

		ang = Angle( 0, 0, 85 ),

		scale = 0.032

	}

}
GM.Cars:Register( Car )

local Car = {}
Car.Make = "Ford"
Car.Name = "Ford Focus SVT"
Car.UID = "focusrstdm1"
Car.Desc = "A drivable Ford Focus SVT by TheDanishMaster"
Car.Model = "models/tdmcars/for_focussvt.mdl"
Car.Script = "scripts/vehicles/TDMCars/focussvt.txt"
Car.Price = 3000
Car.TrunkSize = 500
Car.FuellTank = 95
Car.FuelConsumption = 15.6
Car.LPlates = {

	{

		pos = Vector( 0, 101, 21.5 ),

		ang = Angle( 0, 180, 90 ),

		scale = 0.035

	},

	{

		pos = Vector( 0, -103, 38 ),

		ang = Angle( 0, 0, 85 ),

		scale = 0.032

	}

}
GM.Cars:Register( Car )