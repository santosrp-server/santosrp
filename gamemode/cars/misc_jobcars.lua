--[[
	Name: misc_jobcars.lua
	For: TalosLife
	By: TalosLife
]]--

local Car = {}
Car.Make = "Ford"
Car.Name = "Crown Victoria Taxi"
Car.UID = "taxi_cab"
Car.Job = "JOB_TAXI"
Car.Desc = "A taxi cab."
Car.Model = "models/tdmcars/crownvic_taxi.mdl"
Car.Script = "scripts/vehicles/TDMCars/crownvic_taxi.txt"
Car.TrunkSize = 300
Car.NoTrunk = true
Car.FuellTank = 100
Car.FuelConsumption = 20
Car.LPlates = {

	{

		pos = Vector( 3, 103, 20 ),

		ang = Angle( 0, 180, 85 ),

		scale = 0.035

	},

	{

		pos = Vector( 3, -94.7, 29.9 ),

		ang = Angle( 0, 0, 70 ),

		scale = 0.035

	}

}
GM.Cars:RegisterJobCar( Car )

local Car = {}
Car.Make = "Dodge"
Car.Name = "Flatbed Tow Truck"
Car.UID = "tow_flatbed"
Car.Job = "JOB_TOW"
Car.Desc = "A tow truck."
Car.Model = "models/sentry/flatbed.mdl"
Car.Script = "scripts/vehicles/lwcars/merc_sprinter_swb.txt"
Car.TrunkSize = 300
Car.NoTrunk = true
Car.FuellTank = 65
Car.FuelConsumption = 28
Car.LPlates = {

	{

		pos = Vector( 0, 184.5, 24.60 ),

		ang = Angle( 0, 180, 90 ),

		scale = 0.025

	},

	{

		pos = Vector( 0, -187.5, 29 ),

		ang = Angle( 0, 0, 90 ),

		scale = 0.021

	}

}
GM.Cars:RegisterJobCar( Car )

local Car = {}
Car.Make = "Mercedes"
Car.Name = "Sprinter SWB"
Car.UID = "mail_truck"
Car.Job = "JOB_MAIL"
Car.Desc = "A mail truck."
Car.Model = "models/LoneWolfie/merc_sprinter_swb.mdl"
Car.Script = "scripts/vehicles/lwcars/merc_sprinter_swb.txt"
Car.TrunkSize = 300
Car.NoTrunk = true
Car.FuellTank = 200
Car.FuelConsumption = 20
Car.LPlates = {

	{

		pos = Vector( 0, 132.5, 24.4 ),

		ang = Angle( 0, 180, 90 ),

		scale = 0.020

	},

	{

		pos = Vector( -17, -113, 24.8 ),

		ang = Angle( 0, 0, 85 ),

		scale = 0.022

	}

}
GM.Cars:RegisterJobCar( Car )

local Car = {}
Car.Make = "Mercedes"
Car.Name = "Sprinter SWB"
Car.UID = "sales_truck"
Car.Job = "JOB_SALES_TRUCK"
Car.Desc = "A mail truck."
Car.Model = "models/LoneWolfie/merc_sprinter_swb.mdl"
Car.Script = "scripts/vehicles/lwcars/merc_sprinter_swb.txt"
Car.TrunkSize = 300
Car.NoTrunk = true
Car.FuellTank = 200
Car.FuelConsumption = 20
Car.LPlates = {

	{

		pos = Vector( 0, 132.5, 24.4 ),

		ang = Angle( 0, 180, 90 ),

		scale = 0.020

	},

	{

		pos = Vector( -17, -113, 24.8 ),

		ang = Angle( 0, 0, 85 ),

		scale = 0.022

	}

}
GM.Cars:RegisterJobCar( Car )

local Car = {}
Car.Make = "MTL"
Car.Name = "Bus"
Car.UID = "mtl_bus"
Car.Job = "JOB_BUS_DRIVER"
Car.Desc = "A city bus."
Car.Model = "models/sligwolf/bus/bus.mdl"
Car.Script = "scripts/oakroleplay/sw_bus.txt"
Car.TrunkSize = 300
Car.NoTrunk = true
Car.VehicleTable = "sw_bus"
Car.FuellTank = 200
Car.FuelConsumption = 20
Car.LPlates = {

	{
		pos = Vector( 0, 125, 30 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.028
	},
	{
		pos = Vector( 0, -183, 33 ),
		ang = Angle( 0, 0, 90 ),
		scale = 0.030
	}

}
GM.Cars:RegisterJobCar( Car )