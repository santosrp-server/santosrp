--[[
	Name: lamborghini.lua
	For: TalosLife
	By: Bradley
]]--

local Car = {}
Car.Make = "Lamborghini"
Car.Name = "Gallardo"
Car.UID = "gallardotdm"
Car.Desc = "A drivable Lamborghini Gallardo by TheDanishMaster"
Car.Model = "models/tdmcars/gallardo.mdl"
Car.Script = "scripts/vehicles/TDMCars/gallardo.txt"
Car.Price = 199000
Car.TrunkSize = 500
Car.FuellTank = 91
Car.FuelConsumption = 9.75
Car.LPlates = {

	{

		pos = Vector( 0, -103, 19 ),

		ang = Angle( 0, 0, 85 ),

		scale = 0.026

	}

}
GM.Cars:Register( Car )

local Car = {}
Car.Make = "Lamborghini"
Car.Name = "Murcielago"
Car.UID = "murcielagotdm"
Car.Desc = "A drivable Lamborghini Murcielago by TheDanishMaster"
Car.Model = "models/tdmcars/murcielago.mdl"
Car.Script = "scripts/vehicles/TDMCars/murcielago.txt"
Car.Price = 365000
Car.TrunkSize = 500
Car.FuellTank = 120
Car.FuelConsumption = 8.75
Car.LPlates = {

	{

		pos = Vector( 0, -108, 31.5 ),

		ang = Angle( 0, 0, 80 ),

		scale = 0.026

	}

}
GM.Cars:Register( Car )

local Car = {}
Car.Make = "Lamborghini"
Car.Name = "Reventon Roadster"
Car.UID = "reventonrtdm"
Car.Desc = "A drivable Lamborghini Reventon Roadster by TheDanishMaster"
Car.Model = "models/tdmcars/reventon_roadster.mdl"
Car.Script = "scripts/vehicles/TDMCars/reventonr.txt"
Car.Price = 962000
Car.TrunkSize = 500
Car.FuellTank = 90
Car.FuelConsumption = 8
Car.LPlates = {

	{

		pos = Vector( 0, -113, 32 ),

		ang = Angle( 0, 0, 70 ),

		scale = 0.025

	}

}
GM.Cars:Register( Car )