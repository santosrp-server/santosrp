--[[
	Name: fire_cars.lua
	For: TalosLife
	By: TalosLife
]]--

local Car = {}
Car.Make = "Fire"
Car.Name = "Rescue Firetruck"
Car.UID = "fire_truck_rescue"
Car.Job = "JOB_FIREFIGHTER"
Car.Desc = "A rescue fire truck."
Car.Model = "models/perrynsvehicles/rescue_truck/rescue_truck.mdl"
Car.Script = "scripts/vehicles/perryn/rescue_truck.txt"
Car.TrunkSize = 300
Car.FuellTank = 120
Car.FuelConsumption = 20
Car.LPlates = {

	{
		pos = Vector( 0, 163, 26 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.035
	},
	{
		pos = Vector( 0, -227, 25.5 ),
		ang = Angle( 0, 0, 90 ),
		scale = 0.032
	}

}
GM.Cars:RegisterJobCar( Car )

local Car = {}
Car.Make = "Fire"
Car.Name = "Large Truck"
Car.UID = "fire_truck_large"
Car.Job = "JOB_FIREFIGHTER"
Car.Desc = "A large fire truck."
Car.Model = "models/perrynsvehicles/pierce_pumper/pierce_pumper.mdl"
Car.Script = "scripts/vehicles/perryn/pierce_pumper.txt"
Car.TrunkSize = 300
Car.FuellTank = 120
Car.FuelConsumption = 20
Car.LPlates = {

	{
		pos = Vector( 0, 197, 33 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.035
	},
	{
		pos = Vector( 0, -218, 34 ),
		ang = Angle( 0, 0, 90 ),
		scale = 0.032
	}

}
GM.Cars:RegisterJobCar( Car )

local Car = {}
Car.Make = "Chevrolet"
Car.Name = "Suburban"
Car.UID = "fire_tahoe"
Car.Job = "JOB_FIREFIGHTER"
Car.Desc = "A tahoe."
Car.Model = "models/lonewolfie/chev_suburban_pol.mdl"
Car.Script = "scripts/vehicles/lwcars/chev_suburban.txt"
Car.TrunkSize = 300
Car.FuellTank = 120
Car.FuelConsumption = 20
Car.LPlates = {

	{

		pos = Vector( 0, 136.2, 22.5 ),

		ang = Angle( 0, 180, 117 ),

		scale = 0.030

	},

	{

		pos = Vector( 0, -129.5, 46.6 ),

		ang = Angle( 0, 0, 90 ),

		scale = 0.029

	}

}

GM.Cars:RegisterJobCar( Car )

local Car = {}
Car.Make = "Ford"
Car.Name = "Ford F150 Police"
Car.UID = "fire_first"
Car.Job = "JOB_FIREFIGHTER"
Car.Desc = "A fire truck."
Car.Model = "models/talonvehicles/tal_f150_pol.mdl"
Car.Script = "scripts/vehicles/tal_f150pol.txt"
Car.TrunkSize = 300
Car.FuellTank = 120
Car.FuelConsumption = 20
Car.LPlates = {

	{

		pos = Vector( 1, 127, 26 ),

		ang = Angle( 0, 180, 90 ),

		scale = 0.022

	},

	{

		pos = Vector( 0, -139, 28 ),

		ang = Angle( 0, 0, 95 ),

		scale = 0.024

	}

}
GM.Cars:RegisterJobCar( Car )
