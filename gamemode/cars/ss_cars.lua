local Car = {}
Car.Make = "Chevrolet"
Car.Name = "Suburban Undercover"
Car.UID = "police_suburban_undercover"
Car.Job = "JOB_SSERVICE"
Car.Desc = "A Suburban undercover."
Car.Model = "models/lonewolfie/chev_suburban_pol_und.mdl"
Car.Script = "scripts/vehicles/lwcars/chev_suburban.txt"
Car.TrunkSize = 300
Car.FuellTank = 120
Car.FuelConsumption = 20
Car.LPlates = {
	{
		pos = Vector( 0, 136.2, 22.5 ),
		ang = Angle( 0, 180, 117 ),
		scale = 0.030
	},

	{
		pos = Vector( 0, -129.5, 46.6 ),
		ang = Angle( 0, 0, 90 ),
		scale = 0.029
	}
}

GM.Cars:RegisterJobCar( Car )

local Car = {}
Car.Make = "Jaguar"
Car.Name = "XFR Special Escort Group"
Car.UID = "jag_xfr_pol_und"
Car.Job = "JOB_SSERVICE"
Car.Desc = ""
Car.Model = "models/lonewolfie/jaguar_xfr_pol_und.mdl"
Car.Script = "scripts/vehicles/lwcars/jag_xfr_pol.txt"
Car.FuellTank = 120
Car.FuelConsumption = 20

GM.Cars:RegisterJobCar( Car )

local Car = {}
Car.Make = "Cadillac"
Car.Name = "Mayor's Limousine"
Car.UID = "perryn_cadillac_dts_limousine"
Car.Job = "JOB_POLICE"
Car.Desc = "A limo for the mayor"
Car.Model = "models/perrynsvehicles/cadillac_dts_limousine/cadillac_dts_limousine.mdl"
Car.Script = "scripts/vehicles/tdmcars/for_crownvic.txt"
Car.FuellTank = 70
Car.FuelConsumption = 9
GM.Cars:RegisterJobCar( Car )