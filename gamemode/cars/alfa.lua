--[[
	Name: Alfa
	For: TalosLife
	By: TalosLife
]]--

local Car = {}
Car.Make = "Alfa"
Car.Name = "Alfa Romeo Giulietta"
Car.UID = "alfa_romeo"
Car.Desc = "A drivable Aston Martin DBS by TheDanishMaster"
Car.Model = "models/tdmcars/alfa_giulietta.mdl"
Car.Script = "scripts/vehicles/TDMCars/alfa_giulietta.txt"
Car.Price = 28000
Car.TrunkSize = 700
Car.FuellTank = 80
Car.FuelConsumption = 14.375
Car.LPlates = {

	{
		pos = Vector( 0, 107, 15 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.035
	},
	{
		pos = Vector( 0, -101, 26.5 ),
		ang = Angle( 0, 0, 75 ),
		scale = 0.025
	}

}
GM.Cars:Register( Car )