--[[
	Name: dodge.lua
	For: TalosLife
	By: TalosLife
]]--

local Car = {}
Car.Make = "Dodge"
Car.Name = "Dodge Challenger 1970"
Car.UID = "dodge_challenger_1970"
Car.Desc = "The Dodge, gmod-able by TDM"
Car.Model = "models/tdmcars/dod_challenger70.mdl"
Car.Script = "scripts/vehicles/TDMCars/challenger70.txt"
Car.Price = 32000
Car.TrunkSize = 700
Car.FuellTank = 150
Car.FuelConsumption = 12.125
Car.LPlates = {

	{

		pos = Vector( 0, 122.5, 29 ),

		ang = Angle( 0, 180, 90 ),

		scale = 0.0

	},

	{

		pos = Vector( 0, -116.7, 27 ),

		ang = Angle( 0, 0, 115 ),

		scale = 0.025

	}

}
GM.Cars:Register( Car )

local Car = {}
Car.Make = "Dodge"
Car.Name = "Dodge RAM SRT10"
Car.UID = "dodge_ram_srt10"
Car.Desc = "The Dodge, gmod-able by TDM"
Car.Model = "models/tdmcars/dodgeram.mdl"
Car.Script = "scripts/vehicles/TDMCars/dodgeram.txt"
Car.Price = 23500
Car.TrunkSize = 700
Car.FuellTank = 150
Car.FuelConsumption = 12.125
Car.LPlates = {

	{

		pos = Vector( 0, 122.3, 21.5 ),

		ang = Angle( 0, 180, 90 ),

		scale = 0.025

	},

	{

		pos = Vector( 0, -119.5, 27 ),

		ang = Angle( 0, 0, 90 ),

		scale = 0.020

	}

}
GM.Cars:Register( Car )

local Car = {}
Car.Make = "Dodge"
Car.Name = "Dodge Challenger 2015"
Car.UID = "dodge_challenger_2015"
Car.Desc = "The Dodge, gmod-able by TDM"
Car.Model = "models/tdmcars/dod_challenger15.mdl"
Car.Script = "scripts/vehicles/TDMCars/dod_challenger15.txt"
Car.Price = 52000
Car.TrunkSize = 700
Car.FuellTank = 150
Car.FuelConsumption = 12.125
Car.LPlates = {

	{

		pos = Vector( 0, 124.4, 26 ),

		ang = Angle( 0, 180, 90 ),

		scale = 0.023

	},

	{

		pos = Vector( 0, -121.3, 29.7 ),

		ang = Angle( 0, 0, 85 ),

		scale = 0.022

	}

}
GM.Cars:Register( Car )

local Car = {}
Car.Make = "Dodge"
Car.Name = "SRT Viper GTS"
Car.UID = "dodge_viper"
Car.Desc = "The Dodge, gmod-able by TDM"
Car.Model = "models/tdmcars/vip_viper.mdl"
Car.Script = "scripts/vehicles/TDMCars/vipviper.txt"
Car.Price = 108000
Car.TrunkSize = 500
Car.FuellTank = 150
Car.FuelConsumption = 12.125
Car.LPlates = {

	{

		pos = Vector( 0, 107.7, 18 ),

		ang = Angle( 0, 180, 90 ),

		scale = 0.025

	},

	{

		pos = Vector( 0, -107.2, 35 ),

		ang = Angle( 0, 0, 85 ),

		scale = 0.025

	}

}
GM.Cars:Register( Car )