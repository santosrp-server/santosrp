--[[
	Name: cl_qmenu_tab_rules.lua
	For: TalosLife
	By: TalosLife
]]--

surface.CreateFont( "MapFont", {size = 20, weight = 400, font = "DermaLarge"} )

local Panel = {}
Panel.Waypoints = {}
Panel.Signs = {}

/*
	Waypoints
*/
function Panel:AddWaypoint(id, image, name, color, scale, pos, ang)
	-- Build position
	pos = isvector(pos) and pos or Vector(0, 0, 0)

	local newPos = {x = 0, y = 0}

	newPos.x = ((pos.x + -GAMEMODE.Config.MapDimensions.TopLeft[1])/(-GAMEMODE.Config.MapDimensions.TopLeft[1] + GAMEMODE.Config.MapDimensions.BottomRight[1]))
	newPos.y = ((pos.y + -GAMEMODE.Config.MapDimensions.TopLeft[2])/-(GAMEMODE.Config.MapDimensions.TopLeft[2] + -GAMEMODE.Config.MapDimensions.BottomRight[2]))

	-- Build rotation
	local newAng = isangle(ang) and ang[2] or 0

	self.Waypoints[id] = {
		image = image,
		name = name,
		color = color,
		scale = scale or 1,
		pos = newPos,
		ang = newAng
	}
end

function Panel:RemoveWaypoint(id)
	self.Waypoints[id] = nil
end

function Panel:RemoveWaypointsWithTag(tag)
	for k, v in pairs(self.Waypoints) do
		if string.find(k, tag, nil, true) then
			self.Waypoints[k] = nil
		end
	end
end

function Panel:GetWaypoints()

	self:AddWaypoint("localplayer", "map_player", "Me", color_white, 1, LocalPlayer():GetPos(), LocalPlayer():GetAngles())

	if GAMEMODE.Jobs:GetPlayerJobID(LocalPlayer()) ~= "JOB_CIVILIAN" then
		local job = GAMEMODE.Jobs:GetPlayerJob(LocalPlayer())

		for k, v in pairs( GAMEMODE.Cars:GetActiveJobCars( job.Enum ) ) do
			self:AddWaypoint("jobcar"..v:EntIndex(), "map_copcar", "Colleague", job.TeamColor, 1, v:GetPos(), Angle(0,0,0))
		end
	end

	for k, v in pairs(self.Signs) do
		self:AddWaypoint("sign"..v:EntIndex(), "map_sign", v:GetDisplayName(), color_white, 1.5, v:GetPos())
	end

end

/*
	Build Minimap
*/
function Panel:BuildMinimap()

	self.Options = vgui.Create("DPanel", self)
	self.Options.Paint = function(s,w,h)
		surface.SetDrawColor( 40, 40, 40, 200 )
		surface.DrawRect( 0, 0, w, h )
	end

	self.viewPlayer = vgui.Create( "DCheckBoxLabel", self.Options )
	self.viewPlayer:SetConVar( "srp_map_player" )
	self.viewPlayer:SetText( "Show your player on map" )

	self.viewPoints = vgui.Create( "DCheckBoxLabel", self.Options )
	self.viewPoints:SetConVar( "srp_map_points" )
	self.viewPoints:SetText( "Show waypoints on map" )

	self.viewSigns = vgui.Create( "DCheckBoxLabel", self.Options )
	self.viewSigns:SetConVar( "srp_map_signs" )
	self.viewSigns:SetText( "Show signs on map" )

	self.Container = vgui.Create("DPanel", self)

	self.Image = vgui.Create("DImage", self.Container) 
	self.Image:Dock(FILL)
	self.Image:SetMaterial( GAMEMODE.Images.Mat["map_overhead"] )

	self.Overlay = vgui.Create("DPanel", self.Image)
	self.Overlay:Dock(FILL)
	self.Overlay.Paint = function(s, w, h)

		local adjustH = self.Image:GetTall()
		local adjustW = self.Image:GetWide()

		self:GetWaypoints()

		for k, v in pairs(self.Waypoints) do

			if( k == "localplayer" ) then
				if !GAMEMODE.Config.MapPlayer:GetBool() then continue end
			else
				if( string.sub(k,1,4) == "sign" ) then
					if !GAMEMODE.Config.MapSigns:GetBool() then continue end
				else
					if !GAMEMODE.Config.MapPoints:GetBool() then continue end
				end
			end

			surface.SetDrawColor(v.color)
		    surface.SetMaterial( GAMEMODE.Images.Mat[ v.image ] )

		    local scale = 20*v.scale
		    surface.DrawTexturedRectRotated(v.pos.x*adjustW, v.pos.y*adjustH, scale, scale, v.ang)
			draw.SimpleTextOutlined(v.name, "MapFont", v.pos.x*adjustW, v.pos.y*adjustH+(5*v.scale), v.color, TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP, 1, color_black)
		end
	end

end

/*
	Panel Structure
*/
function Panel:Init()
	self:AddWaypoint("localplayer", "map_player", "Me", color_white, 1, Vector(0, 0, 0), Angle(0, 0, 0))

	for k, v in pairs( GAMEMODE.Config.Waypoints ) do
		self:AddWaypoint( v[1], v[2], v[3], v[4], v[5], v[6] )
	end

	self:BuildMinimap()
end

function Panel:Refresh()
	self:InvalidateLayout()
end

function Panel:PerformLayout( intW, intH )

	local xMove = (intW - intH)

	self.Options:SetSize( xMove, intH )
	self.Options:SetPos( 0, 0 )

	self.viewPlayer:SetTall( 32 )
	self.viewPlayer:DockMargin( 5, 0, 5, 5 )
	self.viewPlayer:Dock( TOP )

	self.viewPoints:SetTall( 32 )
	self.viewPoints:DockMargin( 5, 0, 5, 5 )
	self.viewPoints:Dock( TOP )

	self.viewSigns:SetTall( 32 )
	self.viewSigns:DockMargin( 5, 0, 5, 5 )
	self.viewSigns:Dock( TOP )

	self.Container:SetSize( intH, intH )
	self.Container:SetPos( xMove, 0 )

end

vgui.Register( "SRPQMenu_Map", Panel, "EditablePanel" )