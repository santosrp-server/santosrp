--[[
	Name: cl_qmenu_tab_dev.lua
	For: TalosLife
	By: TalosLife
]]--

local Panel = {}
function Panel:Init()

	self.m_pnlNameLabel = vgui.Create( "DLabel", self )
	self.m_pnlNameLabel:SetExpensiveShadow( 2, Color(0, 0, 0, 255) )
	self.m_pnlNameLabel:SetTextColor( color_white )
	self.m_pnlNameLabel:SetFont( "ItemCardFont2" )

	self.m_pnlRunButton = vgui.Create( "SRP_Button", self )
	self.m_pnlRunButton:SetText("Run")
	self.m_pnlRunButton.DoClick = function()
		
		if self.str then

			GAMEMODE.Gui:StringRequest(
				"Dev String Input",
				self.str, 
				"AK-47",
				function( strText )
					
					if self.two then

						GAMEMODE.Gui:StringRequest(
							"Dev String Input",
							self.two, 
							"1",
							function( txt )
								RunConsoleCommand( self.command , strText, txt )
							end,
							function() end,
							"Submit",
							"Cancel"
						)
			
					else
						RunConsoleCommand( self.command, strText )
					end					

				end,
				function() end,
				"Submit",
				"Cancel"
			)

		else
			RunConsoleCommand( self.command )
		end

	end

end

function Panel:SetCommand( command )
	self.command = command
end

function Panel:SetDisplayName( name )
	self.name = name
	self.m_pnlNameLabel:SetText( name )
end

function Panel:SetDisplayStr( str )
	self.str = str
end

function Panel:SetDisplayTwo( two )
	self.two = two
end

function Panel:Paint( intW, intH )
	surface.SetDrawColor( 50, 50, 50, 200 )
	surface.DrawRect( 0, 0, intW, intH )
end

function Panel:PerformLayout( intW, intH )

	local padding = 5

	self.m_pnlNameLabel:SizeToContents()
	self.m_pnlNameLabel:SetPos( (padding *2) +intH, (intH /2) -(self.m_pnlNameLabel:GetTall() /2) )

	self.m_pnlRunButton:SetPos( intW - 100, 0 )
	self.m_pnlRunButton:SetSize( 100, intH )

end
vgui.Register( "SRPQMenuDevCard", Panel, "EditablePanel" )

--[[
	Scroll Panel
]]

local Panel = {}
function Panel:Init()

	self.tblCommands = {}

	self.m_pnlCommandList = vgui.Create( "SRP_ScrollPanel", self )

end

function Panel:CreateDevCard( cmd, display, input, two )

	local card = vgui.Create("SRPQMenuDevCard", self)
	card:SetCommand( cmd )
	card:SetDisplayName( display )

	if input then
		card:SetDisplayStr( input )
	else
		card:SetDisplayStr( false )
	end

	if two then
		card:SetDisplayTwo( two )
	else
		card:SetDisplayTwo( false )
	end	
	
	table.insert( self.tblCommands, card )

end

function Panel:Refresh()

	for k, v in pairs( self.tblCommands ) do
		if IsValid( v ) then v:Remove() end
	end	

	self.tblCommands = {}

	// Format: cmd, display name, show string input, show int input
	self:CreateDevCard( "srp_dev_heal_me", "Heal Self", false, false )
	self:CreateDevCard( "srp_dev_unrag", "Revive Self", false, false )

	self:CreateDevCard( "srp_dev_remove", "Remove Entity", false, false )

	self:CreateDevCard( "srp_dev_give_money", "Give Money", "Enter Amount ($)", false )
	self:CreateDevCard( "srp_dev_give_item", "Give Item", "Enter Item Name", "Enter Amount (#)" )

	self:CreateDevCard( "srp_dev_give_xp", "Give XP", "Enter Skill Name", "Enter Desired XP (#)" )
	self:CreateDevCard( "srp_dev_set_level", "Set Level", "Enter Skill Name", "Enter Desired Level (#)" )

	self:CreateDevCard( "srp_dev_givelicense", "Give Driving License", false, false )
	self:CreateDevCard( "srp_dev_removelicense", "Remove Driving License", false, false )

	self:InvalidateLayout()

end

function Panel:PerformLayout( intW, intH )

	self.m_pnlCommandList:SetPos(0,0)
	self.m_pnlCommandList:SetSize( intW, intH )

	for _, pnl in pairs(self.tblCommands) do
		pnl:DockMargin( 0, 0, 0, 5 )
		pnl:SetTall( 64 )
		pnl:Dock( TOP )
	end

end
vgui.Register( "SRPQMenu_Dev", Panel, "EditablePanel" )