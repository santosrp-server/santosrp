--[[
	Name: sv_business.lua
	For: SantosRP.Life
	By: Jake Butts
]]--

GM.Business = (GAMEMODE or GM).Business or {}
GM.Business.Businesses = (GAMEMODE or GM).Business.Businesses or {}
GM.Business.Prices = (GAMEMODE or GM).Business.Prices or {}

--[[ Business Management ]]--
function GM.Business:Load()
	self:LoadBusinesses()
	self:LoadPrices()
end

function GM.Business:LoadBusinesses()
	GM:PrintDebug( 0, "->LOADING BUSINESSES" )

	local foundFiles, foundFolders = file.Find( GM.Config.GAMEMODE_PATH.. "business/*.lua", "LUA" )
	GM:PrintDebug( 0, "\tFound ".. #foundFiles.. " files." )

	for k, v in pairs( foundFiles ) do
		GM:PrintDebug( 0, "\tLoading ".. v )
		include( GM.Config.GAMEMODE_PATH.. "business/".. v )
		AddCSLuaFile( GM.Config.GAMEMODE_PATH.. "business/".. v )
	end

	GM:PrintDebug( 0, "->BUSINESSES LOADED" )

end

function GM.Business:RegisterBusiness( tblItem )
	self.Businesses[tblItem.Name] = tblItem
	GM:PrintDebug( 0, "\t\tRegistered item ".. tblItem.Name )
end

function GM.Business:GetBusiness( strBusiness )
	return self.Businesses[strBusiness]
end

function GM.Business:GetBusinessValue( pPlayer, strBusiness )
	local pData = self.Players[pPlayer:SteamID64()]
	local bData = pData[strBusiness]
	return bData.Value
end

function GM.Business:LoadPrices()
	for k, v in pairs( self.Businesses ) do
		self.Prices[v.Name] = v.StartBuyPrice
	end
end

--[[ Player Management ]] --
function GM.Business:InitialSpawn( pPlayer )
	self:UpdatePlayer( pPlayer )
end

hook.Add( "GamemodePlayerSelectCharacter", "SendBusinessPlayerData", function( pPlayer )
	GAMEMODE.Business:UpdatePlayer( pPlayer )
end )

function GM.Business:PlayerOwnsBusiness( pPlayer, strBusiness )
	
	local saveTable = GAMEMODE.Char:GetCurrentSaveTable( pPlayer )
	if not saveTable then return false end

	saveTable.Business = saveTable.Business or {}

	if not saveTable.Business[strBusiness] then return false end

	return true
end

function GM.Business:UpdatePlayer( pPlayer )

	local saveTable = GAMEMODE.Char:GetCurrentSaveTable( pPlayer )
	if not saveTable then return false end

	saveTable.Business = saveTable.Business or {}

	GAMEMODE.Net:SendBusinessData( pPlayer, saveTable.Business )
	GAMEMODE.Net:SendBusinessPriceData( pPlayer, self.Prices )

end

function GM.Business:GetPlayerBusiness( pPlayer, strBusiness )

	local saveTable = GAMEMODE.Char:GetCurrentSaveTable( pPlayer )
	if not saveTable then return false end

	saveTable.Business = saveTable.Business or {}

	return saveTable.Business[strBusiness]
end

function GM.Business:NewBusiness( pPlayer, strBusiness )

	local saveTable = GAMEMODE.Char:GetCurrentSaveTable( pPlayer )
	if not saveTable then return false end

	saveTable.Business = saveTable.Business or {}

	local business = {
		["HasActiveMission"] = false,
		["Supplies"] = 0,
		["Value"] = 0,
		["Employees"] = {},
	}

	saveTable.Business[strBusiness] = business

	GAMEMODE.SQL:MarkDiffDirty( pPlayer, "data_store", "Business" )

	pPlayer:AddNote("You have purchased " .. strBusiness)
	self:UpdatePlayer( pPlayer )

end

function GM.Business:PlayerBuyBusiness( pPlayer, strBusiness )
	local business = self:GetBusiness( strBusiness )
	if !business then return end

	local price = business.Price

	if !(pPlayer:CanAfford( price )) then pPlayer:AddNote("You cannot afford that!") return end
	if self:PlayerOwnsBusiness( pPlayer, strBusiness ) then pPlayer:AddNote("You already own this business!") return end

	pPlayer:AddMoney( -price )

	self:NewBusiness( pPlayer, strBusiness )
end

function GM.Business:GetBusinessSupplies( pPlayer, strBusiness )
	local saveTable = GAMEMODE.Char:GetCurrentSaveTable( pPlayer )
	if not saveTable then return false end
	return saveTable.Business[strBusiness].Supplies
end

function GM.Business:SetBusinessSupplies( pPlayer, strBusiness, intSupply )
	local saveTable = GAMEMODE.Char:GetCurrentSaveTable( pPlayer )
	if not saveTable then return false end
	saveTable.Business[strBusiness].Supplies = intSupply
	GAMEMODE.SQL:MarkDiffDirty( pPlayer, "data_store", "Business" )
end

function GM.Business:GetBusinessValue( pPlayer, strBusiness )
	local saveTable = GAMEMODE.Char:GetCurrentSaveTable( pPlayer )
	if not saveTable then return false end
	return saveTable.Business[strBusiness].Value
end

function GM.Business:SetBusinessValue( pPlayer, strBusiness, intValue )
	local saveTable = GAMEMODE.Char:GetCurrentSaveTable( pPlayer )
	if not saveTable then return false end
	saveTable.Business[strBusiness].Value = intValue
	GAMEMODE.SQL:MarkDiffDirty( pPlayer, "data_store", "Business" )
end

--[[ Job Management ]]--
function GM.Business:PlayerMissionUpdate( pPlayer, strBusiness, boolMissionActive )
	local saveTable = GAMEMODE.Char:GetCurrentSaveTable( pPlayer )
	if not saveTable then return false end
	saveTable.Business[strBusiness].HasActiveMission = boolMissionActive
end

function GM.Business:PlayerCollectedSupplies( pPlayer, strBusiness, intAmount )

	local business = self:GetBusiness( strBusiness )
	if !business then return end
	if !self:PlayerOwnsBusiness( pPlayer, strBusiness ) then return end

	for i=1, intAmount do
		local supplyAmount = self:GetBusinessSupplies( pPlayer, strBusiness )
		local toAdd = math.Clamp( supplyAmount + 10, 0, 100 )
		self:SetBusinessSupplies( pPlayer, strBusiness, toAdd )
	end

	self:PlayerMissionUpdate( pPlayer, strBusiness, false )
	self:UpdatePlayer( pPlayer )
end

function GM.Business:StoreSupplies( pPlayer, strBusiness )

	local business = self:GetBusiness( strBusiness )
	if !business then return end

	if !self:PlayerOwnsBusiness( pPlayer, strBusiness ) then
		pPlayer:AddNote( "You do not own that business!" )
		return
	end

	if GAMEMODE.Inv:GetPlayerItemAmount( pPlayer, "Business Supplies" ) == 0 then
		pPlayer:AddNote( "You do not have any Business Supplies!" )
		return
	end

	local supAmount = GAMEMODE.Inv:GetPlayerItemAmount( pPlayer, "Business Supplies" )

	GAMEMODE.Inv:TakePlayerItem( pPlayer, "Business Supplies", supAmount )

	pPlayer:AddNote( "You have stored "..supAmount.. " supplies" )

	self:PlayerCollectedSupplies( pPlayer, strBusiness, supAmount )	

end

function GM.Business:SellSupplies( pPlayer, strBusiness, strSupplyType )

	local business = self:GetBusiness( strBusiness )
	if !business then return end

	if !self:PlayerOwnsBusiness( pPlayer, strBusiness ) then
		pPlayer:AddNote( "You do not own that business!" )
		return
	end

	local value = math.random( business.MinSupplyValue, business.MaxSupplyValue )

	local supAmount = GAMEMODE.Inv:GetPlayerItemAmount( pPlayer, strSupplyType )
	GAMEMODE.Inv:TakePlayerItem( pPlayer, strSupplyType, supAmount )

	local price = value * supAmount

	pPlayer:AddNote( "You have sold "..supAmount.. " supplies for $"..price.."!" )

	local bVal = self:GetBusinessValue( pPlayer, strBusiness )
	local newVal = bVal + price

	self:SetBusinessValue( pPlayer, strBusiness, newVal )

	for i=1, supAmount do
		local supplyAmount = self:GetBusinessSupplies( pPlayer, strBusiness )
		local toAdd = math.Clamp( supplyAmount - 10, 0, 100 )
		self:SetBusinessSupplies( pPlayer, strBusiness, toAdd )
	end

	self:PlayerMissionUpdate( pPlayer, strBusiness, false )
	self:UpdatePlayer( pPlayer )

end

function GM.Business:PlayerRequestJob( pPlayer, strBusiness, strJobType )

	local business = self:GetBusiness( strBusiness )
	if !business then return end
	if !self:PlayerOwnsBusiness( pPlayer, strBusiness ) then return end

	local bData = self:GetPlayerBusiness( pPlayer, strBusiness )

	local job = business:RequestJob( pPlayer, bData, strJobType )

	if job then
		self:PlayerMissionUpdate( pPlayer, strBusiness, true )
	end

end