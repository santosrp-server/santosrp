--[[
	Name: drug_buyer.lua
	For: TalosLife
	By: TalosLife
]]--

local NPCMeta = {}
NPCMeta.Name = "Black Market"
NPCMeta.UID = "drug_buyer"
NPCMeta.SubText = "Buy and sell drugs here"
NPCMeta.Model = "models/Humans/Group02/Male_04.mdl"
NPCMeta.RandomSpots = GM.Config.DrugNPCPositions
NPCMeta.MinMoveTime = GM.Config.DrugNPCMoveTime_Min
NPCMeta.MaxMoveTime = GM.Config.DrugNPCMoveTime_Max
NPCMeta.NoSalesTax = true
NPCMeta.Sounds = {
	StartDialog = {
		"vo/npc/male01/question05.wav",
		"vo/npc/male01/question07.wav",
		"vo/npc/male01/question17.wav",
		"vo/npc/male01/question30.wav",
	},
	EndDialog = {
		"vo/npc/male01/littlecorner01.wav",
		"vo/npc/male01/question02.wav",
		"vo/npc/male01/question28.wav",
	}
}
--[itemID] = priceToBuy,
NPCMeta.ItemsForSale = {
	["Cannabis Seeds (Low Quality)"] = 12,
	["Cannabis Seeds (Medium Quality)"] = 15,
	["Cannabis Seeds (High Quality)"] = 20,
	--["Cannabis (Low Quality)"] = 10,
	["Phenylacetic Acid"] = 8,
	--["Methamphetamine (Low Quality)"] = 22,
	["Coca Leaves"] = 15,
	["C4 Casing"] = 4000,
	["Specialized Rubber"] = 300,
	["Printing Press"] = 2000,
}
--[itemID] = priceToSell,
NPCMeta.ItemsCanBuy = {
	["Cannabis (Low Quality)"] = 30,
	["Cannabis (Medium Quality)"] = 45,
	["Cannabis (High Quality)"] = 80,
	["Moonshine"] = 70,
	["Methamphetamine (Low Quality)"] = 75,
	["Methamphetamine (Medium Quality)"] = 400,
	["Methamphetamine (High Quality)"] = 500,
	["Cocaine (Low Quality)"] = 50,
	["Cocaine (Medium Quality)"] = 300,
	["Cocaine (High Quality)"] = 350,
	["Crack Cocaine"] = 450,
	["Money Bag"] = 7000,
	["Counterfeit Money"] = 200,
}

function NPCMeta:OnPlayerTalk( entNPC, pPlayer )
	GAMEMODE.Net:ShowNPCDialog( pPlayer, "drug_buyer" )

	if (entNPC.m_intLastSoundTime or 0) < CurTime() then
		local snd, _ = table.Random( self.Sounds.StartDialog )
		entNPC:EmitSound( snd, 54 )
		entNPC.m_intLastSoundTime = CurTime() +2
	end
end

function NPCMeta:OnPlayerEndDialog( pPlayer )
	if not pPlayer:WithinTalkingRange() then return end
	if pPlayer:GetTalkingNPC().UID ~= self.UID then return end

	if (pPlayer.m_entTalkingNPC.m_intLastSoundTime or 0) < CurTime() then
		local snd, _ = table.Random( self.Sounds.EndDialog )
		pPlayer.m_entTalkingNPC:EmitSound( snd, 54 )
		pPlayer.m_entTalkingNPC.m_intLastSoundTime = CurTime() +2
	end

	pPlayer.m_entTalkingNPC = nil
end

if SERVER then
	--RegisterDialogEvents is called when the npc is registered! This is before the gamemode loads so GAMEMODE is not valid yet.
	function NPCMeta:RegisterDialogEvents()
	end

	function NPCMeta:OnSpawn( entNPC )
		local moveNPC
		moveNPC = function()
			timer.Simple( math.random(self.MinMoveTime, self.MaxMoveTime), function()
				if not IsValid( entNPC ) then return end

				local pos = table.Random( self.RandomSpots )
				entNPC:SetPos( pos[1] )
				entNPC:SetAngles( pos[2] )
				moveNPC()
			end )
		end

		moveNPC()
	end
elseif CLIENT then
	NPCMeta.RandomGreetings = {
		"I've got the best stuff around!",
		"You selling? I'm looking to re-up.",
		"Always need to keep moving. The police never let up.",
	}

	function NPCMeta:RegisterDialogEvents()
		GM.Dialog:RegisterDialog( "drug_buyer", self.StartDialog, self )
	end
	
	function NPCMeta:StartDialog()
		GAMEMODE.Dialog:ShowDialog()
		GAMEMODE.Dialog:SetModel( self.Model )
		GAMEMODE.Dialog:SetTitle( self.Name )
		GAMEMODE.Dialog:SetPrompt( table.Random(self.RandomGreetings) )

		GAMEMODE.Dialog:AddOption( "Show me what you have for sale.", function()
			GAMEMODE.Gui:ShowNPCShopMenu( self.UID )
			GAMEMODE.Dialog:HideDialog()
		end )
		GAMEMODE.Dialog:AddOption( "I've got items I'd like to sell.", function()
			GAMEMODE.Gui:ShowNPCSellMenu( self.UID )
			GAMEMODE.Dialog:HideDialog()
		end )
		GAMEMODE.Dialog:AddOption( "Never mind, I have to go.", function()
			GAMEMODE.Net:SendNPCDialogEvent( self.UID.. "_end_dialog" )
			GAMEMODE.Dialog:HideDialog()
		end )
	end
end

GM.NPC:Register( NPCMeta )