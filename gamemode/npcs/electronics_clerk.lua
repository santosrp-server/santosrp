--[[
	Name: electronics_clerk.lua
	For: TalosLife
	By: TalosLife
]]--

local NPCMeta = {}
NPCMeta.Name = "Store Clerk"
NPCMeta.UID = "electronics_clerk"
NPCMeta.SubText = "Purchase new electronics here"
NPCMeta.Model = "models/Humans/Group02/male_06.mdl"
NPCMeta.Sounds = {
	StartDialog = {
		"vo/npc/male01/answer30.wav",
		"vo/npc/male01/gordead_ans01.wav",
		"vo/npc/male01/gordead_ques16.wav",
		"vo/npc/male01/hi01.wav",
		"vo/npc/male01/hi02.wav",
	},
	EndDialog = {
		"vo/npc/male01/finally.wav",
		"vo/npc/male01/pardonme01.wav",
		"vo/npc/male01/vanswer01.wav",
		"vo/npc/male01/vanswer13.wav",
	}
}
--[itemID] = priceToBuy,
NPCMeta.ItemsForSale = {
	["Radio"] = 100,
	["Large Sign"] = 100,
	["Box Fan"] = 75,
	["Large Lamp"] = 115,
	["Cash Register"] = 150,
    //["Civ Radio"] = 30,
    ["Billboard"] = 250,
    ["CCTV Camera"] = 300,
	["Ink Vial"] = 25,
    //["Card Reader"] = 300,
    //["Stational Radio Civ"] = 300,
}
--[itemID] = priceToSell,
NPCMeta.ItemsCanBuy = {}
for k, v in pairs( NPCMeta.ItemsForSale ) do
	NPCMeta.ItemsCanBuy[k] = math.ceil( v *0.66 )
end

NPCMeta.HandsUpBones = {
	
	["ValveBiped.Bip01_R_UpperArm"] = Angle(73,35,128),
	["ValveBiped.Bip01_L_Hand"] = Angle(-12,12,90),
	["ValveBiped.Bip01_L_Forearm"] = Angle(-28,-29,44),
	["ValveBiped.Bip01_R_Forearm"] = Angle(-22,1,15),
	["ValveBiped.Bip01_L_UpperArm"] = Angle(-77,-46,4),
	["ValveBiped.Bip01_R_Hand"] = Angle(33,39,-21),
	["ValveBiped.Bip01_L_Finger01"] = Angle(0,30,0),
	["ValveBiped.Bip01_L_Finger1"] = Angle(0,45,0),
	["ValveBiped.Bip01_L_Finger11"] = Angle(0,45,0),
	["ValveBiped.Bip01_L_Finger2"] = Angle(0,45,0),
	["ValveBiped.Bip01_L_Finger21"] = Angle(0,45,0),
	["ValveBiped.Bip01_L_Finger3"] = Angle(0,45,0),
	["ValveBiped.Bip01_L_Finger31"] = Angle(0,45,0),
	["ValveBiped.Bip01_R_Finger0"] = Angle(-10,0,0),
	["ValveBiped.Bip01_R_Finger11"] = Angle(0,30,0),
	["ValveBiped.Bip01_R_Finger2"] = Angle(20,25,0),
	["ValveBiped.Bip01_R_Finger21"] = Angle(0,45,0),
	["ValveBiped.Bip01_R_Finger3"] = Angle(20,35,0),
	["ValveBiped.Bip01_R_Finger31"] = Angle(0,45,0),	

}

function NPCMeta:AttemptRobbery( pPlayer, ... )

	if !IsValid( pPlayer ) then return end

	local entNPC = pPlayer.m_entTalkingNPC
	if !IsValid( entNPC ) then return end

	if !pPlayer:GetActiveWeapon() then
		GAMEMODE.Net:ShowNPCDialog( pPlayer, "deny_robbery_elec" )
		return
	end

	if !table.HasValue( GAMEMODE.Config.RobberyWeapons , pPlayer:GetActiveWeapon():GetClass() ) then
		GAMEMODE.Net:ShowNPCDialog( pPlayer, "deny_robbery_elec" )
		return
	end

	if GAMEMODE.Jobs:GetNumPlayers( "JOB_POLICE" ) < 2 then
		GAMEMODE.Net:ShowNPCDialog( pPlayer, "deny_robbery_elec" )
		return
	end

	if !entNPC.LastRobbery then entNPC.LastRobbery = -GAMEMODE.Config.RobberyDelay end

	if CurTime() < entNPC.LastRobbery + GAMEMODE.Config.RobberyDelay then
		GAMEMODE.Net:ShowNPCDialog( pPlayer, "deny_robbery_elec" )
		return
	end	

	self:InitiateRobbery( pPlayer , entNPC )

end

function NPCMeta:InitiateRobbery( pPlayer, entNPC )

	local snd = "vo/npc/female01/help01.wav"
	entNPC:EmitSound( snd, 60 )

	entNPC.IsBeingRobbed = true
	self:ToggleHandsUp( entNPC )

	strText = "The local electronics store is requesting assistance!"
	for k, v in pairs( player.GetAll() ) do
		if not GAMEMODE.Jobs:GetPlayerJob( v ) then continue end
		if GAMEMODE.Jobs:GetPlayerJob( v ).Receives911Messages then
			GAMEMODE.Net:SendTextMessage( v, "Dispatch", strText )
			v:EmitSound( "santosrp/sms.mp3" )
		end
	end

	entNPC.alarmSound = CreateSound(entNPC, Sound("ambient/alarms/alarm1.wav"))
	entNPC.alarmSound:SetSoundLevel( 80 )
	entNPC.alarmSound:Play()

	local robberyTime = math.random( GAMEMODE.Config.RobberyMinTime , GAMEMODE.Config.RobberyMaxTime )

	timer.Create( "NPCRobbery"..self.UID, robberyTime, 1, function()

		entNPC.alarmSound:Stop()

		local snd = "vo/npc/male01/no01.wav"
		entNPC:EmitSound( snd, 60 )

		self:DropMoneyBag( entNPC )
		entNPC.IsBeingRobbed = false
		self:ToggleHandsUp( entNPC )
		entNPC.LastRobbery = CurTime()

	end)

end

function NPCMeta:DropMoneyBag( entNPC )

	local pos = self:GetClosestDrop( entNPC )

	local money = ents.Create( "ent_moneybag" )
	money:SetPos(  pos  )
	money:SetModel("models/freeman/duffel_bag.mdl")
	money.IsItem = true
	money.ItemID = "Money Bag"
	money.ItemData = GAMEMODE.Inv:GetItem("Money Bag")
	money:Spawn()

end

function NPCMeta:ToggleHandsUp( entNPC )

	for k,v in pairs( self.HandsUpBones ) do
		local Bone = entNPC:LookupBone(k)
		if Bone then
			if !entNPC.IsBeingRobbed then
				entNPC:ManipulateBoneAngles(Bone, Angle(0,0,0))
			else
				entNPC:ManipulateBoneAngles(Bone, v)	
			end
		end
	end

end

function NPCMeta:GetClosestDrop( entNPC )

	local chosenPos = GAMEMODE.Config.RobberyMoneyDrops[1]
	local entPos = entNPC:GetPos()

	for k, v in pairs( GAMEMODE.Config.RobberyMoneyDrops ) do
		if entPos:Distance( v ) < entPos:Distance( chosenPos ) then
			chosenPos = v
		end
	end

	return chosenPos

end

function NPCMeta:OnPlayerTalk( entNPC, pPlayer )
	if !entNPC.IsBeingRobbed then
		GAMEMODE.Net:ShowNPCDialog( pPlayer, "electronics_clerk" )
	else
		GAMEMODE.Net:ShowNPCDialog( pPlayer, "being_robbed_elec" )
	end

	if (entNPC.m_intLastSoundTime or 0) < CurTime() then
		local snd, _ = table.Random( self.Sounds.StartDialog )
		entNPC:EmitSound( snd, 60 )
		entNPC.m_intLastSoundTime = CurTime() +2
	end
end

function NPCMeta:OnPlayerEndDialog( pPlayer )
	if not pPlayer:WithinTalkingRange() then return end
	if pPlayer:GetTalkingNPC().UID ~= self.UID then return end

	if (pPlayer.m_entTalkingNPC.m_intLastSoundTime or 0) < CurTime() then
		local snd, _ = table.Random( self.Sounds.EndDialog )
		pPlayer.m_entTalkingNPC:EmitSound( snd, 60 )
		pPlayer.m_entTalkingNPC.m_intLastSoundTime = CurTime() +2
	end

	pPlayer.m_entTalkingNPC = nil
end

function NPCMeta:AttemptBusinessSell( pPlayer )
	local reqSupply = "Electric Supplies"
	if GAMEMODE.Inv:GetPlayerItemAmount( pPlayer, reqSupply ) == 0 then
		pPlayer:AddNote( "You do not have any "..reqSupply.."!" )
		return
	end
	GAMEMODE.Business:SellSupplies( pPlayer, "Haulage Inc.", reqSupply )
end

if SERVER then
	--RegisterDialogEvents is called when the npc is registered! This is before the gamemode loads so GAMEMODE is not valid yet.
	function NPCMeta:RegisterDialogEvents()
		GM.Dialog:RegisterDialogEvent( "robbery_attempt_elec", self.AttemptRobbery, self )
		GM.Dialog:RegisterDialogEvent( "business_sell_elec", self.AttemptBusinessSell, self )
	end
elseif CLIENT then
	NPCMeta.RandomGreetings = {
		"You should check out our new TVs!",
		"Can I help you with anything today?",
		"We sell some of the best radios around!",
	}

	function NPCMeta:RegisterDialogEvents()
		GM.Dialog:RegisterDialog( "electronics_clerk", self.StartDialog, self )
		GM.Dialog:RegisterDialog( "being_robbed_elec", self.RobberyDialog, self )
		GM.Dialog:RegisterDialog( "deny_robbery_elec", self.DenyRobberyAttempt, self )
	end
	
	function NPCMeta:StartDialog()
		GAMEMODE.Dialog:ShowDialog()
		GAMEMODE.Dialog:SetModel( self.Model )
		GAMEMODE.Dialog:SetTitle( self.Name )
		GAMEMODE.Dialog:SetPrompt( table.Random(self.RandomGreetings) )

		GAMEMODE.Dialog:AddOption( "Show me what you have for sale.", function()
			GAMEMODE.Gui:ShowNPCShopMenu( self.UID )
			GAMEMODE.Dialog:HideDialog()
		end )
		GAMEMODE.Dialog:AddOption( "I would like to return some items.", function()
			GAMEMODE.Gui:ShowNPCSellMenu( self.UID )
			GAMEMODE.Dialog:HideDialog()
		end )
		GAMEMODE.Dialog:AddOption( "I have some supplies to sell.", function()
			GAMEMODE.Net:SendNPCDialogEvent( "business_sell_elec" )
			GAMEMODE.Dialog:HideDialog()
		end )	
		GAMEMODE.Dialog:AddOption( "Hands up, this is a robbery.", function()
			GAMEMODE.Net:SendNPCDialogEvent( "robbery_attempt_elec" )
			GAMEMODE.Dialog:HideDialog()
		end )		
		GAMEMODE.Dialog:AddOption( "Never mind, I have to go.", function()
			GAMEMODE.Net:SendNPCDialogEvent( self.UID.. "_end_dialog" )
			GAMEMODE.Dialog:HideDialog()
		end )
	end
	
	function NPCMeta:RobberyDialog()
		GAMEMODE.Dialog:ShowDialog()
		GAMEMODE.Dialog:SetModel( self.Model )
		GAMEMODE.Dialog:SetTitle( self.Name )
		GAMEMODE.Dialog:SetPrompt( "Not now! I'm being robbed!" )

		GAMEMODE.Dialog:AddOption( "I'll be back.", function()
			GAMEMODE.Dialog:HideDialog()
		end )
	end

	function NPCMeta:DenyRobberyAttempt()
		GAMEMODE.Dialog:ShowDialog()
		GAMEMODE.Dialog:SetModel( self.Model )
		GAMEMODE.Dialog:SetTitle( self.Name )
		GAMEMODE.Dialog:SetPrompt( "Don't make me laugh idiot, get out of my store!" )

		GAMEMODE.Dialog:AddOption( "I'll be back.", function()
			GAMEMODE.Dialog:HideDialog()
		end )
	end
end

GM.NPC:Register( NPCMeta )