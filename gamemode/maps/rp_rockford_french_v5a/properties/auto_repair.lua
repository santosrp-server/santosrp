local Prop = {}
Prop.Name = "Auto Repair"
Prop.Government = true
Prop.Doors = {
	Vector( -7111.921387, 862.413635, 65.031242 ),
	Vector( -7110.922363, 813.772156, 65.031242 ),
	Vector( -7479.551758, 942.256775, 65.031242 ),
	Vector( -7500.965332, 823.490967, 65.031242 ),
	Vector( -7708.721680, 822.625427, 65.031242 ),
}

GM.Property:Register( Prop )