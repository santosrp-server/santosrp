local Prop = {}
Prop.Name = "McDonalds"
Prop.Cat = "Stores"
Prop.Price = 800
Prop.Doors = {
	Vector( 634.838013, 2224.727783, 601.031250 ),
	Vector( 599.721313, 2225.114502, 601.031250 ),
	Vector( 752.967468, 1687.980103, 601.031250 ),
	Vector( 753.862488, 1647.293701, 601.031250 ),
	Vector( 638.106567, 1584.030029, 601.031250 ),
	Vector( 524.031250, 1362.817383, 607.287231 ),

}

GM.Property:Register( Prop )