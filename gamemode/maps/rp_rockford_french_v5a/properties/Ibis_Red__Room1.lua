local Prop = {}
Prop.Name = "Ibis Red | Room 1"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
    Vector(-1346.812866, -5479.122559, 279.031250),
    Vector(-1627.778076, -5764.380371, 279.423279),
    Vector(-1696.053345, -5259.702148, 279.031250),
    Vector(-1795.095947, -5320.979980, 279.031250),
    Vector(-1702.479980, -5387.208008, 279.031250),
}

GM.Property:Register( Prop )