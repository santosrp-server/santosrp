local Prop = {}
Prop.Name = "Ibis Red | Room 5"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
    Vector(-1083.944336, -5487.851563, 415.031250),
    Vector(-732.553528, -5261.256836, 415.031250),
    Vector(-730.230103, -5387.644043, 415.031250),
    Vector(-636.390747, -5327.382324, 415.031250),
}

GM.Property:Register( Prop )