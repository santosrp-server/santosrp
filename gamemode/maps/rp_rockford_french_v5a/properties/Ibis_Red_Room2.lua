local Prop = {}
Prop.Name = "Ibis Red | Room 2"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
    Vector(-1084.691650, -5490.075195, 279.031250),
    Vector(-736.963440, -5260.145996, 279.031250),
    Vector(-636.353577, -5329.880859, 279.031250),
    Vector(-735.799011, -5388.508789, 279.031250),
    Vector(-807.020752, -5764.038086, 279.423279),
}

GM.Property:Register( Prop )