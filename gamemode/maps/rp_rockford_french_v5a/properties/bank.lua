local Prop = {}
Prop.Name = "Bank"
Prop.Government = true
Prop.Doors = {
	{ Pos = Vector( -5426.891113, -5204.402344, 72.031258 ), },
	{ Pos = Vector( -5381.829102, -5204.445801, 72.031258 ), },
	{ Pos = Vector( -5225.197754, -5845.977051, 72.031258 ), Locked = true,},
	{ Pos = Vector( -5059.958496, -5547.506348, 72.031258 ), },
	{ Pos = Vector( -5044.495605, -5204.821289, 72.031258 ), },
	{ Pos = Vector( -5223.104492, -6170.046387, 72.031250 ), },
	{ Pos = Vector( -5660.295898, -5845.994629, 72.031250 ), },
	{ Pos = Vector( -5767.600098, -5834.015625, 72.031250 ), },
	{ Pos = Vector( -5767.535645, -6165.956543, 72.031250 ), },
	{ Pos = Vector( -5658.831055, -6168.985352, 72.031250 ), },
}

GM.Property:Register( Prop )