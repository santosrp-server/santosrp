local Prop = {}
Prop.Name = "Dark Slum | Room 2"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
	Vector( -1863.377930, -3354.765381, 185.031250 ),
	Vector( -1938.028809, -3050.661133, 185.031250 ),
	Vector( -1866.863159, -2961.649658, 185.031250 ),
}

GM.Property:Register( Prop )