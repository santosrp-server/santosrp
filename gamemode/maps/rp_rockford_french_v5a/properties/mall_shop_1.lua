local Prop = {}
Prop.Name = "Mall Shop 1"
Prop.Cat = "Stores"
Prop.Price = 800
Prop.Doors = {
	Vector( -2299.533203, 1225.208130, 600.035156 ),
	Vector( -2347.398682, 1224.828857, 600.035156 ),
}

GM.Property:Register( Prop )