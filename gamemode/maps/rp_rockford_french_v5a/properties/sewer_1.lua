local Prop = {}
Prop.Name = "Sewer Room 1"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
	Vector( -4593.268555, -5731.909180, -223.968750 ),
	Vector( -4630.205078, -5732.738770, -223.968750 ),
}

GM.Property:Register( Prop )