local Prop = {}
Prop.Name = "Court Shop 2"
Prop.Cat = "Stores"
Prop.Price = 800
Prop.Doors = {
	Vector( -7756.503906, -7549.510742, 72.031250 ),
	Vector( -7708.879395, -7548.417969, 72.031250 ),
}

GM.Property:Register( Prop )