local Prop = {}
Prop.Name = "Cosmos 169"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
	Vector( -8667.915039, -3899.255859, 72.031250 ),
	Vector( -8666.859375, -3851.215820, 72.031250 ),
}

GM.Property:Register( Prop )