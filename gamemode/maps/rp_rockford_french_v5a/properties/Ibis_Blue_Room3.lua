local Prop = {}
Prop.Name = "Ibis Red | Room 3"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
    Vector(445.006805, -6560.378906, 280.031250),
    Vector(89.613144, -6786.539063, 280.031250),
    Vector(89.887604, -6659.524902, 280.031250),
    Vector(-2.990279, -6716.795898, 280.031250),
}

GM.Property:Register( Prop )