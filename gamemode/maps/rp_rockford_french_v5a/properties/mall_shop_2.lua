local Prop = {}
Prop.Name = "Mall Shop 2"
Prop.Cat = "Stores"
Prop.Price = 800
Prop.Doors = {
	Vector( -2254.694580, 2303.174561, 600.031250 ),
	Vector( -2255.354248, 2337.524658, 600.031250 ),
}

GM.Property:Register( Prop )