local Prop = {}
Prop.Name = "Light Slum | Room 2"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
	Vector( -886.216248, -3353.301758, 185.031250 ),
	Vector( -965.697266, -3047.146729, 185.031250 ),
	Vector( -884.805847, -2959.981934, 185.031250 ),

}

GM.Property:Register( Prop )