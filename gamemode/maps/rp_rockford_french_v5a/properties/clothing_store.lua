local Prop = {}
Prop.Name = "Clothing Store"
Prop.Government = true
Prop.Doors = {
    Vector( -2918.744629, -3452.498047, 72.031250 ),
	Vector( -2971.968750, -3451.837402, 72.031250 ),
	Vector( -3300.028809, -3451.580566, 72.031250 ),
	Vector( -3352.035156, -3451.289551, 72.031250 ),
}

GM.Property:Register( Prop )