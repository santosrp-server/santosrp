local Prop = {}
Prop.Name = "Cosmos 255"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
	Vector( -8666.425781, 1685.091187, 72.031250 ),
	Vector( -8290.683594, 1419.436401, 72.031250 ),
}

GM.Property:Register( Prop )