local Prop = {}
Prop.Name = "Bar"
Prop.Cat = "Stores"
Prop.Price = 800
Prop.Doors = {
	Vector( 1726.850464, 6809.392090, 632.031250 ),
	Vector( 1726.318237, 6842.579590, 632.031250 ),
}

GM.Property:Register( Prop )