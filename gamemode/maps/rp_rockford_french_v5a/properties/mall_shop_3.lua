local Prop = {}
Prop.Name = "Mall Shop 3"
Prop.Cat = "Stores"
Prop.Price = 800
Prop.Doors = {
	Vector( -2252.214600, 2900.611816, 600.031250 ),
	Vector( -2254.441895, 2937.493408, 600.031250 ),
}

GM.Property:Register( Prop )