local Prop = {}
Prop.Name = "Ibis Blue | Room 5"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
    Vector(444.135803, -6560.352051, 416.031250),
    Vector(82.136314, -6790.463379, 416.031250),
    Vector(92.480713, -6660.778320, 416.031250),
    Vector(-5.042883, -6723.500977, 416.031250),
}

GM.Property:Register( Prop )