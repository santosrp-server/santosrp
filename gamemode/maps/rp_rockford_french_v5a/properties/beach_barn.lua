local Prop = {}
Prop.Name = "Beach Barn"
Prop.Cat = "Warehouse"
Prop.Price = 800
Prop.Doors = {
	Vector( 11501.215820, -9435.344727, 388.031250 ),
	Vector( 11438.156250, -9439.085938, 388.031250 ),
}

GM.Property:Register( Prop )