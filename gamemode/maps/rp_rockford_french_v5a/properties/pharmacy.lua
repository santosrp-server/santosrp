local Prop = {}
Prop.Name = "Pharmacy"
Prop.Cat = "Stores"
Prop.Price = 800
Prop.Doors = {
	Vector( -6098.740723, -3631.037842, 72.031250 ),
	Vector( -6064.945313, -2991.779053, 72.031250 ),
}

GM.Property:Register( Prop )