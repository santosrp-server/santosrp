local Prop = {}
Prop.Name = "Realtor"
Prop.Government = true
Prop.Doors = {
	Vector( 1786.432495, 5744.881348, 600.031250 ),
	Vector( 1786.482300, 5711.690918, 600.031250 ),
	Vector( 1561.925903, 5763.965332, 600.031250 ),
}

GM.Property:Register( Prop )