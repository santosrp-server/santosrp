local Prop = {}
Prop.Name = "Leroy Merlin"
Prop.Cat = "Warehouse"
Prop.Price = 800
Prop.Doors = {
	Vector( -5673.353027, 7868.770508, 64.031250 ),
	Vector( -5674.178711, 7828.083008, 64.031250 ),
	Vector( -5677.343750, 7754.149902, 64.031250 ),
	Vector( -5676.077148, 7713.473633, 64.031250 ),
	Vector( -6369.996582, 6816.654785, 64.031250 ),
	Vector( -6448.355469, 6236.736816, 64.031250 ),
}

GM.Property:Register( Prop )