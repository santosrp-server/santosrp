local Prop = {}
Prop.Name = "Main Square | Electronics"
Prop.Government = true
Prop.Doors = {
	Vector( -4505.220215, -3452.372803, 72.031250 ),
	Vector( -4464.120605, -3451.408203, 72.031250 ),
}

GM.Property:Register( Prop )