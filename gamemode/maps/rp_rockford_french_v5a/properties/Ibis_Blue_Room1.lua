local Prop = {}
Prop.Name = "Ibis Blue | Room 1"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
    Vector(443.603180, -6555.034668, 144.031250),
    Vector(82.552345, -6786.180176, 144.031250),
    Vector(-2.923290, -6718.069336, 144.031250),
    Vector(87.179291, -6661.423340, 144.031250),
}

GM.Property:Register( Prop )