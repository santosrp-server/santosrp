local Prop = {}
Prop.Name = "Mall Supermarket"
Prop.Government = true
Prop.Doors = {
	Vector( -2127.051758, 3172.365967, 600.031250 ),
	Vector( -2086.355469, 3172.382568, 600.031250 ),
}

GM.Property:Register( Prop )