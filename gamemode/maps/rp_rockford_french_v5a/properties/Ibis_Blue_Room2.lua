local Prop = {}
Prop.Name = "Ibis Blue | Room 2"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
    Vector(707.003357, -6565.034668, 280.031250),
    Vector(1056.890503, -6661.896484, 280.031250),
    Vector(1062.711060, -6786.805176, 280.031250),
    Vector(1157.574829, -6730.430664, 280.031250),
}

GM.Property:Register( Prop )