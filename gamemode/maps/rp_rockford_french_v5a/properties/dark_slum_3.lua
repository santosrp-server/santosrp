local Prop = {}
Prop.Name = "Dark Slum | Room 3"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
	Vector( -1866.252686, -3353.582275, 305.031250 ),
	Vector( -1940.181152, -3050.553955, 305.031250 ),
	Vector( -1869.267212, -2961.636719, 305.031250 ),
}

GM.Property:Register( Prop )