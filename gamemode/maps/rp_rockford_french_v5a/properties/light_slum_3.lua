local Prop = {}
Prop.Name = "Light Slum | Room 3"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
	Vector( -886.604370, -3352.414063, 305.031250 ),
	Vector( -962.151123, -3049.071289, 305.031250 ),
	Vector( -891.126465, -2960.186523, 305.031250 ),

}

GM.Property:Register( Prop )