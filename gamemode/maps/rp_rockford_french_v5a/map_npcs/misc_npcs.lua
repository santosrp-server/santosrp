--[[
	Name: misc_npcs.lua
	For: TalosLife
	By: TalosLife
]]--

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "evocity_dmv",
	pos = { Vector( -4164.034180, -8476.631836, 64 ) },
	angs = { Angle( 0, 90, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "healer",
	pos = { Vector( -125.150238, 8506.908203, 613 ) },
	angs = { Angle( 0, 180, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "bank_storage",
	pos = { Vector( -5431.505859, -5451.973145, 8 ) },
	angs = { Angle( 0, 90, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "chop_shop",
	pos = { Vector( 7090, -13532, 400 ) },
	angs = { Angle( 0, 65, 0 ) },
}