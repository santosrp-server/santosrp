--[[
	Name: atms.lua
	For: TalosLife
	By: TalosLife
]]--

local MapProp = {}
MapProp.ID = "atms"
MapProp.m_tblSpawn = {}
MapProp.m_tblAtms = {
	{ pos = Vector( -4885.423340, -5207.920898, 0 ), ang = Angle( -0.848909, 90.140511, 0.000000 ) },
	{ pos = Vector( -4960.190430, -5207.968750, 0 ), ang = Angle( -0.849036, 89.826927, 0.000000 ) },
	{ pos = Vector( -4162.409668, -1319.968750, 0 ), ang = Angle( 1.157529, 88.737755, 0.000000 ) },
	{ pos = Vector( -10447.968750, 1602.998413, 8 ), ang = Angle( 1.721669, -0.177723, 0.000000 ) },
	{ pos = Vector( -1287.689819, 2591.968750, 536 ), ang = Angle( 1.031986, -91.782455, 0.000000 ) },
	{ pos = Vector( -1189.130737, 2591.968750, 536 ), ang = Angle( -0.033914, -90.214966, 0.000000 ) },
}

function MapProp:CustomSpawn()
	for _, propData in pairs( self.m_tblAtms ) do
		local ent = ents.Create( "ent_atm" )
		ent:SetPos( propData.pos )
		ent:SetAngles( propData.ang )
		ent.IsMapProp = true
		ent:Spawn()
		ent:Activate()

		local phys = ent:GetPhysicsObject()
		if IsValid( phys ) then
			phys:EnableMotion( false )
		end
	end
end

GAMEMODE.Map:RegisterMapProp( MapProp )