--[[
	Name: xray.lua
	For: TalosLife
	By: TalosLife
]]--

local MapProp = {}
MapProp.ID = "xray"
MapProp.m_tblSpawn = {}
MapProp.m_tblXrays = {
	{ pos = Vector( -206.500000, 9022.218750, 631.312500 ), ang = Angle( 0, -90, 0 ) },
}

function MapProp:CustomSpawn()
	for _, propData in pairs( self.m_tblXrays ) do
		local ent = ents.Create( "ent_xray_machine" )
		ent:SetPos( propData.pos )
		ent:SetAngles( propData.ang )
		ent.IsMapProp = true
		ent:Spawn()
		ent:Activate()

		local phys = ent:GetPhysicsObject()
		if IsValid( phys ) then
			phys:EnableMotion( false )
		end
	end
end

GAMEMODE.Map:RegisterMapProp( MapProp )