--[[
	Name: police_station.lua
	For: TalosLife
	By: TalosLife
]]--

local MapProp = {}
MapProp.ID = "police_station"
MapProp.m_tblSpawn = {
	{ mdl = 'models/sickness/cubicle_full_01.mdl',pos = Vector('-8824.718750 -5002.812500 8.343750'), ang = Angle('0.000 0.000 0.000') },
	{ mdl = 'models/sickness/cubicle_full_01.mdl',pos = Vector('-8690.562500 -5002.812500 8.343750'), ang = Angle('0.000 0.000 0.000') },
	{ mdl = 'models/sickness/cubicle_full_01.mdl',pos = Vector('-8556.406250 -5002.812500 8.343750'), ang = Angle('0.000 0.000 0.000') },
	{ mdl = 'models/sickness/cubicle_full_01.mdl',pos = Vector('-8422.281250 -5002.812500 8.343750'), ang = Angle('0.000 0.000 0.000') },
	{ mdl = 'models/sickness/cubicle_full_01.mdl',pos = Vector('-8288.125000 -5002.812500 8.343750'), ang = Angle('0.000 0.000 0.000') },
	{ mdl = 'models/sickness/cubicle_full_01.mdl',pos = Vector('-8276.375000 -5237.593750 8.312500'), ang = Angle('0.000 180.000 0.000') },
	{ mdl = 'models/sickness/cubicle_full_01.mdl',pos = Vector('-8411.281250 -5237.593750 8.312500'), ang = Angle('0.000 180.000 0.000') },
	{ mdl = 'models/sickness/cubicle_full_01.mdl',pos = Vector('-8815.968750 -5237.593750 8.312500'), ang = Angle('0.000 180.000 0.000') },

	{ mdl = 'models/props_combine/breenchair.mdl',pos = Vector('-8819.312500 -5012.906250 8.343750'), ang = Angle('0.000 103.000 0.000') },
	{ mdl = 'models/props_combine/breenchair.mdl',pos = Vector('-8668.718750 -5022.062500 8.343750'), ang = Angle('0.000 103.000 0.000') },
	{ mdl = 'models/props_combine/breenchair.mdl',pos = Vector('-8537.281250 -5014.875000 8.343750'), ang = Angle('0.000 103.000 0.000') },
	{ mdl = 'models/props_combine/breenchair.mdl',pos = Vector('-8407.218750 -5010.781250 8.343750'), ang = Angle('0.000 103.000 0.000') },
	{ mdl = 'models/props_combine/breenchair.mdl',pos = Vector('-8275.281250 -5008.968750 8.343750'), ang = Angle('0.000 103.000 0.000') },
	{ mdl = 'models/props_combine/breenchair.mdl',pos = Vector('-8294.406250 -5225.312500 8.250000'), ang = Angle('0.000 -76.000 0.000') },
	{ mdl = 'models/props_combine/breenchair.mdl',pos = Vector('-8432.562500 -5228.937500 8.343750'), ang = Angle('0.000 -76.000 0.000') },
	{ mdl = 'models/props_combine/breenchair.mdl',pos = Vector('-8821.812500 -5235.000000 8.343750'), ang = Angle('0.000 -76.000 0.000') },
}

GAMEMODE.Map:RegisterMapProp( MapProp )