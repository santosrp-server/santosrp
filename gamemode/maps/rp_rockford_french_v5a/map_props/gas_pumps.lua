--[[
	Name: gas_pumps.lua
	For: TalosLife
	By: TalosLife
]]--

local MapProp = {}
MapProp.ID = "gas_pumps"
MapProp.m_tblSpawn = {}
MapProp.m_tblEnts = {
	{ pos = Vector(2242.531250, 2027.281250, 544.281250), ang = Angle(0, 180, 0) },
	{ pos = Vector(2263.375000, 2401.906250, 544.500000), ang = Angle(0, 0, 0) },
	{ pos = Vector(-13930.343750, 2442.437500, 392.250000), ang = Angle(0, 180, 0) },
	{ pos = Vector(-13909.093750, 2824.812500, 392.406250), ang = Angle(0, 0, 0) },
}

function MapProp:CustomSpawn()
	for _, propData in pairs( self.m_tblEnts ) do
		local ent = ents.Create( "ent_fuelpump" )
		ent:SetPos( propData.pos )
		ent:SetAngles( propData.ang )
		ent.IsMapProp = true
		ent:Spawn()
		ent:Activate()

		local phys = ent:GetPhysicsObject()
		if IsValid( phys ) then
			phys:EnableMotion( false )
		end
	end
end

GAMEMODE.Map:RegisterMapProp( MapProp )