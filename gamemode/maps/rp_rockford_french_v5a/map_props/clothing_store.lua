--[[
	Name: clothing_store.lua
	For: TalosLife
	By: TalosLife
]]--

local MapProp = {}
MapProp.ID = "clothing_store"
MapProp.m_tblSpawn = {
	{ mdl = 'models/props/de_tides/vending_tshirt.mdl', pos = Vector('-2345.402588 -3059.934814 -409.607994'), ang = Angle('0.000 0.088 0.000') },
}

MapProp.m_tblMenuTriggers = {
	{ pos = Vector('-2776.258057 -2815.680908 58.639050'), ang = Angle('0 180 0'), msg = "(Use) Purchase Clothing", menu = "clothing_items_store" },
	{ pos = Vector('-2776.333740 -2895.076416 60.049355'), ang = Angle('0 180 0'), msg = "(Use) Purchase Clothing", menu = "clothing_items_store" },
	{ pos = Vector('-2776.274902 -2975.674561 60.703968'), ang = Angle('0 180 0'), msg = "(Use) Purchase Clothing", menu = "clothing_items_store" },
}

function MapProp:CustomSpawn()
	for _, propData in pairs( self.m_tblMenuTriggers ) do
		local ent = ents.Create( "ent_menu_trigger" )
		ent:SetPos( propData.pos )
		ent:SetAngles( propData.ang )
		ent:SetMenu( propData.menu )
		ent.IsMapProp = true
		ent:Spawn()
		ent:Activate()
		ent:SetText( propData.msg )
	end
end

GAMEMODE.Map:RegisterMapProp( MapProp )