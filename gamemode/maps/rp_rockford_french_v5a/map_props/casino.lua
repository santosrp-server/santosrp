--[[
	Name: casino.lua
	For: TalosLife
	By: TalosLife
]]--

local MapProp = {}
MapProp.ID = "casino"
MapProp.m_tblSpawn = {}
MapProp.m_tblCasinos = {
	{ class = "pcasino_wheel_slot_machine", pos = Vector(-3881.468750, -3398.750000, 52.093750), ang = Angle(0, 0, 0) },
	{ class = "pcasino_wheel_slot_machine", pos = Vector(-3880.343750, -3310.812500, 52.218750), ang = Angle(0, 0, 0) },

	{ class = "pcasino_slot_machine", pos = Vector(-3865.906250, -3237.937500, 56.375000), ang = Angle(0, 0, 0) },
	{ class = "pcasino_slot_machine", pos = Vector(-3864.687500, -3138.500000, 56.375000), ang = Angle(0, 0, 0) },
	{ class = "pcasino_slot_machine", pos = Vector(-3865.625000, -3045.625000, 56.375000), ang = Angle(0, 0, 0) },

	{ class = "pcasino_blackjack_table", pos = Vector(-3830.843750, -2897.375000, 29.093750), ang = Angle(0, 0, 0) },
	{ class = "pcasino_blackjack_table", pos = Vector(-3704.437500, -2825.937500, 29.093750), ang = Angle(0, -90, 0) },
	{ class = "pcasino_blackjack_table", pos = Vector(-3588.687500, -2913.031250, 29.093750), ang = Angle(0, 180, 0) },

	{ class = "pcasino_npc", pos = Vector(-3550.656250, -2913.687500, 8.031250), ang = Angle(0, 180, 0) },
	{ class = "pcasino_npc", pos = Vector(-3704.375000, -2792.125000, 8.250000), ang = Angle(0, -90, 0) },
	{ class = "pcasino_npc", pos = Vector(-3865.156250, -2897.437500, 8.156250), ang = Angle(0, 0, 0) },
	{ class = "pcasino_npc", pos = Vector(-3607.593750, -3192.000000, 8.031250), ang = Angle(0, 90, 0) },
	{ class = "pcasino_npc", pos = Vector(-3607.093750, -3395.468750, 8.031250), ang = Angle(0, 90, 0) },

	{ class = "pcasino_roulette_table", pos = Vector(-3576.843750, -3146.093750, 28.125000), ang = Angle(0, -90, 0) },
	{ class = "pcasino_roulette_table", pos = Vector(-3577.500000, -3344.718750, 27.937500), ang = Angle(0, -90, 0) },	

	{ class = "pcasino_sign_stand", pos = Vector(-3823.125000, -3583.406250, 35.468750), ang = Angle(0, -40, 0) },
	{ class = "pcasino_sign_stand", pos = Vector(-3599.281250, -3583.968750, 35.406250), ang = Angle(0, -140, 0) },

	{ class = "pcasino_sign_wall_logo", pos = Vector(-3711.125000, -3444.062500, 151.375000), ang = Angle(0, 90, 0) },

	{ class = "pcasino_sign_plaque", pos = Vector(-3742.593750, -3459.468750, 121.781250), ang = Angle(0, -90, 0) },
}

function MapProp:CustomSpawn()

	for _, propData in pairs( self.m_tblCasinos ) do
		local ent = ents.Create( propData.class )
		ent:SetPos( propData.pos )
		ent:SetAngles( propData.ang )
		ent.IsMapProp = true
		ent:Spawn()
		ent:Activate()

		local phys = ent:GetPhysicsObject()
		if IsValid( phys ) then
			phys:EnableMotion( false )
		end
	end

end

GAMEMODE.Map:RegisterMapProp( MapProp )