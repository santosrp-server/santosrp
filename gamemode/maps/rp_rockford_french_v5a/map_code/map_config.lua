--[[
	Name: map_config.lua
	For: TalosLife
	By: TalosLife
]]--

// Used in the Inventory Map
GM.Config.MapDimensions = {
	TopLeft = { -15192, 15326 },
	BottomRight = { 15344, -15216 }
}

if CLIENT then

	/*
		AddWaypoint(id, image, name, color, scale, pos, ang)
		Images defined in sh_config.lua
	*/
	GM.Config.Waypoints = {
		{ "pd", "map_pd", "PD", color_white, 1.8, Vector(-8229, -5710, 0) },
		{ "hospital", "map_ems", "Hospital", color_white, 1.6, Vector(54, 8496, 0) },
		{ "cardealer", "map_cardealer", "Car Dealer", color_white, 1.8, Vector(-4447, -778, 0) },
		{ "bank", "map_bank", "Bank", color_white, 1.8, Vector(-5517, -5494, 0) },
		{ "towtruck", "map_tow", "Tow Truck", color_white, 1.5, Vector(-7280, 745, 0) },
		{ "shell1", "map_shell", "Shell", color_white, 1.6, Vector(2097, 2160, 0) },
		{ "shell2", "map_shell", "Shell", color_white, 1.6, Vector(-14067, 2708, 0) },
		{ "truck", "map_truck", "City Jobs", color_white, 1.6, Vector(-7070, 5832, 0) },
		{ "downtownshops", "map_shop", "Downtown Shops", color_white, 1.6, Vector(-4098, -3082, 0) },
		{ "supermarket", "map_shop", "Supermarket", color_white, 1.6, Vector(-2431, 3471, 0) },
	}

end

if SERVER then
	resource.AddWorkshop( "1424074969" )
	resource.AddWorkshop( "1427347342" )

	--[[ Car Dealer Settings ]]--
	GM.Config.CarSpawns = {
		{ Vector( -5201.373047, -1427.121826, 64.031250 ), Angle( 0.561904, -89.619385, 0.000000 ) },
		{ Vector( -4972.264648, -1425.597290, 64.031250 ), Angle( 0.561904, -89.619385, 0.000000 ) },
		{ Vector( -4721.368164, -1423.927979, 64.031250 ), Angle( 0.561904, -89.619385, 0.000000 ) },
		{ Vector( -4533.235352, -1420.040283, 64.031250 ), Angle( 0.722505, -88.816383, 0.000000 ) },
	}
	GM.Config.CarGarageBBox = {
		Min = Vector( -5383.279297, -2012.328979, -129.932526 ),
		Max = Vector( -3064.520508, -349.785156, 658.069641 ),
	}

	--[[ Robbery Settings ]]

	GM.Config.RobberyMoneyDrops = {

		Vector( -14606.083008, 2652.164551, 504.031250 ),
		Vector( 1570.470581, 2228.343750, 656.031250 ),
		Vector( -5258.583496, -2940.274658, 72.031250 ),
		Vector( -4867.029297, -2954.521729, 72.031250 ),
		Vector( -4482.790039, -2917.823486, 72.031250 ),
		Vector( -3012.499512, -3260.708008, 108.031250 ),		

	}

	--[[ Jail Settings ]]--
	GM.Config.JailPositions = {
		Vector( -7152.941406, -5787.862305, -1400 ),
		Vector( -7151.350586, -5559.465332, -1400 ),
		Vector( -7611.212891, -5528.903320, -1400 ),
		Vector( -7617.375977, -5768.806641, -1400 ),
	}
	GM.Config.JailReleasePositions = {
		Vector( -8557.254883, -5779.613281, 72.031250 ),
		Vector( -8619.270508, -5878.685547, 72.031250 ),
		Vector( -8708.294922, -5794.080566, 72.031250 ),
		Vector( -8774.986328, -5906.421387, 72.031250 ),
	}

	--[[ NPC Drug Dealer Settings ]]--
	GM.Config.DrugNPCPositions = {
		{ Vector( -3746.178223, 59.518246, 86.249695 ), Angle( 5.299816, -37.431000, 0.000000 ) },
		{ Vector( 13662.565430, -2724.337158, 384.030640 ), Angle( 2.409032, -48.432148, 0.000000 ) },
		{ Vector( -2589.476318, 13766.430664, 319.134827 ), Angle( 0.321232, 89.106865, 0.000000 ) },
		{ Vector( -10249.177734, 9510.731445, 72.031250 ), Angle( 2.007532, 5.002855, 0.000000 ) },
		{ Vector( -10265.190430, 3408.993164, 72.031250 ), Angle( -1.402402, -4.176769, 0.000000 ) },
	}

	--[[ Map Settings ]]--
	GM.Config.SpawnPoints = {
		Vector( -3846.661133, -8145.819824, 128.031250 ),
		Vector( -4020.956787, -8143.003418, 128.031250 ),
		Vector( -4173.871094, -8140.529785, 128.031250 ),
		Vector( -4361.573730, -8137.494629, 128.031250 ),
		Vector( -4512.484863, -8135.053711, 128.031250 ),
		Vector( -4669.163086, -8132.520508, 128.031250 ),
		Vector( -4819.716797, -8130.085938, 128.031250 ),
		Vector( -4992.685059, -8127.289063, 128.031250 ),
	}

	--[[ Business Settings ]]--
	GM.Config.BusinessSupplyDrops = {
		Vector( 1471.277466, -6076.395020, 72.485382 ),
	}

	GM.Config.HaulageIncDrops = {
		Vector( -10081.507813, 4689.520020, 74.031250 ),
	}


	--[[ Register the car customs shop location ]]--
	GM.CarShop.m_tblGarage["rp_rockford_french_v5a"] = {
		Doors = {
			["truckbay"] = { CarPos = Vector(-7524.404297, 1540.734375, 68.531250) },
			["carbay"] = { CarPos = Vector(-7527.860352,1310.864868, 68.531250) },
		},
		BBox = {
			Min = Vector( 0, 0, 0 ),
			Max = Vector( 0, 0, 0 ),
		},
		PlayerSetPos = Vector(0, 0, 0),
	}

	--[[ Fire Spawner Settings ]]--
	GM.Config.AutoFiresEnabled = false
	GM.Config.AutoFireSpawnMinTime = 60 *15
	GM.Config.AutoFireSpawnMaxTime = 60 *60
	GM.Config.AutoFireSpawnPoints = {
		Vector( -7313.187988, 3557.926025, 72.031250 ),
		Vector( -1816.031250, -2984.031250, 185.031250 ),
		Vector( -13919.029297, 2747.661133, 456.031250 ),
		Vector( 11207.075195, -8345.997070, 388.031250 ),
		Vector( -8461.106445, -14172.270508, 208.02339 ),
		Vector( 245.495132, 1563.968750, 601.031250 ),
		Vector( 2243.959229, 2101.555908, 608.022583 ),
		Vector( -6544.031250, -8236.968750, 72.031250 ),
		Vector( -941.840942, -5719.105957, 279.031250 ),
		Vector( 8970.031250, 944.894043, 1632.031250 ),
		Vector( 10538.031250, 7759.105957, 1632.031250 ),
		Vector( -8963.466797, 8615.889648, 72.031250 ),
		Vector( -8954.574219, 8522.129883, 72.031250 ),
	}
end

--[[ Car Dealer Settings ]]--
GM.Config.CarPreviewModelPos = Vector( -3353.479492, -802.124573, 0 )
GM.Config.CarPreviewCamPos = Vector( -3155.255859, -466.681305, 158.869476 )
GM.Config.CarPreviewCamAng = Angle( 20.825291, -133.535751, 0.000000 )
GM.Config.CarPreviewCamLen = 2

--[[ Chop Shop ]]--
GM.Config.ChopShop_ChopLocation = Vector( 7105.775879, -13681.086914, 384.031250 )

--[[ Weather & Day Night ]]--
GM.Config.Weather_SkyZPos = nil --Let the code figure the zpos out on this map
GM.Config.FogLightingEnabled = false --This will override the fog hiding the farz

GM.Config.UnwashedLenght = 60 -- Minutes
GM.Config.WashedWorth = 1000 -- Bag worth
GM.Config.PrivateBankDoors = {Vector(-5224.000000, -5815.000000, 62.281300),Vector(-5077.000000, -5548.000000, 62.281300),Vector(-5065.000000, -5204.000000, 62.281200)}
GM.Config.AlarmLocation = Vector(-5067.359863, -5720.408203, 108.781250)
GM.Config.BankCoolDown = 1 *60
GM.Config.BagLocations = {
Vector(-5055.224609, -5716.039551, 114.104080)
}