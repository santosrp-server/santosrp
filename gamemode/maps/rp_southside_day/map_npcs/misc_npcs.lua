--[[
	Name: misc_npcs.lua
	For: TalosLife
	By: TalosLife
]]--

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "evocity_dmv",
	pos = { Vector( 2496.833008, 5106.527832, 0.031248 ) },
	angs = { Angle( 0, 0, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "healer",
	pos = { Vector( 7648.427734, 4982.522949, -55.968750 ) },
	angs = { Angle( 0, -180, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "bank_storage",
	pos = { Vector( -1066.854980, 2818.085938, -103.968750 ) },
	angs = { Angle( 0, -90, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "chop_shop",
	pos = { Vector( -3166.353027, -2134.991943, -95.968750 ) },
	angs = { Angle( 0.000, -43.885, 0.000 ) },
}