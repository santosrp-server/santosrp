--[[
	Name: shop_npcs.lua
	For: TalosLife
	By: TalosLife
]]--

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "car_dealer",
	pos = { Vector( -7569.989746, 1590.346069, -31.968750 ) },
	angs = { Angle( 0, 0, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "car_spawn",
	pos = { Vector( 6435.029785, 7317.864258, 136.031250 ) },
	angs = { Angle( 0, 40, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "property_buy",
	pos = { Vector( 3292.185303, 5511.208496, 0.031250 ) },
	angs = { Angle( 0, -90, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "gas_clerk",
	pos = { Vector( 458.819153, 14027.570313, 128.031250 ) },
	angs = { Angle( 0, 0, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "gas_clerk_2",
	pos = { Vector( -5900.279297, 1537.031250, -15.968750 ) },
	angs = { Angle( 0, -90, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "electronics_clerk",
	pos = { Vector( -606.568604, 11493.366211, 136.031250 ) },
	angs = { Angle( 0, 180, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "hardware_clerk",
	pos = { Vector( 1296.799561, -1578.379395, -95.968750 ) },
	angs = { Angle( 0, 90, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "clothing_clerk",
	pos = { Vector( -2178.562988, 10542.968750, 128.031250 ) },
	angs = { Angle( 0, -90, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "book_clerk",
	pos = { Vector( -610.968750, 11412.722656, 136.031250 ) },
	angs = { Angle( 0, 180, 0 ) },
}

local randPos = table.Random( GAMEMODE.Config.DrugNPCPositions )
GAMEMODE.Map:RegisterNPCSpawn{
	UID = "drug_buyer",
	pos = { randPos[1] },
	angs = { randPos[2] },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "home_items_clerk",
	pos = { Vector( 1373.675781, -1569.031250, -95.968750 ) },
	angs = { Angle( 0, 90, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "club_foods_clerk",
	pos = { Vector( 458.968750, 13970.280273, 128.031250 ) },
	angs = { Angle( 0, 0, 0 ) },
}