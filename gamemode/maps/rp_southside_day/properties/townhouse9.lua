local Prop = {}
Prop.Name = "Townhouse 9"
Prop.Cat = "Houses"
Prop.Price = 300
Prop.Doors = {
	Vector( -6170.000000, 11188.000000, 212.000000 ),
	Vector( -6074.000000, 10612.000000, 212.000000 ),
	Vector( -6218.000000, 10740.000000, 212.000000 ),
	Vector( -6602.000000, 10644.000000, 212.000000 ),
	Vector( -6720.000000, 11056.000000, 220.000000 ),
	Vector( -6412.000000, 10602.000000, 212.000000 ),
}

GM.Property:Register( Prop )