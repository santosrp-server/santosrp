local Prop = {}
Prop.Name = "Bar"
Prop.Cat = "Stores"
Prop.Price = 350
Prop.Doors = {
	Vector( 5306.000000, 8610.000000, 196.000000 ),
	Vector( 4597.843750, 8276.000000, 196.000000 ),
	Vector( 4858.968750, 7914.000000, 196.000000 ),
	Vector( 4742.000000, 7652.000000, 188.000000 ),
	Vector( 4550.000000, 7412.000000, 188.000000 ),
	Vector( 4492.000000, 7770.000000, 188.000000 ),
}

GM.Property:Register( Prop )