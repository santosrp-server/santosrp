local Prop = {}
Prop.Name = "Townhouse 5"
Prop.Cat = "Houses"
Prop.Price = 300
Prop.Doors = {
	Vector( -5098.000000, 13452.000000, 220.000000 ),
	Vector( -5148.000000, 13926.000000, 220.000000 ),
	Vector( -5258.000000, 14052.000000, 220.000000 ),
	Vector( -5434.000000, 13916.000000, 220.000000 ),
	Vector( -5568.000000, 13504.000000, 220.000000 ),
	Vector( -5508.000000, 13942.000000, 220.000000 ),
}

GM.Property:Register( Prop )