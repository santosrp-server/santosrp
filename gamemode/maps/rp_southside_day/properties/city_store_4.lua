local Prop = {}
Prop.Name = "City Store 4"
Prop.Cat = "Stores"
Prop.Price = 350
Prop.Doors = {
	Vector( 1623.968750, 6660.000000, 68.000000 ),
	Vector( 1738.000000, 7244.000000, 68.000000 ),
	Vector( 1284.000000, 7462.000000, 72.000000 ),
}

GM.Property:Register( Prop )