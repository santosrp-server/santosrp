local Prop = {}
Prop.Name = "Warehouse 4"
Prop.Cat = "Warehouses"
Prop.Price = 500
Prop.Doors = {
	Vector( -13804.000000, -9980.000000, -104.000000 ),
	Vector( -12957.000000, -9670.000000, -204.000000 ),
	Vector( -12956.000000, -9984.000000, -144.000000 ),
}

GM.Property:Register( Prop )