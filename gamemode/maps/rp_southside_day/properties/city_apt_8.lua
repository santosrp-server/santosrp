local Prop = {}
Prop.Name = "City Apartment 8"
Prop.Cat = "Apartments"
Prop.Price = 250
Prop.Doors = {
	Vector( 1524.000000, 5114.000000, 612.000000 ),
	Vector( 1030.000000, 5244.000000, 612.000000 ),
}

GM.Property:Register( Prop )