local Prop = {}
Prop.Name = "Park Apartment A"
Prop.Cat = "Apartments"
Prop.Price = 200
Prop.Doors = {
	Vector( 1878.000000, 3148.000000, 220.000000 ),
	Vector( 1964.000000, 3354.000000, 220.000000 ),
}

GM.Property:Register( Prop )