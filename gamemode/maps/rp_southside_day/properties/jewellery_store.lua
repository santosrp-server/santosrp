local Prop = {}
Prop.Name = "Jewellery Store"
Prop.Cat = "Stores"
Prop.Price = 450
Prop.Doors = {
	Vector( -204.000000, 280.000000, -44.000000 ),
	Vector( -652.000000, 426.000000, -44.000000 ),
	Vector( -954.000000, 308.000000, -44.000000 ),
}

GM.Property:Register( Prop )