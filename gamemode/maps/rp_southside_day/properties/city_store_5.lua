local Prop = {}
Prop.Name = "City Store 5"
Prop.Cat = "Stores"
Prop.Price = 350
Prop.Doors = {
	Vector( 600.000000, 6852.000000, 68.000000 ),
	Vector( 438.000000, 7436.000000, 68.250000 ),
	Vector( 284.000000, 7658.000000, 76.000000 ),
}

GM.Property:Register( Prop )