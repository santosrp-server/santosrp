local Prop = {}
Prop.Name = "Diner"
Prop.Cat = "Stores"
Prop.Price = 400
Prop.Doors = {
	Vector( 882.000000, 10662.000000, 206.000000 ),
	Vector( 1038.000000, 10662.000000, 206.000000 ),
	Vector( 649.843750, 11180.000000, 220.000000 ),
	Vector( 1029.843750, 11180.000000, 220.000000 ),
	Vector( 1152.000000, 11338.000000, 221.000000 ),
}

GM.Property:Register( Prop )