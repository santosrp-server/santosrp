local Prop = {}
Prop.Name = "City Office Floor 6"
Prop.Cat = "Offices"
Prop.Price = 300
Prop.Doors = {
	Vector( -1722.000000, 4950.000000, 972.000000 ),
	Vector( -710.000000, 4950.000000, 972.000000 ),
	Vector( -922.000000, 5420.000000, 972.250000 ),
}


GM.Property:Register( Prop )