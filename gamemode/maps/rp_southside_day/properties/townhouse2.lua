local Prop = {}
Prop.Name = "Townhouse 2"
Prop.Cat = "Houses"
Prop.Price = 300
Prop.Doors = {
	Vector( -1317.968750, 13452.000000, 220.000000 ),
	Vector( -1380.000000, 13818.000000, 220.250000 ),
	Vector( -1286.000000, 14028.000000, 220.250000 ),
	Vector( -1162.000000, 13756.000000, 220.250000 ),
	Vector( -992.000000, 13376.000000, 252.000000 ),
	Vector( -1004.000000, 13774.000000, 220.250000 ),
}

GM.Property:Register( Prop )