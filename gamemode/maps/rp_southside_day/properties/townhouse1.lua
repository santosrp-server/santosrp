local Prop = {}
Prop.Name = "Townhouse 1"
Prop.Cat = "Houses"
Prop.Price = 300
Prop.Doors = {
	Vector( 3468.000000, 10778.000000, 252.000000 ),
	Vector( 3892.000000, 10558.000000, 252.000000 ),
	Vector( 3942.000000, 10724.000000, 252.000000 ),
	Vector( 3772.000000, 10454.000000, 252.000000 ),
	Vector( 3392.000000, 10336.000000, 252.000000 ),
	Vector( 3798.000000, 10348.000000, 252.000000 ),
}

GM.Property:Register( Prop )