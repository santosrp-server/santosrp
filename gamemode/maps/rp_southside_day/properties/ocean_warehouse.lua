local Prop = {}
Prop.Name = "Ocean Warehouse"
Prop.Cat = "Warehouses"
Prop.Price = 500
Prop.Doors = {
	Vector( -90.000000, -5635.000000, -268.000000 ),
	Vector( 224.000000, -5636.000000, -208.000000 ),
	Vector( -476.000000, -6400.000000, -208.000000 ),
	Vector( -757.968750, -6121.937500, -237.312500 ),
	Vector( -757.968750, -6313.937500, -237.312500 ),
	Vector( 395.968750, -3473.000000, -160.000000 ),
	Vector( 115.968750, -3473.000000, -160.000000 ),
}

GM.Property:Register( Prop )