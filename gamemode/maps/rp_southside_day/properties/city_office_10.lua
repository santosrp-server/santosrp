local Prop = {}
Prop.Name = "City Office Floor 10"
Prop.Cat = "Offices"
Prop.Price = 300
Prop.Doors = {
	Vector( -922.000000, 5420.000000, 1532.500000 ),
	Vector( -909.968750, 4950.000000, 1532.000000 ),
	Vector( -1522.000000, 4950.000000, 1532.000000 ),
}


GM.Property:Register( Prop )