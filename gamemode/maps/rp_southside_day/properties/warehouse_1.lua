local Prop = {}
Prop.Name = "Warehouse 1"
Prop.Cat = "Warehouses"
Prop.Price = 500
Prop.Doors = {
	Vector( -5219.000000, 3430.000000, 20.000000 ),
	Vector( -5200.000000, 3744.000000, 112.000000 ),
	Vector( -5200.000000, 3168.000000, 111.000000 ),
	Vector( -4108.000000, 3813.968750, 20.000000 ),
}

GM.Property:Register( Prop )