local Prop = {}
Prop.Name = "Hardware Store"
Prop.Government = true
Prop.Doors = {
	Vector( 1448.000000, -1036.000000, -44.000000 ),
	Vector( 1414.000000, -1691.000000, -44.000000 ),
	Vector( 1907.010010, -1750.000000, -44.000000 ),
}

GM.Property:Register( Prop )