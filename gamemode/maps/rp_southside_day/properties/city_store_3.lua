local Prop = {}
Prop.Name = "City Store 3"
Prop.Cat = "Stores"
Prop.Price = 350
Prop.Doors = {
	Vector( 3896.000000, 6276.000000, 68.000000 ),
	Vector( 3786.000000, 6860.000000, 68.000000 ),
	Vector( 3478.000000, 7163.968750, 72.000000 ),
}

GM.Property:Register( Prop )