local Prop = {}
Prop.Name = "Townhouse 8"
Prop.Cat = "Houses"
Prop.Price = 300
Prop.Doors = {
	Vector( -4902.000000, 11378.000000, 220.000000 ),
	Vector( -4886.000000, 11084.000000, 220.000000 ),
	Vector( -5450.000000, 10964.000000, 220.000000 ),
	Vector( -5568.000000, 11328.000000, 220.000000 ),
	Vector( -5476.000000, 10694.000000, 220.000000 ),
	Vector( -5340.000000, 10746.000000, 220.000000 ),
}

GM.Property:Register( Prop )