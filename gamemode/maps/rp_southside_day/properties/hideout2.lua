local Prop = {}
Prop.Name = "Hideout 2"
Prop.Cat = "Apartments"
Prop.Price = 350
Prop.Doors = {
	Vector( -9056.000000, 3000.000000, 104.000000 ),
	Vector( -9338.000000, 2953.000000, 52.000000 ),
	Vector( -8712.000000, 3702.000000, 244.000000 ),
}

GM.Property:Register( Prop )