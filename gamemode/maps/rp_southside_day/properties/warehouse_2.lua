local Prop = {}
Prop.Name = "Warehouse 2"
Prop.Cat = "Warehouses"
Prop.Price = 500
Prop.Doors = {
	Vector( 6470.000000, -1028.000000, -52.000000 ),
	Vector( 6784.000000, -1028.000000, 8.000000 ),
}

GM.Property:Register( Prop )