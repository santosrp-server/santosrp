local Prop = {}
Prop.Name = "Townhouse 4"
Prop.Cat = "Houses"
Prop.Price = 300
Prop.Doors = {
	Vector( -4074.000000, 13452.000000, 220.000000 ),
	Vector( -4124.000000, 13926.000000, 220.000000 ),
	Vector( -4234.000000, 14052.000000, 220.000000 ),
	Vector( -4410.000000, 13916.000000, 220.000000 ),
	Vector( -4544.000000, 13504.000000, 220.000000 ),
	Vector( -4484.000000, 13942.000000, 220.000000 ),
}

GM.Property:Register( Prop )