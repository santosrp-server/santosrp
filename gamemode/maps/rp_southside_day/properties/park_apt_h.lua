local Prop = {}
Prop.Name = "Park Apartment H"
Prop.Cat = "Apartments"
Prop.Price = 200
Prop.Doors = {
	Vector( 1098.000000, 3148.000000, 484.000000 ),
	Vector( 979.968750, 3386.000000, 484.000000 ),
}

GM.Property:Register( Prop )