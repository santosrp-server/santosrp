local Prop = {}
Prop.Name = "Warehouse 3"
Prop.Cat = "Warehouses"
Prop.Price = 500
Prop.Doors = {
	Vector( -1757.000000, -2042.000000, -44.000000 ),
	Vector( -1750.000000, -1728.000000, 16.000000 ),
}

GM.Property:Register( Prop )