local Prop = {}
Prop.Name = "City Store 6"
Prop.Cat = "Stores"
Prop.Price = 350
Prop.Doors = {
	Vector( 4646.000000, 3260.000000, 4.000000 ),
	Vector( 4534.000000, 2675.968750, 4.000000 ),
	Vector( 4620.000000, 2166.000000, -4.000000 ),
}

GM.Property:Register( Prop )