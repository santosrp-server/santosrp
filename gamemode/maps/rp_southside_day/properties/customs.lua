local Prop = {}
Prop.Name = "Customs"
Prop.Government = true
Prop.Doors = {
	Vector( -1852.000000, 5990.000000, 76.000000 ),
	Vector( -1896.000000, 6240.000000, 160.000000 ),
	Vector( -2298.000000, 6452.000000, 76.000000 ),
	Vector( -1896.000000, 6632.000000, 160.000000 ),
	Vector( -2428.000000, 6294.000000, 76.000000 ),
	Vector( -2618.000000, 6212.000000, 76.000000 ),
}

GM.Property:Register( Prop )