local Prop = {}
Prop.Name = "Park Apartment F"
Prop.Cat = "Apartments"
Prop.Price = 200
Prop.Doors = {
	Vector( 1930.000000, 2644.000000, 484.000000 ),
	Vector( 2051.968750, 2486.000000, 484.000000 ),
}

GM.Property:Register( Prop )