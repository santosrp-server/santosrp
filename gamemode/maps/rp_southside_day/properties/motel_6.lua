local Prop = {}
Prop.Name = "Motel 6"
Prop.Cat = "Hotels"
Prop.Price = 150
Prop.Doors = {
	Vector( -4772.000000, 1222.000000, 132.000000 ),
	Vector( -4774.000000, 1602.000000, 132.000000 ),
}

GM.Property:Register( Prop )