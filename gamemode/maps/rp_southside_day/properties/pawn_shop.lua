local Prop = {}
Prop.Name = "Pawn Shop"
Prop.Cat = "Stores"
Prop.Price = 350
Prop.Doors = {
	Vector( 3990.000000, 8642.000000, 188.000000 ),
	Vector( 3892.093750, 8314.000000, 188.000000 ),
	Vector( 3990.000000, 7907.968750, 188.000000 ),
	Vector( 3986.062500, 7656.000000, 192.000000 ),
	Vector( 4211.968750, 7609.968750, 188.000000 ),
}

GM.Property:Register( Prop )