local Prop = {}
Prop.Name = "Church"
Prop.Cat = "Warehouses"
Prop.Price = 500
Prop.Doors = {
	Vector( -2498.000000, 8498.000000, 230.000000 ),
	Vector( -2622.000000, 8498.000000, 230.000000 ),
	Vector( -2408.968750, 8279.000000, 218.000000 ),
	Vector( -2252.531250, 8278.375000, 218.250000 ),
}

GM.Property:Register( Prop )