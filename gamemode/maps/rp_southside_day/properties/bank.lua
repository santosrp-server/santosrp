local Prop = {}
Prop.Name = "Bank"
Prop.Government = true
Prop.Doors = {
	Vector( -1583.000000, 2178.000000, -44.000000 ),
	Vector( -1489.000000, 2178.000000, -44.000000 ),
	Vector( -694.000000, 2780.000000, -48.000000 ),
	Vector( -901.000000, 3040.000000, -48.000000 ),
}

GM.Property:Register( Prop )