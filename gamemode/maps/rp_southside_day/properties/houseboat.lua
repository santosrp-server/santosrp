local Prop = {}
Prop.Name = "Houseboat"
Prop.Cat = "Houses"
Prop.Price = 400
Prop.Doors = {
	Vector( 6246.000000, -4724.000000, -260.000000 ),
	Vector( 6108.000000, -4262.000000, -252.000000 ),
	Vector( 6340.000000, -4226.000000, -260.000000 ),
	Vector( 6394.000000, -4254.000000, -130.000000 ),
}

GM.Property:Register( Prop )