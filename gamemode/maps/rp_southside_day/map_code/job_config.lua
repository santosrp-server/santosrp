--[[
	Name: job_config.lua
	For: TalosLife
	By: TalosLife
]]--

--[[ EMS Config ]]--
if SERVER then
	GM.Config.EMSParkingZone = {
		Min = Vector( 6386.468262, 4194.785645, -109.875336 ),
		Max = Vector( 7941.286133, 5084.780273, 127.786247 ),
	}
	

	GM.Config.EMSCarSpawns = {
		{ Vector( 6761.843750, 4882.437500, -69.843750 ), Angle( 0, -180, 0 ) },
		{ Vector( 6502.468750, 4904.687500, -69.812500 ), Angle( 0, -180, 0 ) },
	}
end

--[[ Fire Config ]]--
-- NOT CHANGED FOR SOUTHSIDE
if SERVER then
	GM.Config.FireParkingZone = {
		Min = Vector( 1812.988770, 3937.418945, 499.356049 ),
		Max = Vector( 2473.279297, 4677.401855, 987.542358 ),
	}
	
	

	GM.Config.FireCarSpawns = {
		{ Vector( 2211.588379, 4517.111328, 600.031250 ), Angle( 2.838100, -90, 0.000000 ) },
		{ Vector( 2211.361328, 4336.678711, 600.031250 ), Angle( 2.838100, -90, 0.000000 ) },
		{ Vector( 2211.109863, 4138.150879, 600.031250 ), Angle( 2.838100, -90, 0.000000 ) },
	}
	
	
	GM.Config.FireStationGarageDoors = {
        Vector( 2439.049316, 4526.585449, 600.031250 ),
        Vector( 2441.297119, 4344.121094, 600.031250 ),
        Vector( 2439.140381, 4169.350586, 600.031250 ),
	}

end

--[[ Bank Door Config]]--
if SERVER then
	GM.Config.PrivateBankDoors = {
		Vector( -694.000000, 2780.000000, -48.000000	),
		Vector( -901.000000, 3040.000000, -48.000000	),
	}
end

--[[ Cop Config ]]--
if SERVER then
	GM.Config.CopParkingZone = {
		Min = Vector( 7662.227051, 7243.433105, -110.290039 ),
		Max = Vector( 8889.108398, 8993.025391, 88.856659 ),
	}

	GM.Config.CopCarSpawns = {
		{ Vector( 7855.905762, 7705.382324, -129.530777 ), Angle( 0, -90, 0 ) },
		{ Vector( 7852.044434, 7889.919434, -129.287201 ), Angle( 0, -90, 0 ) },
		{ Vector( 7866.686523, 8170.183594, -129.309723 ), Angle( 0, -90, 0 ) },
		{ Vector( 7873.762207, 8359.233398, -129.263580 ), Angle( 0, -90, 0 ) },
	}
end

--[[ Tow Config ]]--
if SERVER then
	GM.Config.TowWelderZone = {
		Min = Vector( 3047.700195, -2161.637207, -159.213242 ),
		Max = Vector( 3648.603027, -1425.261353, 72.982002 ),
	}

	GM.Config.TowParkingZone = {
		Min = Vector( 3692.836914, -2507.176514, -182.127655 ),
		Max = Vector( 5311.310059, -825.326599, 281.144653 ),
	}

	GM.Config.TowCarSpawns = {
		{ Vector( 4032.419434, -2106.370361, -130.488190 ), Angle( 0, 0, 0 ) },
		{ Vector( 4314.433594, -2096.451660, -115.587807 ), Angle( 0, 0, 0 ) },
		{ Vector( 4055.225586, -966.063293, -102.714500 ), Angle( 0, -90, 0 ) },
	}
end

--[[ Taxi Config ]]--
if SERVER then
	GM.Config.TaxiParkingZone = {
		Min = Vector( -584.874390, 12688.536133, 84.583748 ),
		Max = Vector( 84.156914, 13735.572266, 415.016632 ),
	}

	GM.Config.TaxiCarSpawns = {
		{ Vector( -380.336365, 13629.514648, 119.315353 ), Angle( 0, -90, 0 ) },
		{ Vector( -392.686646, 13416.923828, 119.306686 ), Angle( 0, -90, 0 ) },
		{ Vector( -398.947632, 13230.240234, 119.326462 ), Angle( 0, -90, 0 ) },
	}
end

--[[ Mail Config ]]--
if SERVER then
	GM.Config.MailParkingZone = {
		Min = Vector( -2928.082520, -2544.008789, -163.320923 ),
		Max = Vector( -1782.573486, -1168.408203, 469.822662 ),
	}

	GM.Config.MailCarSpawns = {
		{ Vector( -2642.113770, -2275.864014, -97.229500 ), Angle( 0,0,0 ) },
		{ Vector( -2472.713867, -2267.328857, -96.805634 ), Angle( 0,0,0 ) },
		{ Vector( -2292.228760, -2258.214600, -96.982338 ), Angle( 0,0,0 ) },
		{ Vector( -2127.249512, -2249.872314, -96.663918 ), Angle( 0,0,0 ) },
		{ Vector( -1933.072876, -2277.779785, -96.980682 ), Angle( 0,0,0 ) },
	}
end

if SERVER then
	GM.Config.SSParkingZone = {
		Min = Vector( 3702.625977, 3344.994873, -97.339310 ),
		Max = Vector( 5339.427246, 4487.018555, 292.726807 ),
	}

	GM.Config.SSCarSpawns = {
		{ Vector( 4123.563477, 4242.147949, -64.536980 ), Angle( 0, 90, 0.000000 ) },
		{ Vector( 4511.979004, 4224.014648, -64.493912 ), Angle( 0, 90, 0.000000 ) },
		{ Vector( 5042.955566, 4201.176270, -64.507851 ), Angle( 0, 90, 0.000000 ) },
		{ Vector( 4893.249023, 3564.119873, -64.527367 ), Angle( 0, -90, 0.000000 ) },
		{ Vector( 4443.926270, 3581.430908, -64.276939 ), Angle( 0, -90, 0.000000 ) },
		{ Vector( 4031.309326, 3594.829346, -64.497040 ), Angle( 0, -90, 0.000000 ) },
	}
end

--[[ Sales Config ]]--
if SERVER then
	GM.Config.SalesParkingZone = GM.Config.MailParkingZone
	GM.Config.SalesCarSpawns = GM.Config.MailCarSpawns
end

GM.Config.MailDepotPos = Vector( -1904.878540, -1228.697388, -31.968750 )
GM.Config.MailPoints = {
	{ Pos = Vector('-3639.249023 204.530777 -31.968750'), 	 	Name = "Motel", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('-6226.499512 1072.891357 32.031250'), 		Name = "Gas Station", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('-7349.767578 1281.817261 24.031250'), 		Name = "Car Dealer", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('-6347.712402 2894.690674 24.031250'), 		Name = "Pharmacy", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('-1457.823853 2086.016846 -39.968750'), 		Name = "Bank", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('3629.043213 5087.430176 8.031250'), 		Name = "City Hall", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('7070.693359 4781.004395 8.062084'), 		Name = "Hospital", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('7208.052734 7737.722656 192.031250'), 		Name = "Police Station", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('1076.417480 10594.591797 192.031250'), 		Name = "Diner", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('-1086.269287 11397.430664 192.031250'), 	Name = "Electronic Store", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('-1575.098145 10554.475586 192.031250'), 	Name = "Gun Store", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('-2159.215332 10923.223633 192.031250'), 	Name = "Clothing Store", MinPrice = 20, MaxPrice = 60 },
}

--[[ Bus Config ]]--
if SERVER then
	GM.Config.BusParkingZone = GM.Config.MailParkingZone
	GM.Config.BusCarSpawns = GM.Config.MailCarSpawns
end

--[[ Vault bag spawn locations ]]--
if SERVER then
	GM.Config.BagLocations = {
		Vector(-1816.662964, 3234.781250, -268.184052),
		Vector(-1825.915039, 3067.310547, -267.750000),
		Vector(-1817.609253, 3147.618408, -267.823303),
		Vector(-1868.214233, 3236.634521, -267.814911),
		Vector(-1867.795044, 3150.887939, -267.854309),
		Vector(-1865.954712, 3065.437012, -267.854309),
	}
end

if SERVER then
	timer.Create( "DoorRefresh", 300, 0, function()
		for k, v in pairs( ents.GetAll() ) do
			if IsValid( v ) and GAMEMODE.Util:IsDoor( v ) then
				v:SetHealth( 999999999 )
			end
		end
	end )
end