--[[
	Name: atms.lua
	For: TalosLife
	By: TalosLife
]]--

local MapProp = {}
MapProp.ID = "atms"
MapProp.m_tblSpawn = {}
MapProp.m_tblAtms = {
	{ pos = Vector( 3907.297607, 5512.615234, -55.582783 ), ang = Angle( -0.848909, -90.140511, 0.000000 ) },
	{ pos = Vector( 7686.463867, 5007.058594, -55.697289 ), ang = Angle( -0.849036, 89.826927, 0.000000 ) },
	{ pos = Vector( 7134.087891, 8112.687500, 128.596115 ), ang = Angle( 1.157529, -88.737755, 0.000000 ) },
	{ pos = Vector( 929.574524, 13808.771484, 128.380447 ), ang = Angle( 0, -90, 0 ) },
	{ pos = Vector( -1551.143433, 10719.446289, 128.374405 ), ang = Angle( 0, -180, 0 ) },
	{ pos = Vector( -1679.062012, 2386.969971, -103.498459 ), ang = Angle( 0, 180, 0 ) },
}

function MapProp:CustomSpawn()
	for _, propData in pairs( self.m_tblAtms ) do
		local ent = ents.Create( "ent_atm" )
		ent:SetPos( propData.pos )
		ent:SetAngles( propData.ang )
		ent.IsMapProp = true
		ent:Spawn()
		ent:Activate()

		local phys = ent:GetPhysicsObject()
		if IsValid( phys ) then
			phys:EnableMotion( false )
		end
	end
end

GAMEMODE.Map:RegisterMapProp( MapProp )