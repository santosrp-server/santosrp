--[[
	Name: job_item_lockers.lua
	For: SantosRP
	By: Ultra
]]--

local MapProp = {}
MapProp.ID = "job_item_lockers"
MapProp.m_tblSpawn = {}
MapProp.m_tblEnts = {
	{ pos = Vector(7193.028320, 5424.303223, -20.153931), ang = Angle(0, -90, 0), model = "models/props_c17/lockers001a.mdl", job = "JOB_EMS" },
	{ pos = Vector(3095.037598, 4892.229980, 259.848846), ang = Angle(0, -180, 0), model = "models/props_c17/lockers001a.mdl", job = "JOB_SSERVICE" },
	--{ pos = Vector(1635.323486, 3915.032959, 566), ang = Angle(0, 90, 0), model = "models/props_c17/lockers001a.mdl", job = "JOB_FIREFIGHTER" },
	{ pos = Vector(8392.973633, 8403.875000, 323.762512), ang = Angle(0, 0, 0), model = "models/props_c17/lockers001a.mdl", job = "JOB_POLICE" },
}

function MapProp:CustomSpawn()
	for _, propData in pairs( self.m_tblEnts ) do
		local ent = ents.Create( "ent_job_item_locker" )
		ent:SetPos( propData.pos )
		ent:SetAngles( propData.ang )
		ent:SetCollisionGroup( COLLISION_GROUP_NONE )
		ent:SetMoveType( MOVETYPE_NONE )
		ent.IsMapProp = true
		ent.MapPropID = id
		ent:Spawn()
		ent:Activate()
		ent:SetModel( propData.model )
		ent:SetJobID( _G[propData.job] )

		local phys = ent:GetPhysicsObject()
		if IsValid( phys ) then
			phys:EnableMotion( false )
		end
	end
end

GAMEMODE.Map:RegisterMapProp( MapProp )