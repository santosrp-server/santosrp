--[[
	Name: clothing_store.lua
	For: TalosLife
	By: TalosLife
]]--

local MapProp = {}
MapProp.ID = "clothing_store"
MapProp.m_tblSpawn = {
	{ mdl = 'models/props/de_tides/vending_tshirt.mdl', pos = Vector('-2345.402588 -3059.934814 -409.607994'), ang = Angle('0.000 0.088 0.000') },
}

MapProp.m_tblMenuTriggers = {
	{ pos = Vector('-2006.624268 10663.872070 169.071625'), ang = Angle('0 180 0'), msg = "(Use) Purchase Clothing", menu = "clothing_items_store" },
	{ pos = Vector('-2006.630493 10728.923828 164.584229'), ang = Angle('0 180 0'), msg = "(Use) Purchase Clothing", menu = "clothing_items_store" },
	{ pos = Vector('-2252.922607 10786.702148 163.030151'), ang = Angle('0 -90 0'), msg = "(Use) Purchase Clothing", menu = "clothing_items_store" },
}

function MapProp:CustomSpawn()
	for _, propData in pairs( self.m_tblMenuTriggers ) do
		local ent = ents.Create( "ent_menu_trigger" )
		ent:SetPos( propData.pos )
		ent:SetAngles( propData.ang )
		ent:SetMenu( propData.menu )
		ent.IsMapProp = true
		ent:Spawn()
		ent:Activate()
		ent:SetText( propData.msg )
	end
end

GAMEMODE.Map:RegisterMapProp( MapProp )