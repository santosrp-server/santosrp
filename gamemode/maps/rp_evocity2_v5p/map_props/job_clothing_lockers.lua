--[[
	Name: job_clothing_lockers.lua
	For: TalosLife
	By: TalosLife
]]--

local MapProp = {}
MapProp.ID = "job_clothing_lockers"
MapProp.m_tblSpawn = {}
MapProp.m_tblLockers = {
	--not already in the map
	{ pos = Vector(4552.002441, 13682.834961, 100), ang = Angle(0, -90, 0), id = "JOB_FIREFIGHTER" }, --Fire
	{ pos = Vector(-586.968750, -1896.853271, -393), ang = Angle(0, 0, 0), id = "JOB_POLICE" }, --Police
	//{ pos = Vector(-295.900757, -2342.680908, 500), ang = Angle(0, 0, 0), id = "JOB_FBI" }, --Police
}

function MapProp:CustomSpawn()
	for _, propData in pairs( self.m_tblLockers ) do
		local ent = ents.Create( "ent_job_clothing_locker" )
		ent:SetJobID( _G[propData.id] )
		ent:SetPos( propData.pos )
		ent:SetAngles( propData.ang )
		ent.IsMapProp = true
		ent:Spawn()
		ent:Activate()

		local phys = ent:GetPhysicsObject()
		if IsValid( phys ) then
			phys:EnableMotion( false )
		end
	end
end

GAMEMODE.Map:RegisterMapProp( MapProp )