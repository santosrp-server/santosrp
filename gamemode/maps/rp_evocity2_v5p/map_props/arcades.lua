--[[
	Name: atms.lua
	For: TalosLife
	By: TalosLife
]]--

local MapProp = {}
MapProp.ID = "arcades"
MapProp.m_tblSpawn = {}
MapProp.m_tblArcades = {
	{ class = "arcade_pacman", pos = Vector(2706.775879, 276.206879, 180), ang = Angle(0, 90, 0) },
	{ class = "arcade_pong", pos = Vector(2778.500488, 277.944611, 180), ang = Angle(0, 90, 0) },
	{ class = "arcade_spaceinvaders", pos = Vector(2858.444824, 279.209198, 180), ang = Angle(0, 90, 0) },
}

function MapProp:CustomSpawn()
	for _, propData in pairs( self.m_tblArcades ) do
		local ent = ents.Create( propData.class )
		ent:SetPos( propData.pos )
		ent:SetAngles( propData.ang )
		ent.IsMapProp = true
		ent:Spawn()
		ent:Activate()

		local phys = ent:GetPhysicsObject()
		if IsValid( phys ) then
			phys:EnableMotion( false )
		end
	end
end

GAMEMODE.Map:RegisterMapProp( MapProp )