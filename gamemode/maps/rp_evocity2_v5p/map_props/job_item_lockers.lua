--[[
	Name: job_item_lockers.lua
	For: SantosRP
	By: Ultra
]]--

local MapProp = {}
MapProp.ID = "job_item_lockers"
MapProp.m_tblSpawn = {}
MapProp.m_tblEnts = {
	//{ pos = Vector(-2895.968750, 459.734161, 110), ang = Angle(0, 0, 0), model = "models/props_c17/lockers001a.mdl", job = "JOB_DOCTOR" },
	{ pos = Vector(-2895.998779, 432.034485, 108), ang = Angle(0, -180, 0), model = "models/props_c17/lockers001a.mdl", job = "JOB_EMS" },
	{ pos = Vector(-183.628845, -1431.996704, 3859.031250), ang = Angle(0, 90, 0), model = "models/props_c17/lockers001a.mdl", job = "JOB_SSERVICE" },
	{ pos = Vector(4905.135254, 13348.031250, 100), ang = Angle(0, 90, 0), model = "models/props_c17/lockers001a.mdl", job = "JOB_FIREFIGHTER" },
	{ pos = Vector(-368.261810, -1829.982422, -392.100677), ang = Angle(0, -90, 0), model = "models/props_c17/lockers001a.mdl", job = "JOB_POLICE" },
	//{ pos = Vector(-243.967026, -2432.377930, 500.019836), ang = Angle(0, 90, 0), model = "models/props_c17/lockers001a.mdl", job = "JOB_FBI" },
}

function MapProp:CustomSpawn()
	for _, propData in pairs( self.m_tblEnts ) do
		local ent = ents.Create( "ent_job_item_locker" )
		ent:SetPos( propData.pos )
		ent:SetAngles( propData.ang )
		ent:SetCollisionGroup( COLLISION_GROUP_NONE )
		ent:SetMoveType( MOVETYPE_NONE )
		ent.IsMapProp = true
		ent.MapPropID = id
		ent:Spawn()
		ent:Activate()
		ent:SetModel( propData.model )
		ent:SetJobID( _G[propData.job] )

		local phys = ent:GetPhysicsObject()
		if IsValid( phys ) then
			phys:EnableMotion( false )
		end
	end
end

GAMEMODE.Map:RegisterMapProp( MapProp )