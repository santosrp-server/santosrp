--[[
	Name: bank.lua
	For: Jake Butts
	By: Jake Butts
]]--

local MapProp = {}
MapProp.ID = "bank"
MapProp.m_tblSpawn = {
	{ mdl = 'models/props_building_details/storefront_template001a_bars.mdl',pos = Vector('1354.500000 259.500000 234.656250'), ang = Angle('0 0 90') },
	{ mdl = 'models/props_building_details/storefront_template001a_bars.mdl',pos = Vector('1352.062500 172.125000 234.593750'), ang = Angle('0 0 90') },
	{ mdl = 'models/props_building_details/storefront_template001a_bars.mdl',pos = Vector('1354.562500 89.343750 234.500000'), ang = Angle('0 0 90') },
}

GAMEMODE.Map:RegisterMapProp( MapProp )