local Prop = {}
Prop.Name = "City Studio 103"
Prop.Cat = "Apartments"
Prop.Price = 950
Prop.Doors = {
	Vector( 4331, 1594, 258 ),
	Vector( 4203, 1658, 258 ),
}

GM.Property:Register( Prop )