local Prop = {}
Prop.Name = "Drug Den"
Prop.Cat = "House"
Prop.Price = 340
Prop.Doors = {
	Vector ( 3802.968750, 2435.983643, 204.031250 ),
	Vector ( 3536.968750, 2817.654785, 204.031250 ),
	Vector ( 3303.980469, 2815.239746, 204.031250 ),
	Vector ( 3950.968750, 2812.365967, 204.031250 ),
	Vector ( 4278.968750, 2811.619629, 204.031250 ),
}

GM.Property:Register( Prop )