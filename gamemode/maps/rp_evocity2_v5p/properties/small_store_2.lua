local Prop = {}
Prop.Name = "Small Store 2"
Prop.Cat = "Stores"
Prop.Price = 500
Prop.Doors = {
	Vector( 10628, -12058, -1664.75 ),
	Vector( 10492, -11586, -1664.75 ),
}

GM.Property:Register( Prop )