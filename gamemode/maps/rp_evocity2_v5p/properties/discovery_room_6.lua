local Prop = {}
Prop.Name = "Discovery Room 6"
Prop.Cat = "House"
Prop.Price = 340
Prop.Doors = {
	Vector( -168.048782, 1004.968750, 908.031250 ),
	Vector( 148.759827, 1327.703613, 908.031250 ),
	Vector( 148.378021, 1396.468750, 908.031250 ),
}


GM.Property:Register( Prop )