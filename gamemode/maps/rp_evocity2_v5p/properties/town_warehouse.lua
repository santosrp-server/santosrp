local Prop = {}
Prop.Name = "Town Warehouse"
Prop.Cat = "Warehouse"
Prop.Price = 650
Prop.Doors = {
	Vector( 11347, -13732, -1664.75 ),
	Vector( 11347, -13439, -1655 ),
}

GM.Property:Register( Prop )