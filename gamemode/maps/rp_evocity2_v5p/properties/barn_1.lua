local Prop = {}
Prop.Name = "Barn 1"
Prop.Cat = "Warehouse"
Prop.Price = 2200
Prop.Doors = {
	Vector( 13297, -3332, -1617 ),
	Vector( 13481, -3332, -1617 ),
}

GM.Property:Register( Prop )