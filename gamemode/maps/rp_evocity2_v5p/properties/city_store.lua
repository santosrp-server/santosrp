local Prop = {}
Prop.Name = "City Store"
Prop.Cat = "Stores"
Prop.Price = 250
Prop.Doors = {
	Vector( 3620, -3147, 130.25 ),
}

GM.Property:Register( Prop )