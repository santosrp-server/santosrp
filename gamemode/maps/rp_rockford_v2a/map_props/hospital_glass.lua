--[[
	Name: hospital_Glass
	For: TalosLife
	By: TalosLife & Rusitc & Devon
]]--

local MapProp = {}
MapProp.ID = "hospital_glass"
MapProp.m_tblSpawn = {
{ mdl = 'models/props_phx/construct/glass/glass_plate2x4.mdl',pos = Vector('-127.136124 -6036.396973 124.524559'), ang = Angle('90.000 179.995 180.000') },
{ mdl = 'models/props_phx/construct/glass/glass_plate2x4.mdl',pos = Vector('-127.135445 -5816.794922 124.524559'), ang = Angle('90.000 179.995 180.000') },
{ mdl = 'models/props/cs_office/chair_office.mdl',pos = Vector('-623.883240 -6305.453613 64.396111'), ang = Angle('0.044 177.896 -0.066') },
{ mdl = 'models/props_unique/coffeepot01.mdl',pos = Vector('-287.925903 -6424.126953 98.132324'), ang = Angle('0.269 139.016 -0.989') },
{ mdl = 'models/props_interiors/refrigerator03.mdl',pos = Vector('-286.538727 -6399.194824 64.338600'), ang = Angle('0.022 -179.654 0.060') },
{ mdl = 'models/props/cs_office/shelves_metal2.mdl',pos = Vector('-279.681793 -6345.481934 64.272926'), ang = Angle('0.011 0.000 0.000') },
{ mdl = 'models/props_interiors/styrofoam_cups.mdl',pos = Vector('-274.417389 -6424.977051 112.121552'), ang = Angle('0.220 -173.727 0.220') },
{ mdl = 'models/props/cs_office/shelves_metal3.mdl',pos = Vector('-278.443420 -6276.735840 64.362122'), ang = Angle('0.000 0.000 0.000') },
{ mdl = 'models/props/cs_office/microwave.mdl',pos = Vector('-285.950470 -6526.121582 98.107674'), ang = Angle('-0.192 54.152 -0.357') },
{ mdl = 'models/props/cs_office/bookshelf1.mdl',pos = Vector('-677.526978 -6545.017090 64.363358'), ang = Angle('0.044 90.077 0.005') },
{ mdl = 'models/props/cs_office/offcertificatea.mdl',pos = Vector('-264.353363 -6465.718262 124.795540'), ang = Angle('0.000 89.995 0.000') },
{ mdl = 'models/props/cs_office/bookshelf3.mdl',pos = Vector('-730.634705 -6544.874512 64.280815'), ang = Angle('0.011 90.044 -0.005') },
{ mdl = 'models/props/cs_office/file_cabinet1_group.mdl',pos = Vector('-983.637817 -6537.810547 64.288635'), ang = Angle('0.011 90.071 0.000') },
{ mdl = 'models/props/cs_office/file_box.mdl',pos = Vector('-987.293274 -6537.924805 127.748253'), ang = Angle('-0.022 13.563 -0.533') },
{ mdl = 'models/props/cs_office/file_cabinet2.mdl',pos = Vector('-935.892395 -6539.125488 64.405830'), ang = Angle('-0.176 72.729 0.121') },
{ mdl = 'models/props_combine/breenchair.mdl',pos = Vector('-967.645325 -6098.518066 64.275490'), ang = Angle('-0.016 -89.962 0.005') },
{ mdl = 'models/props/cs_office/table_meeting.mdl',pos = Vector('-795.488098 -6305.739258 64.453438'), ang = Angle('0.000 0.000 0.000') },
{ mdl = 'models/props_interiors/phone.mdl',pos = Vector('-945.847961 -6131.515137 95.888077'), ang = Angle('-0.170 116.543 1.203') },
{ mdl = 'models/testmodels/macbook_pro.mdl',pos = Vector('-982.827393 -6135.654297 95.641525'), ang = Angle('-0.016 67.489 0.022') },
{ mdl = 'models/props/cs_office/bookshelf2.mdl',pos = Vector('-989.396423 -6063.175781 64.273827'), ang = Angle('-0.165 -89.962 0.000') },
{ mdl = 'models/props/cs_office/cardboard_box03.mdl',pos = Vector('-911.347839 -6140.833008 64.342064'), ang = Angle('0.033 -22.159 0.000') },
{ mdl = 'models/props/cs_office/file_box.mdl',pos = Vector('-891.052063 -6067.048828 64.490356'), ang = Angle('0.000 -18.913 -0.082') },
{ mdl = 'models/props/cs_office/bookshelf3.mdl',pos = Vector('-936.205933 -6063.208496 64.344986'), ang = Angle('-0.033 -90.055 -0.044') },
{ mdl = 'models/props_interiors/furniture_chair03a.mdl',pos = Vector('-436.396454 -6531.881836 83.619118'), ang = Angle('0.055 89.341 -0.104') },
{ mdl = 'models/props/cs_office/sofa.mdl',pos = Vector('-611.337280 -6077.583496 64.382240'), ang = Angle('-0.066 -89.967 0.016') },
{ mdl = 'models/props/cs_office/sofa_chair.mdl',pos = Vector('-521.022034 -6110.719727 64.296852'), ang = Angle('0.000 -136.099 0.000') },
{ mdl = 'models/sunabouzu/theater_table.mdl',pos = Vector('-609.741821 -6136.964844 80.497192'), ang = Angle('0.033 -0.242 0.055') },
{ mdl = 'models/props_interiors/magazine_rack.mdl',pos = Vector('-455.898895 -6068.371582 64.358467'), ang = Angle('0.027 -90.038 0.016') },
{ mdl = 'models/props/cs_office/trash_can_p.mdl',pos = Vector('-360.070953 -6065.294922 64.358116'), ang = Angle('-0.280 -179.912 -0.275') },
{ mdl = 'models/props/cs_office/file_cabinet1_group.mdl',pos = Vector('-824.532227 -6070.239258 64.265892'), ang = Angle('0.000 -89.995 0.000') },
{ mdl = 'models/props/cs_office/file_cabinet1.mdl',pos = Vector('-781.432678 -6070.660645 64.300797'), ang = Angle('-0.016 -89.989 0.049') },
{ mdl = 'models/props/cs_office/plant01.mdl',pos = Vector('-738.131531 -6077.080566 63.600925'), ang = Angle('-0.022 -67.495 0.022') },
{ mdl = 'models/props/cs_office/chair_office.mdl',pos = Vector('-748.323792 -6227.737305 64.562431'), ang = Angle('0.000 -89.995 0.000') },
{ mdl = 'models/props_wasteland/controlroom_desk001a.mdl',pos = Vector('-284.602509 -6483.600098 80.763626'), ang = Angle('-0.417 0.005 0.000') },
{ mdl = 'models/props/cs_office/chair_office.mdl',pos = Vector('-852.796631 -6227.737793 64.562431'), ang = Angle('0.000 -89.995 0.000') },
{ mdl = 'models/props_interiors/corkboardverticle01.mdl',pos = Vector('-350.972290 -6551.275879 129.745285'), ang = Angle('0.000 90.005 -0.005') },
{ mdl = 'models/props/cs_office/offpaintingf.mdl',pos = Vector('-437.444153 -6551.604980 137.145782'), ang = Angle('0.049 -0.033 0.000') },
{ mdl = 'models/props_interiors/furniture_chair03a.mdl',pos = Vector('-436.186676 -6448.509277 83.539848'), ang = Angle('0.055 -90.555 -0.104') },
{ mdl = 'models/props_interiors/furniture_chair03a.mdl',pos = Vector('-395.189880 -6488.952148 83.636360'), ang = Angle('0.027 179.830 -0.011') },
{ mdl = 'models/props_interiors/dining_table_round.mdl',pos = Vector('-437.342163 -6491.057617 64.274277'), ang = Angle('0.000 135.000 0.000') },
{ mdl = 'models/props_combine/breendesk.mdl',pos = Vector('-969.652466 -6140.664551 64.498482'), ang = Angle('0.110 -90.077 -0.055') },
{ mdl = 'models/props/cs_office/sofa_chair.mdl',pos = Vector('-699.345093 -6108.057129 64.328476'), ang = Angle('-0.005 -44.083 -0.033') },
{ mdl = 'models/props/cs_office/chair_office.mdl',pos = Vector('-852.795654 -6385.258301 64.562431'), ang = Angle('0.000 90.000 0.000') },
{ mdl = 'models/props/cs_office/offpaintingb.mdl',pos = Vector('-610.271057 -6056.404297 133.565140'), ang = Angle('0.000 -179.995 0.000') },
{ mdl = 'models/props_combine/breenchair.mdl',pos = Vector('-965.156677 -6306.942383 64.494324'), ang = Angle('0.000 0.000 0.000') },
{ mdl = 'models/props/cs_office/vending_machine.mdl',pos = Vector('-401.944489 -6074.312988 64.291245'), ang = Angle('0.000 0.000 0.000') },
{ mdl = 'models/props/cs_office/chair_office.mdl',pos = Vector('-748.322815 -6385.257813 64.562431'), ang = Angle('0.000 90.000 0.000') },
{ mdl = 'models/props_interiors/furniture_chair03a.mdl',pos = Vector('-477.280579 -6491.247070 83.539848'), ang = Angle('0.055 -0.928 -0.104') },
}

GAMEMODE.Map:RegisterMapProp( MapProp )