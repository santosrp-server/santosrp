--[[
	Name: misc_npcs.lua
	For: TalosLife
	By: TalosLife
]]--

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "evocity_dmv",
	pos = { Vector( -4734.677734, 10998.722656, 480.031250 ) },
	angs = { Angle( 0, -140, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "healer",
	pos = { Vector( -6903.781738, 7239.276855, 356.031250 ) },
	angs = { Angle( 0, -90, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "bank_storage",
	pos = { Vector( -4895.968750, 5268.631348, 352.031250 ) },
	angs = { Angle( 0, -180, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "chop_shop",
	pos = { Vector( -3657.332275, -7903.100586, 364.280884 ) },
   	angs = { Angle( 13, -92, 0 ) },
}