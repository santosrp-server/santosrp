local Prop = {}
Prop.Name = "Bazzar Shop 6"
Prop.Cat = "Stores"
Prop.Price = 100
Prop.Doors = {
	Vector( -13553, -10661, 359 ),
	Vector( -13697, -10890, 359 ),
}

GM.Property:Register( Prop )