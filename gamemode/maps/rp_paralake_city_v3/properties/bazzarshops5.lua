local Prop = {}
Prop.Name = "Bazzar Shop 5"
Prop.Cat = "Stores"
Prop.Price = 100
Prop.Doors = {
	Vector( -13553, -10181, 359 ),
	Vector( -13697, -10410, 359 ),
}

GM.Property:Register( Prop )