local Prop = {}
Prop.Name = "Bazzar Market 2"
Prop.Cat = "Stores"
Prop.Price = 50
Prop.Doors = {
	Vector( -12525, -8688.5, 347 ),
	Vector( -12467, -8455.5, 347 ),
}

GM.Property:Register( Prop )