local Prop = {}
Prop.Name = "Bank Of America"
Prop.Government = true
Prop.Doors = {
	{ Pos = Vector( -4847.5, 4819, 343 ), Locked = true },
	{ Pos = Vector( -4595, 4880.5, 343 ), Locked = true },
	{ Pos = Vector( -4437, 5375.5, 343 ), Locked = true },
	{ Pos = Vector( -3968.5, 5277, 347 ), Locked = true },
	{ Pos = Vector( -4400, 4656, 360 ), Locked = true },
}

GM.Property:Register( Prop )