local Prop = {}
Prop.Name = "Bazzar Shop 2"
Prop.Cat = "Stores"
Prop.Price = 100
Prop.Doors = {
	Vector( -13553, -8741, 359 ),
	Vector( -13697, -8970, 359 ),
}

GM.Property:Register( Prop )