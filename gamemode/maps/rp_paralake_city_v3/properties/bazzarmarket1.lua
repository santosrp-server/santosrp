local Prop = {}
Prop.Name = "Bazzar Market 1"
Prop.Cat = "Stores"
Prop.Price = 50
Prop.Doors = {
	Vector( -13553, -8261, 359 ),
	Vector( -13697, -8490, 359 ),
}

GM.Property:Register( Prop )