local Prop = {}
Prop.Name = "Jail"
Prop.Government = true
Prop.Doors = {
    { Pos = Vector( -8463, 10425, -720 ), Locked = true },
    { Pos = Vector( -8463, 10519, -720 ), Locked = true },
    { Pos = Vector( -8135, 10425, -720 ), Locked = true },
    { Pos = Vector( -8135, 10519, -720 ), Locked = true },
    { Pos = Vector( -8189, 10404, -720 ), Locked = true },
    { Pos = Vector( -8189, 10404, -720 ), Locked = true },
    { Pos = Vector( -7281, 10378, -723 ), Locked = true },
    { Pos = Vector( -7440, 10378, -723 ), Locked = true },
    { Pos = Vector( -7599, 10378, -723 ), Locked = true },
    { Pos = Vector( -7758, 10378, -724 ), Locked = true },
    { Pos = Vector( -7763, 10566, -723 ), Locked = true },
    { Pos = Vector( -7604, 10566, -723 ), Locked = true },
    { Pos = Vector( -7445, 10566, -723 ), Locked = true },
    { Pos = Vector( -7286, 10566, -723 ), Locked = true },
    { Pos = Vector( -7866, 10473, -716 ), Locked = true },
}

GM.Property:Register( Prop )