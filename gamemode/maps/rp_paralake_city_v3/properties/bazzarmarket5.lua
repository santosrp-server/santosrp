local Prop = {}
Prop.Name = "Bazzar Market 5"
Prop.Cat = "Stores"
Prop.Price = 50
Prop.Doors = {
	Vector( -11885, -8688.5, 347 ),
	Vector( -11827, -8455.5, 347 ),
}

GM.Property:Register( Prop )