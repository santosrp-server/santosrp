local Prop = {}
Prop.Name = "Bazzar Market 6"
Prop.Cat = "Stores"
Prop.Price = 50
Prop.Doors = {
	Vector( -11565, -9327.5, 347.20901489258 ),
	Vector( -11507, -9576.5, 346.99899291992 ),
}

GM.Property:Register( Prop )