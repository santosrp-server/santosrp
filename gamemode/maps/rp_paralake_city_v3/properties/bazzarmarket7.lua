local Prop = {}
Prop.Name = "Bazzar Market 7"
Prop.Cat = "Stores"
Prop.Price = 50
Prop.Doors = {
	Vector( -11565, -8688.5, 347 ),
	Vector( -11507, -8455.5, 347 ),
}

GM.Property:Register( Prop )