local Prop = {}
Prop.Name = "Bazzar Market 3"
Prop.Cat = "Stores"
Prop.Price = 50
Prop.Doors = {
	Vector( -12189, -9327.5, 347.20901489258 ),
	Vector( -12131, -9576.5, 346.99899291992 ),
}

GM.Property:Register( Prop )