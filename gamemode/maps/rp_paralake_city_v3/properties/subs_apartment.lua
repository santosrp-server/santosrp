local Prop = {}
Prop.Name = "Subs Apartment 101"
Prop.Cat = "Apartments"
Prop.Price = 100
Prop.Doors = {
	Vector( -3469, 9537, 374 ),
	Vector( -3257, 9229, 374 ),
	Vector( -3257, 9413, 374 ),
	Vector( -3515, 9191, 374 ),
}

GM.Property:Register( Prop )