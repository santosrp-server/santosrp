local Prop = {}
Prop.Name = "Bazzar Shop 7"
Prop.Cat = "Stores"
Prop.Price = 100
Prop.Doors = {
	Vector( -13553, -11141, 359 ),
	Vector( -13697, -11370, 359 ),
}

GM.Property:Register( Prop )