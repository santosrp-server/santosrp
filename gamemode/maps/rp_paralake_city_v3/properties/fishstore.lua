local Prop = {}
Prop.Name = "Fish Store"
Prop.Cat = "Stores"
Prop.Price = 150
Prop.Doors = {
	Vector( -6430.90625, 5142, 344.25 ),
	Vector( -6389.875, 5183, 344.25 ),
}

GM.Property:Register( Prop )