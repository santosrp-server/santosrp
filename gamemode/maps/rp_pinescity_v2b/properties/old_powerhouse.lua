local Prop = {}
Prop.Name = "Old Powerhouse"
Prop.Cat = "House"
Prop.Price = 800
Prop.Doors = {
	Vector( 10567.833984, 9472.160156, 182.405518 ),
	Vector( 10542.810547, 9427.847656, 182.031250 ),
	Vector( 10636.422852, 9270.519531, 182.031250 ),
	Vector( 10530.967773, 9953.500977, 152.031250 ),
}

GM.Property:Register( Prop )