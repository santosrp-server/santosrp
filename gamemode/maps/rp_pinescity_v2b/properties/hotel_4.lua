local Prop = {}
Prop.Name = "Pine Hotel - Room 4"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
	Vector( -6889.968750, 6934.925293, 300.031250 ),
	Vector( -6665.837891, 6973.031250, 300.031250 ),
}

GM.Property:Register( Prop )