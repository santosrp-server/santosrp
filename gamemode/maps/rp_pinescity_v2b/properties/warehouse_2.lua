local Prop = {}
Prop.Name = "Warehouse 2"
Prop.Cat = "Warehouse"
Prop.Price = 800
Prop.Doors = {
	Vector( -10164.687500, -4459.692871, 160.031250 ),
	Vector( -10219.261719, -4459.649414, 160.031250 ),
	Vector( -10200.757813, -5239.031738, 160.031250 ),
	Vector( -10894.952148, -5239.423828, 160.031250 ),
	Vector( -11218.554688, -5236.193848, 160.031250 ),
	Vector( -11546.102539, -5239.716797, 160.031250 ),
}

GM.Property:Register( Prop )