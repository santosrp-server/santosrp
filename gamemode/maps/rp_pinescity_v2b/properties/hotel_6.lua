local Prop = {}
Prop.Name = "Pine Hotel - Room 6"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
	Vector( -6889.968750, 6922.368652, 440.031250 ),
	Vector( -6668.845215, 6973.031250, 440.031250 ),
}

GM.Property:Register( Prop )