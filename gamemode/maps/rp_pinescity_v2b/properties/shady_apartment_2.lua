local Prop = {}
Prop.Name = "Shady Apartment 2"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
	Vector( -3539.425049, 1618.541626, 282.031250 ),
	Vector( -3523.422607, 1935.980103, 282.031250 ),
}

GM.Property:Register( Prop )