local Prop = {}
Prop.Name = "Shady Apartment 6"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
	Vector( -2015.255005, 1621.800293, 160.031250 ),
	Vector( -1900.169922, 1924.111206, 404.031250 ),
	Vector( -1832.529053, 1697.627563, 406.031250 )
}

GM.Property:Register( Prop )