local Prop = {}
Prop.Name = "City Apartment 3"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
	Vector( -11043.039063, 11881.968750, 600.031250 ),
	Vector( -10722.475586, 11933.968750, 600.031250 ),
	Vector( -10721.571289, 12103.968750, 600.031250 ),
	Vector( -10720.371094, 12438.968750, 600.031250 ),
	Vector( -11122.968750, 12537.209961, 600.031250 ),
}

GM.Property:Register( Prop )