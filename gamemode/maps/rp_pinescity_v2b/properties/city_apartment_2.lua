local Prop = {}
Prop.Name = "City Apartment 2"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
	Vector( -11433.028320, 11879.968750, 470.031250 ),
	Vector( -11543.181641, 12347.968750, 470.031250 ),
	Vector( -11541.733398, 12527.968750, 470.031250 ),
	Vector( -11749.008789, 11986.031250, 470.031250 ),
}

GM.Property:Register( Prop )