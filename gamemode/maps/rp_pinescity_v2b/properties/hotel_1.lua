local Prop = {}
Prop.Name = "Pine Hotel - Room 1"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
	Vector( -6107.810059, 6720.150391, 300.031250 ),
	Vector( -6324.891113, 6658.189941, 300.031250 ),
}

GM.Property:Register( Prop )