local Prop = {}
Prop.Name = "Shady Apartment 5"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
	Vector( -2015.255005, 1621.800293, 160.031250 ),
	Vector( -2002.977905, 1932.354980, 160.031250 ),
}

GM.Property:Register( Prop )