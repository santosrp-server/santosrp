local Prop = {}
Prop.Name = "High Street 1"
Prop.Cat = "Stores"
Prop.Price = 800
Prop.Doors = {
	Vector( -5710.031250, 11509.867188, 160.031250 ),
	Vector( -5710.031250, 11375.682617, 160.031250 ),
	Vector( -5648.031250, 11057.786133, 160.031250 ),
}

GM.Property:Register( Prop )