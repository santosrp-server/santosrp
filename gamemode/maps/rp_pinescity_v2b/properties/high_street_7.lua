local Prop = {}
Prop.Name = "High Street 7"
Prop.Cat = "Stores"
Prop.Price = 800
Prop.Doors = {
	Vector( -11594.531250, 12709.826172, 160.031250 ),
	Vector( -11664.468750, 12709.533203, 160.031250 ),
}

GM.Property:Register( Prop )