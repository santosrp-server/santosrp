local Prop = {}
Prop.Name = "Pine Hotel - Room 2"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
	Vector( -6096.189941, 6926.990234, 300.031250 ),
	Vector( -6320.171387, 6988.968750, 300.031250 ),
}

GM.Property:Register( Prop )