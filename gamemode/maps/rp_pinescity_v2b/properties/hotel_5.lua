local Prop = {}
Prop.Name = "Pine Hotel - Room 5"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
	Vector( -6887.810059, 6718.652832, 440.031250 ),
	Vector( -6660.570801, 6658.375977, 440.031250 ),
}

GM.Property:Register( Prop )