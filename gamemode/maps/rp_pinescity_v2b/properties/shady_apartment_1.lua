local Prop = {}
Prop.Name = "Shady Apartment 1"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
	Vector( -3537.816406, 1614.696289, 160.031250 ),
	Vector( -3524.916016, 1936.451660, 160.031250 ),
}

GM.Property:Register( Prop )