local Prop = {}
Prop.Name = "Oak City Fire Department"
Prop.Government = true
Prop.Doors = {
	Vector( -3739.294189, 601.145630, 160.031250 ),
	Vector( -3544.086182, 599.963196, 160.031250 ),
	Vector( -3335.023193, 600.613403, 160.031250 ),
	Vector( -3161.278320, 604.030029, 160.031250 ),
	Vector( -2973.308350, 171.779068, 160.031250 ),
	Vector( -2971.434570, 115.189705, 160.031250 ),
	Vector( -2619.899414, 602.396240, 160.031250 ),
	Vector( -2540.031250, 599.507385, 160.031250 ),
}

GM.Property:Register( Prop )