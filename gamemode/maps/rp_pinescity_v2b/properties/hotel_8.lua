local Prop = {}
Prop.Name = "Pine Hotel - Room 8"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
	Vector( -6109.974609, 6934.441406, 440.031250 ),
	Vector( -6318.537109, 6988.968750, 440.031250 ),
}

GM.Property:Register( Prop )