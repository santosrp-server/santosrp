local Prop = {}
Prop.Name = "Warehouse 3"
Prop.Cat = "Warehouse"
Prop.Price = 800
Prop.Doors = {
	Vector( -12727.189453, -5236.859375, 160.031250 ),
	Vector( -12783.681641, -5242.360352, 160.031250 ),
	Vector( -13477.827148, -5239.271973, 160.031250 ),
	Vector( -13775.523438, -5239.119141, 160.031250 ),
	Vector( -14088.707031, -5239.885254, 160.031250 ),
}

GM.Property:Register( Prop )