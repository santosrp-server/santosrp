local Prop = {}
Prop.Name = "High Street 2"
Prop.Cat = "Stores"
Prop.Price = 800
Prop.Doors = {
	Vector( -6209.968750, 10968.841797, 160.031250 ),
	Vector( -6386.031250, 11365.588867, 160.031250 ),
	Vector( -6386.031250, 11515.698242, 160.031250 ),
}

GM.Property:Register( Prop )