local Prop = {}
Prop.Name = "City Apartment 1"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
	Vector( -11043.876953, 11881.968750, 470.031250 ),
	Vector( -10720.892578, 11933.968750, 470.031250 ),
	Vector( -10721.444336, 12103.968750, 470.031250 ),
	Vector( -10721.442383, 12438.968750, 470.031250 ),
	Vector( -11104.531250, 12534.708008, 470.031250 ),
}

GM.Property:Register( Prop )