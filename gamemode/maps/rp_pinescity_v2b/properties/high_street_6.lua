local Prop = {}
Prop.Name = "High Street 6"
Prop.Cat = "Stores"
Prop.Price = 800
Prop.Doors = {
	Vector( -11664.468750, 11820.373047, 160.031250 ),
	Vector( -11594.531250, 11821.377930, 160.031250 ),
}

GM.Property:Register( Prop )