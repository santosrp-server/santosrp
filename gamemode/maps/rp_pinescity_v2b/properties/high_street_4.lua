local Prop = {}
Prop.Name = "High Street 4"
Prop.Cat = "Stores"
Prop.Price = 800
Prop.Doors = {
	Vector( -10499.031250, 10972.965820, 160.031250 ),
	Vector( -10695.031250, 11371.036133, 160.031250 ),
	Vector( -10695.031250, 11511.339844, 160.031250 ),
}

GM.Property:Register( Prop )