--[[
	Name: atms.lua
	For: TalosLife
	By: TalosLife
]]--

local MapProp = {}
MapProp.ID = "atms"
MapProp.m_tblSpawn = {}
MapProp.m_tblAtms = {
	{ pos = Vector( -9693.031250, 10920.768555, 95 ), ang = Angle(0, -180, 0) },
	{ pos = Vector( -9015.031250, 11182.452148, 95 ), ang = Angle(0, -180, 0) },
	{ pos = Vector( -5259.031250, 2328.589111, 95 ), ang = Angle(0, 180, 0) },
	{ pos = Vector( 6391.576172, -7245.007324, 95 ), ang = Angle(0, -90, 0) },
}

function MapProp:CustomSpawn()
	for _, propData in pairs( self.m_tblAtms ) do
		local ent = ents.Create( "ent_atm" )
		ent:SetPos( propData.pos )
		ent:SetAngles( propData.ang )
		ent.IsMapProp = true
		ent:Spawn()
		ent:Activate()

		local phys = ent:GetPhysicsObject()
		if IsValid( phys ) then
			phys:EnableMotion( false )
		end
	end
end

GAMEMODE.Map:RegisterMapProp( MapProp )