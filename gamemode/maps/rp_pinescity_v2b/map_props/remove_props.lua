--[[
	Name: remove_props.lua
	For: TalosLife
	By: TalosLife
]]--

local MapProp = {}
MapProp.ID = "remove_props"
MapProp.m_tblSpawn = {}
MapProp.m_tblRemove = {
	--[[{ class = 'func_button', pos = Vector('-2888.000000 3471.510010 132.990005') },
	{ class = 'func_button', pos = Vector('-2664.000000 3471.510010 132.990005') },
	{ class = 'func_button', pos = Vector('-2879.000000 3091.510010 132.990005') },
	{ class = 'func_button', pos = Vector('-2451.000000 3091.510010 132.990005') },]]
}

function MapProp:CustomSpawn()
	for k, v in pairs( self.m_tblRemove ) do
		for _, ent in pairs( ents.FindInSphere(v.pos, 5) ) do
			if v.mdl then
				if ent:GetModel() == v.mdl then ent:Remove() end
			elseif v.class then
				if ent:GetClass() == v.class then ent:Remove() end
			end
		end
	end
end

GAMEMODE.Map:RegisterMapProp( MapProp )