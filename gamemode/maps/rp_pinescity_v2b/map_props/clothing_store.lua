--[[
	Name: clothing_store.lua
	For: TalosLife
	By: TalosLife
]]--

local MapProp = {}
MapProp.ID = "clothing_store"
MapProp.m_tblSpawn = {
	{ mdl = 'models/props/de_tides/vending_tshirt.mdl',pos = Vector('-9268.468750 8122.020020 168.031250'), ang = Angle('0.000 90.000 0.000') },
}

MapProp.m_tblMenuTriggers = {
	{ pos = Vector('-9268.468750 8122.020020 168.031250'), ang = Angle('0 90 0'), msg = "(Use) Purchase Clothing", menu = "clothing_items_store" },
}

function MapProp:CustomSpawn()
	for _, propData in pairs( self.m_tblMenuTriggers ) do
		local ent = ents.Create( "ent_menu_trigger" )
		ent:SetPos( propData.pos )
		ent:SetAngles( propData.ang )
		ent:SetMenu( propData.menu )
		ent.IsMapProp = true
		ent:Spawn()
		ent:Activate()
		ent:SetText( propData.msg )
	end
end

GAMEMODE.Map:RegisterMapProp( MapProp )