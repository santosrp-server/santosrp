--[[
	Name: misc_npcs.lua
	For: TalosLife
	By: TalosLife
]]--

--[[GAMEMODE.Map:RegisterNPCSpawn{
	UID = "evocity_frontdesk",
	pos = { Vector( -6019.968750, 9113.690430, 161 ) },
	angs = { Angle( 0, 180, 0 ) },
}]]

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "evocity_dmv",
	pos = { Vector( -6019.998047, 9035.158203, 161 ) },
	angs = { Angle( 0, 180, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "healer",
	pos = { Vector( -5376.494629, 2591.707520, 105 ) },
	angs = { Angle( 0, -90, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "bank_storage",
	pos = { Vector( -9898.183594, 11207.773438, 105 ) },
	angs = { Angle( 0, -90, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "chop_shop",
	pos = { Vector( -9670.953125, -2309.926758, 105 ) },
	angs = { Angle( 0, 127, 0 ) },
}