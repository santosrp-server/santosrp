--[[
	Name: job_npcs.lua
	For: TalosLife
	By: TalosLife
]]--

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "cop_job",
	pos = { Vector( -5214.742188, 11915.028320, 129 ) },
	angs = { Angle( 0, 0, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "cop_spawn_car",
	pos = { Vector( -5964.464844, 11763.045898, 105 ) },
	angs = { Angle( 0, 90, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "cop_jail_warden",
	pos = { Vector( -5210.031250, 11983.083008, 129 ) },
	angs = { Angle( 0, 0, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "ems_spawn",
	pos = { Vector( -5449.715332, 3154.045898, 105 ) },
	angs = { Angle( 0, -90, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "fire_chief",
	pos = { Vector( -2584.455078, -40.424206, 105 ) },
	angs = { Angle( 0, 90, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "taxi_spawn",
	pos = { Vector( -8792.232422, 11772.172852, 105 ) },
	angs = { Angle( 0, -90, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "tow_spawn",
	pos = { Vector( -10591.786133, -2273.039063, 105 ) },
	angs = { Angle( 0, 90, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "mail_spawn",
	pos = { Vector( -12057.875977, -647.179077, 105 ) },
	angs = { Angle( 0, 180, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "sales_truck_spawn",
	pos = { Vector( -2200.496094, 2689.442383, 105 ) },
	angs = { Angle( 0, 180, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "bus_spawn",
	pos = { Vector( -8615.583984, 11773.996094, 105 ) },
	angs = { Angle( 0, -90, 0 ) },
}