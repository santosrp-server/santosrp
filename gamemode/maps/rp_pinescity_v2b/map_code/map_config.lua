--[[
	Name: map_config.lua
	For: TalosLife
	By: TalosLife
]]--

if SERVER then
	resource.AddWorkshop( "1299936127" ) --Map

	--[[ Car Dealer Settings ]]--
	GM.Config.CarSpawns = {
		{ Vector( -3652.223877, 11847.091797, -349 ), Angle( 0, 0, 0 ) },
		{ Vector( -3808.682861, 11831.734375, -349 ), Angle( 0, 0, 0 ) },
		{ Vector( -3815.581787, 11505.465820, -349 ), Angle( 0, -180, 0 ) },
		{ Vector( -3644.717041, 11505.793945, -349 ), Angle( 0, -180, 0 ) },

		{ Vector( -5044.103516, 10958.128906, -349 ), Angle( 0, 0, 0) },
		{ Vector( -4879.048340, 10936.916016, -349 ), Angle( 0, 0, 0) },
		{ Vector( -4637.005371, 10940.815430, -349 ), Angle( 0, 0, 0) },
		{ Vector( -4628.050781, 12453.415039, -349 ), Angle( 0, -180, 0) },
	}
	GM.Config.CarGarageBBox = {
		Min = Vector( -5234.323730, 10527.918945, -375.929596 ),
		Max = Vector( -3142.327637, 12800.791016, -201.563629 ),
	}

	GM.Config.RobberyMoneyDrops = {

		Vector(-9136.391602, 11293.062500, 145),
		Vector(6265.870117, -7377.972656, 145),

	}

	--[[ Jail Settings ]]--
	GM.Config.JailPositions = {
		Vector( -6386.072754, 12163.492188, 409 ),
		Vector( -6381.292480, 12034.326172, 409 ),
		Vector( -6382.250000, 11913.604492, 409 ),
		Vector( -6379.563477, 11791.598633, 409 ),

		Vector( -6102.112305, 11786.467773, 409 ),
		Vector( -6104.640625, 11915.788086, 409 ),
		Vector( -6106.664063, 12043.147461, 409 ),
		Vector( -6111.399902, 12177.166016, 409 ),
	}
	GM.Config.JailReleasePositions = {
		Vector( -4904.367188, 11792.070313, 129 ),
		Vector( -5003.498047, 11788.938477, 129 ),
		Vector( -5105.183594, 11785.727539, 129 ),
	}
	GM.Config.JailBBox = {
		{
			Min = Vector( -7178.061035, 11698.194336, -30.545334 ),
			Max = Vector( -4756.517090, 12715.357422, 862.319519 ),
		},		
	}

	--[[ NPC Drug Dealer Settings ]]--
	GM.Config.DrugNPCPositions = {
		{ Vector(-3570.575684, 9033.420898, 105), Angle(1,-140,0) },
		{ Vector(-10100.715820, 10918.917969, 105), Angle(4, 130, 0) },
		{ Vector(6430.882324, 1569.293823, 548), Angle(11, -140, 0) },
		{ Vector(-1702.528564, 3869.135498, 105), Angle(0, -90, 0) },
		{ Vector(13090.618164, 10012.918945, -142), Angle(0,-180,0) },
		{ Vector(-5179.968750, 12339.031250, -341), Angle(9, 31, 0) },
	}

	--[[ Map Settings ]]--
	GM.Config.SpawnPoints = {
		Vector( -6433.809082, 9409.647461, 216.031250 ),
		Vector( -6352.971191, 9407.183594, 216.031250 ),
		Vector( -6280.146973, 9406.208984, 216.031250 ),
		Vector( -6208.260254, 9405.409180, 216.031250 ),
		Vector( -6140.875000, 9404.875000, 216.031250 ),
		Vector( -6139.749023, 8733.515625, 216.031250 ),
		Vector( -6208.625977, 8733.916016, 216.031250 ),
		Vector( -6281.008789, 8733.693359, 216.031250 ),
		Vector( -6358.029785, 8733.552734, 216.031250 ),
		Vector( -6427.949219, 8733.271484, 216.031250 ),
	}

	--[[ Register the car customs shop location ]]--
	GM.CarShop.m_tblGarage["rp_pinescity_v2b"] = {
		NoDoors = true,
		CarPos = {
			Vector( -10555.896484, -2700.819824, 98.031250 ),
			Vector( -10327.044922, -2693.350098, 99.531250 ),
			Vector( -10101.785156, -2695.970947, 99.531250 ),
			Vector( -9878.080078, -2693.971680, 98.031250 ),
		},		
		BBox = {
			Min = Vector( -10696.626953, -2887.614258, 90.987839 ),
			Max = Vector( -9738.854492, -2444.289307, 309.512146 ),
		},
		PlayerSetPos = Vector( -9741.770508, -3054.158203, 105 ),
	}

	--[[ Fire Spawner Settings ]]--
	GM.Config.AutoFiresEnabled = true
	GM.Config.AutoFireSpawnMinTime = 60 *15
	GM.Config.AutoFireSpawnMaxTime = 60 *25
	GM.Config.AutoFireSpawnPoints = {
		--Pine Hotel
		Vector( "-6861.920410 6522.878418 293.703613" ),
		Vector( "-6070.295898 6853.629883 313.476563" ),
		Vector( "-6013.424805 6331.988281 147.987915" ),

		--Tunnels
		Vector( "-4403.037598 5166.711914 96.536270" ),
		Vector( "4980.354980 10511.299805 132.226135" ),
		Vector( "-30.189060 1059.938843 121.574303" ),
		Vector( "7567.907715 -4704.986816 133.359894" ),

		--Garage Lot
		Vector( "-2714.651367 3435.821777 124.830399" ),

		--Hospital
		Vector( "-4496.235840 2859.368652 116.255417" ),

		--Opposite FD
		Vector( "-3432.119629 1872.953369 243.961960" ),
		Vector( "-2700.523193 1894.932861 123.885597" ),
		Vector( "-1502.274536 2234.324463 107.475388" ),

		--Lake
		Vector( "2890.711670 419.133270 165.883209" ),
		Vector( "5482.879883 309.100830 118.735077" ),
		Vector( "4984.878418 -3093.417725 102.459320" ),

		--Gas Station
		Vector( "6401.895996 -7469.266113 109.110733" ),
		Vector( "5937.801758 -6399.118164 92.862396" ),

		--District Houses
		Vector( "5096.353027 -8215.126953 95.160828" ),
		Vector( "6185.672363 -8324.787109 152.624725" ),
		Vector( "6347.052734 -8008.602539 139.044678" ),
		Vector( "8944.372070 -7986.366211 97.241852" ),
		Vector( "8791.139648 -10290.827148 124.483810" ),
		Vector( "6553.890625 -10443.992188 115.137711" ),

		--Trees Outside District Houses
		Vector( "10867.250000 -8238.603516 162.604706" ),
		Vector( "14336.256836 -9156.338867 102.138901" ),

		--Big Tunnel
		Vector( "13375.375977 -4467.700684 -112.611679" ),
		Vector( "12786.540039 -2707.906982 -146.039520" ),
		Vector( "12830.347656 980.815979 -138.118896" ),

		--Highway
		Vector( "13367.902344 6918.735352 -130.108582" ),

		--Abandoned Powerhouse
		Vector( "10738.556641 9280.721680 150.291718" ),

		--Court
		Vector( "-6363.199707 9031.812500 184.171143" ),
		Vector( "-7150.253906 9098.027344 105.901634" ),

		--City Gas Station
		Vector( "-8798.192383 11185.766602 128.221970" ),

		--Burger King
		Vector( "-10606.647461 9436.853516 113.884834" ),

	}
end

--[[ Car Dealer Settings ]]--
GM.Config.CarPreviewModelPos = Vector( -7540.707520, 10524.128906, 97 )
GM.Config.CarPreviewCamPos = Vector( -7275.640625, 10732.101563, 234.057159 )
GM.Config.CarPreviewCamAng = Angle( 19.681873, -136.572601, 0.000000 )
GM.Config.CarPreviewCamLen = 2

--[[ Chop Shop ]]--
GM.Config.ChopShop_ChopLocation = Vector( -9651.322266, -2332.439209, 105 )

--[[ Weather & Day Night ]]--
GM.Config.Weather_SkyZPos = nil --Let the code figure the zpos out on this map
GM.Config.FogLightingEnabled = false --This will override the fog hiding the farz