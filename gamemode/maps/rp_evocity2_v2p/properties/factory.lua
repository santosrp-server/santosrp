local Prop = {}
Prop.Name = "Factory"
Prop.Cat = "Warehouse"
Prop.Price = 800
Prop.Doors = {
	Vector( 8292, 13161, 122.25 ),
	Vector( 8288, 13014, 130 ),
	Vector( 8288, 12878, 130 ),
}

GM.Property:Register( Prop )