local Prop = {}
Prop.Name = "Nathan's Drug Shop"
Prop.Cat = "Stores"
Prop.Price = 800
Prop.Doors = {
	Vector( 3739, 1903, 134 ),
	Vector( 3739, 1803, 134 ),
	Vector( 4203, 1658, 130 ),
}

GM.Property:Register( Prop )