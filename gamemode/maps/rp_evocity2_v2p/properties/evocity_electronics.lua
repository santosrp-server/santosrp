local Prop = {}
Prop.Name = "Evocity Electronics"
Prop.Government = true
Prop.Doors = {
	Vector( -2669, -2146, 139 ),
	Vector( -2668.5, -2206, 139 ),
}

GM.Property:Register( Prop )