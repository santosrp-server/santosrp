local Prop = {}
Prop.Name = "Police Station"
Prop.Government = true
Prop.Doors = {
	Vector( -593, -1460, -373.75 ),
	Vector( -499, -1460, -373.75 ),
	Vector( -600, -1507, -373.75 ),
	Vector( -687, -1817, -373.75 ),
	Vector( -213, -2081, -373.75 ),
}

GM.Property:Register( Prop )