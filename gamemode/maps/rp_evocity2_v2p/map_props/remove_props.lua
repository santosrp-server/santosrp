--[[
	Name: remove_props.lua
	For: TalosLife
	By: TalosLife
]]--

local MapProp = {}
MapProp.ID = "remove_props"
MapProp.m_tblSpawn = {}
MapProp.m_tblRemove = {
	{ mdl = 'models/u4lab/chair_office_a.mdl', pos = Vector('-663.865845 -1715.992676 -403.239410'), ang = Angle('0.357 0.000 0.005') },
	{ mdl = 'models/u4lab/chair_office_a.mdl', pos = Vector('-665.944702 -1652.008057 -403.277527'), ang = Angle('0.132 0.000 -0.005') },
	{ mdl = 'models/props_wasteland/controlroom_desk001b.mdl', pos = Vector('-625.833252 -1683.997192 -411.125702'), ang = Angle('-0.335 -179.725 -0.038') },
}

function MapProp:CustomSpawn()
	for k, v in pairs( self.m_tblRemove ) do
		for _, ent in pairs( ents.FindInSphere(v.pos, 5) ) do
			if ent:GetModel() == v.mdl then ent:Remove() end
		end
	end
end

GAMEMODE.Map:RegisterMapProp( MapProp )