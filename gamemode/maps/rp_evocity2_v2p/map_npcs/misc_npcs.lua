--[[
	Name: misc_npcs.lua
	For: TalosLife
	By: TalosLife
]]--

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "evocity_frontdesk",
	pos = { Vector( -260, -1943, 76 ) },
	angs = { Angle( 0, -180, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "evocity_dmv",
	pos = { Vector( -248, -1853, 76 ) },
	angs = { Angle( 0, 170, 0 ) },
}