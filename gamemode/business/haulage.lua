--[[
	Name: haulage.lua
	For: SantosRP.Life
	By: Jake Butts
]]--

local Business = {}
Business.Name = "Haulage Inc."
Business.Price = 10000
Business.VIP = false

Business.MaxSupplyValue = 2500
Business.MinSupplyValue = 1000

Business.StartBuyPrice = 1000

if SERVER then

	function Business:RequestJob( ply, bData, jobType )

		if bData.HasActiveMission then
			ply:AddNote("You already have an active mission!")
			return false
		end

		if jobType == "buy" then
			if bData.Supplies >= GAMEMODE.Config.MaximumSupplies then ply:AddNote("Your Supply Level is Full!") return false end
			self:RequestBuyJob( ply, bData )
		else
			if bData.Supplies < GAMEMODE.Config.MinimumSupplies then ply:AddNote("You need more supplies!") return false end
			self:RequestSellJob( ply, bData )
		end

		return true

	end

	function Business:RequestBuyJob( ply, bData )

		local randPos = table.Random( GAMEMODE.Config.BusinessSupplyDrops )
		self:SpawnSupplies( ply, randPos, false )
		ply:AddNote("Some supplies have been dropped, find them quick!")

	end

	function Business:RequestSellJob( ply, bData )

		local randPos = table.Random( GAMEMODE.Config.HaulageIncDrops )
		self:SpawnSupplies( ply, randPos, true )
		ply:AddNote("Some supplies have been dropped, grab them to sell!")
		
	end

	function Business:SpawnSupplies( ply, position, sell )

		local ent = ents.Create("ent_business_supplies")
		ent:SetAngles( Angle(0,0,0) )
		ent:SetPos( position )
		ent:SetModel( "models/props_junk/wood_crate002a.mdl" )

		ent:SetPlyOwner( ply )
		ent:SetBusiness( Business.Name )
		ent:SetSell( sell )

		ent:Spawn()
		ent:Activate()

	end

end

GM.Business:RegisterBusiness( Business )